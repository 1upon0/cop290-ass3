#pragma once
#include <iostream>
#include <vector>
#include <cstring>
#include <cstdlib>
#include "game.h"
#include "gameObject.h"
#include "map.h"
#include "physics.h"
#include "log.h"
#define ORIGINAL 0
#define COPY 1
class GameEngine{
private:
	static GameEngine* singleton;
	GameEngine():tp(20),accumulated(0),network_accumulated(0),is_pause(false){
		assert(!singleton && "GameEngine Singleton");
		singleton = this;
		cout<<"GameEngine initialized";
	}
public:
	GameState original,copy;
	bool is_pause;
	Map map;
	const float tp;
	int64_t accumulated;
	int64_t network_accumulated;
	/**
	 * @brief Destructor of the game engine
	 * @details Destroys everything
	 */
	~GameEngine(){
		if(singleton)
			delete singleton;
		singleton=NULL;
	}
	void init(GameState *s){
		original = *s;
		copy = original;
	}
	/**
	 * @brief Score of the game
	 */
	pair<int,int> getScore(){
		pair<int,int> p;
		p.first=0;
		p.second=0;
		for(int i=0;i<original.head.n;i++){
			if(i%2)continue;
			if(original.v[i]->head.type==PUDGE_OBJ){
				if((i/2)%2==0)
					p.second+=original.v[i]->head.deaths;
				else
					p.first+=original.v[i]->head.deaths;
			}
		}
		return p;
	}
	/**
	 * @brief Initializes the game engine by creating the game objects
	 *
	 * @param _n Number of objects to create
	 */
	void init(uint32_t _n){
		//Take care that pattern P-H-P-H is maintained
		original.head.n = 2*_n;
		original.v.resize(2*_n);
		copy.head.n = 2*_n;
		copy.v.resize(2*_n);
		for(int i=0;i<_n;i++){
			Pudge* p = new Pudge;
			Hook* h = new Hook;
			//Initialize the pudge and the hook
			int a;
		
			if(i%2==0)


				a=800;
			else
				a =-800;
			int b =-200+i*3;

			p->init(vector3(a,b,0),i%2,i+1);
			h->init(p);
			original.v[2*i] = p;
			original.v[2*i+1] = h;
			((Hook*)original.v[2*i+1])->pudge = p;

			p = new Pudge;
			h = new Hook;
			p->init(vector3(a,b,0),i%2,i+1);
			h->init(p);
			copy.v[2*i] = p;
			copy.v[2*i+1] = h;
			((Hook*)copy.v[2*i+1])->pudge = p;
		}
		//They are in sync at the beginning
		original.head.last_synced=Timer::now();
		copy.head.last_synced=Timer::now();
	}
	static GameEngine* _(){
		if(!singleton)
			new GameEngine;
		return singleton;
	}
	int numberOfSimulations(int64_t dt){
		if(is_pause) return 0;
		return dt/tp;
	}
	/**
	 * @brief Simulation function
	 * @details Simulates the object as per the physics of each object. This is discrete time simuation in which operation of the system is modelled as sequence of events in time. Each event occur at particular instant in the time. We have kept this time constant(tp) to avoid the floating point determinism.
	 *
	 * @param apply_to Takes the state as the input. Simulation is applied to this state
	 */
	void simulate(uint32_t apply_to=ORIGINAL){
		GameState *state;
		if(apply_to==ORIGINAL){
			state= &original;
		}
		else{
			state=&copy;
		}
		for(int i=0;i<state->head.n;i++){
			state->v[i]->update(tp);
			// cout<<i<<" =";state->v[i]->head.cir.c.print();
		}
		checkAllCollision(apply_to);
	}
	/**
	 * @brief Virtual simulation
	 * @details This is virtual simulation. This is called when we are syncing the game state over the network. This simulates the sequence of commands received from all the players over a copy of game state and later the original game state is replaced be this copy state.
	 *
	 * @param v Vector of commands of all the players
	 * @param simulateTill Time stamp till which we have to simulate
	 */
	void virtualSimulation(vector<Command>& v,int64_t simulateTill){
		int n = numberOfSimulations(simulateTill - copy.head.last_synced + network_accumulated);
		network_accumulated = (simulateTill+network_accumulated-copy.head.last_synced) - n*tp;
		//Assuming that the vector of Commands is sorted as per time stamp
		int index=0,size=v.size();
		int64_t current_sync = copy.head.last_synced;
		for(int i=0;i<size;i++){
			// cout<<"[Accumulated time]"<<network_accumulated<<endl;
			// if(v[i].timestamp<current_sync || v[i].timestamp > simulateTill){
				// cout<<"Time Stamp:" <<v[i].timestamp<<endl;
				// cout<<"current_sync"<<current_sync<<endl;
				// cout<<"Simulate Till"<<simulateTill<<endl;
				// cout<<"Number of Simulation"<<n<<endl;
			// }
		}

		for(int i=0;i<n;i++){
			while(index<size && v[index].timestamp<current_sync + tp){
				applyCommand(v[index],COPY);
				index++;
			}
			if(i==n-1){
				while(index<size){
				applyCommand(v[index],COPY);
				index++;
				}
			}
			simulate(COPY);
			current_sync += tp;
		}
		if(n==0){
			while(index<size){
			applyCommand(v[index],COPY);
			index++;
			}
		}
		//Simulate till is the actual time now. I have successfully simulated till present time.
		//In the assignment constructor my original is also synced till present time.
		copy.head.last_synced = simulateTill;
		original=copy;
	}
	/**
	 * @brief Apply command
	 * @details Applies command on the game object on basis of the type of command. Every command having some id helps us in finding the object uniquely
	 *
	 * @param c Command
	 * @param apply_to Game state on which to apply
	 */
	void applyCommand(Command c,uint32_t apply_to = ORIGINAL){

		Hook* h;
		Pudge* p;
		if(c.type==HOOK){
			if(apply_to==ORIGINAL)
				h= getHook(c.id,&original);
			else
				h = getHook(c.id,&copy);
			h->processCommand(c);
		}
		else{
			if(apply_to==ORIGINAL)
				p = getPudge(c.id,&original);
			else
				p = getPudge(c.id,&copy);
			p->processCommand(c);
		}
	};
	/**
	 * @brief Get pudge by its id
	 */
	Pudge* getPudge(uint32_t _id,GameState* state){
		for(int i=0;i<state->v.size();i++){
		if(state->v[i]->head.type==PUDGE_OBJ && state->v[i]->head.id==_id)
			return (Pudge*)state->v[i];
		}
		assert(false && "Invalid id");
	}
	/**
	 * @brief Get hook by its id
	 */
	Hook* getHook(uint32_t _id,GameState* state){
		for(int i=0;i<state->v.size();i++){
		if(state->v[i]->head.type==HOOK_OBJ && ((Hook*)state->v[i])->pudge->head.id==_id)
			return (Hook*)(state->v[i]);
		}
		assert(false && "Invalid id");
	}
	/**
	 * @brief Checks the rot damage
	 */
	void checkRotDamage(Pudge *p1,Pudge*p2){
		if(p1->head.rot_active){
			Circle rot_circle;
			rot_circle.init(p1->head.cir.c,ROT_RADIUS);
			if(Physics::checkCollision(rot_circle,p2->head.cir,0)){
				p2->inflictDamage(ROT_DAMAGE/1000.0,p1);
			}
		}
	}
	/**
	 * @brief Checks collision between two pudges
	 */
	void checkPPCollision(Pudge* p1, Pudge* p2){
		if(Physics::checkCollision(p1->head.cir,p2->head.cir)){
			p1->stop();
			p2->stop();

		}
		if(p1->head.team != p2->head.team){
			checkRotDamage(p1,p2);
			checkRotDamage(p2,p1);
		}
	}
	/**
	 * @brief Checks collision between objects in the game. Also checks whether objects are within the bounds of the map
	 */
	void checkAllCollision(uint32_t apply_to=ORIGINAL){
		GameState* state;
		if(apply_to==ORIGINAL)
			state = &original;
		else
			state = &copy;
		for(int i=0;i<state->v.size();i++){
			if(state->v[i]->head.type == PUDGE_OBJ){

				if((Pudge*)(state->v[i])->head.is_hooked == false)
					if(!map.isBound(state->v[i]->head.cir)){
						// ((Pudge*)(state->v[i]))->head.cir.c.print();
						((Pudge*)(state->v[i]))->stop();
					}


			}
			for(int j=i+1;j<state->v.size();j++){
				if(state->v[i]->head.is_active==false || state->v[j]->head.is_active==false)continue;
				if(state->v[i]->head.type==HOOK_OBJ && state->v[j]->head.type==HOOK_OBJ)
					continue;
				if(state->v[i]->head.type==PUDGE_OBJ && state->v[j]->head.type==PUDGE_OBJ){
						checkPPCollision((Pudge*)state->v[i],(Pudge*)state->v[j]);
				}
				else{
					// check collision but dont solve penetration
					if(Physics::checkCollision(state->v[i]->head.cir,state->v[j]->head.cir,0)){
					//hook latched on the pudge - guess who?
						Pudge* p;Hook* h;
						if(state->v[i]->head.type==PUDGE_OBJ){
							p = (Pudge*)(state->v[i]);
							h = (Hook*)(state->v[j]);
						}
						else{
							p = (Pudge*)(state->v[j]);
							h = (Hook*)(state->v[i]);
						}
							// is the hook is in rebound state
							if(h->head.is_rebound)continue;
							// cout<<"print id=>";
							// cout<<state->v[j]->head.id << "caught"<<state->v[i]->head.id<<endl;
							h->catchIt(p);
					}
				}
			}
		}
	}
};
GameEngine* GameEngine::singleton=NULL;
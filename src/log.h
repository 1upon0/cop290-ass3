#pragma once
#include <cstdio>
#include <cstdarg>
#include <cassert>
#define LOG_INFO    0
#define LOG_ERROR   1
#define LOG_WARN    2
#define LOG_DEBUG   3
using namespace std;
class Logger{
  static Logger *singleton;
  Logger(){
    assert(singleton == NULL);
    singleton=this;
    #ifdef DEBUG
      level=LOG_DEBUG;
    #else
      level=LOG_ERROR;
    #endif
  #ifndef LOG_FILE
    to_file=false;
    fs= fopen("log.txt","a");
  #else
    to_file=true;
    fs= fopen(LOG_FILE,"a");
  #endif
    assert(fs && "Couldn't open log file for writing");
  }
public:
  int level,to_file;
  FILE *fs;
  ~Logger(){
    if(fs)
      fclose(fs);
  }
  static Logger* _(){
    if(!singleton)
      new Logger();
    return singleton;
  }
  void log(const char *msg,int lvl,const char *file,const char *func,int line,...){
    if(level<lvl)
      return;
    static char prefix[4][50]={"\e[1;32m","\e[1;31m","\e[1;33m","\e[1;36m"};
    static char fileprefix[4][50]={"INFO: ","ERROR: ","WARN: ","DEBUG: "};

    if(to_file){
      fprintf(fs,"%s%s:%d => %s\n",fileprefix[lvl],file,line,func);
    }else
      printf("%s%s:%d\e[0m %c \e[1;34m%s\e[0m\n",prefix[lvl],file,line,175,func);
    va_list args;
    va_start (args, line);
    vprintf (msg, args);
    va_end (args);
    printf("\n");
  }
};
void logTarget(int tofile=true){
  Logger::_()->to_file=tofile;
}
void logLevel(int level=LOG_DEBUG){
  Logger::_()->level=level;
}
Logger* Logger::singleton = NULL;
#define logMsg(msg,lvl,...) (Logger::_()->log((msg),(lvl),__FILE__,__PRETTY_FUNCTION__ ,__LINE__, ## __VA_ARGS__))
#define logInfo(msg,...) (logMsg(msg,LOG_INFO,## __VA_ARGS__))
#define logError(msg,...) (logMsg(msg,LOG_ERROR,## __VA_ARGS__))
#define logWarn(msg,...) (logMsg(msg,LOG_WARN,## __VA_ARGS__))
#define logDebug(msg,...) (logMsg(msg,LOG_DEBUG,## __VA_ARGS__))

#pragma once
#include <iostream>
#include <vector>
#include <cstring>
#include <cstdlib>
#include "command.h"
#include "physics.h"
#include "timer.h"

#include "log.h"
// #include <GL/glut.h>
// #include <GL/freeglut.h>

#define KILLS 2

#define PUDGE_OBJ 1
#define HOOK_OBJ 2

#define ROT_DAMAGE 2
#define HOOK_DAMAGE 190

#define PUDGE_RADIUS 90
#define HOOK_RADIUS	60


#define PUDGE_SPEED 0.4
#define HOOK_SPEED 1.0
#define TURN_RATE 15

#define HOOK_COOLDOWN 5  // in seconds
#define RESPAWN_TIME 6
#define PUDGE_HP 1000
#define HOOK_RANGE 1200
#define ROT_RADIUS 210  //TODO find a value which works the best
/**
 * @brief Parent class of game objects
 * @details contains general properties of the game object like id,type,speed, orientation, state etc
 *
 */
bool someone_died = false;
class GameObj{
public:
	/**
	 * @brief Header class
	 * @details Functions as a header when passed through network
	 *
	 */
	struct Head{

		GameObj *hook,*hooker;
		uint32_t hook_id,hooker_id;


		double move_start_time,rot_start_time,birth_time,hooked_since,idle_since,hook_end_since,dead_since;

		Circle cir,target_cir,temp_target;
		int32_t health,cool_down,respawn_time,hook_range;		//cool_down in ms
		float speed;
		uint32_t team,id;
		bool rot_active,is_hooked,is_killed,is_moving,is_queued,is_queued_hook;
		uint32_t kills,deaths;
		vector3 orientation,target_orientation;
		//For the hook
		Circle qTarget;
		bool is_active,is_rebound;
		double hook_start_time;
		Circle rebound_target;
		uint32_t type;
	} head;

	GameObj(){}
	/**
	 * @brief Copy constructor
	 * @details Called when object is passed as argument, returned as value and copy initialization
	 *
	 * @param g [description]
	 */
	GameObj(const GameObj& g){head = g.head;}
	/**
	 * @brief Assignment operator overloaded
	 * @details Called when already initialized object is copied. And not during the pass by value or return by value. This is also not called in case of copy initialization.
	 *
	 * @param g [description]
	 */
	GameObj& operator=(const GameObj& g){head = g.head;
		return *this;}
	/**
	 * @brief Virtual destructor
	 * @details Because the class contains virtual functions it is necessary to provide virtual destructor.
	 */
	virtual ~GameObj(){if(this)delete this;}
	/**
	 * @brief Virtual draw function
	 * @details Render the object onto the screen. It is kept virutal so that draw function of the derived class can be called from the pointer to base class.
	 */
	virtual void draw(){logInfo("GameObj Draw");}
	/**
	 * @brief Virtual copy constructor
	 * @details C++11 feature. Contructors can be virtual too.
	 * @return Pointer to object that is clone of given object
	 */
	virtual GameObj* clone(){return new GameObj(*this);}
	/**
	 * @brief Process command
	 * @details Processes the command calling the appropiate functions on the basis of the type of the command
	 *
	 * @param c Command
	 */
	virtual void processCommand(Command& c){};
	/**
	 * @brief Virtual update functions
	 * @details Takes care of the simulation. Time(in milli-seconds) is the input to this function. This updates object position, velocity, orientation etc. taking care of physics of the object.
	 *
	 * @param dt Time in milliseconds
	 */
	virtual void update(float dt=0){}
	/**
	 * @brief Loading from the vector<char>
	 * @details Loads the object from the buffer. This buffer is received over the network
	 *
	 * @param buff vector<char> buffer
	 */
	virtual void loadFrom(vector<char>& buff){
		assert(buff.size() >= sizeof(Head) && "Pudge");
		memcpy((char*)&head,buff.data(),sizeof(Head));
	}
	/**
	 * @brief Load to buffer
	 * @details Loads the object into the buffer. This buffer is then passed over the network
	 *
	 * @param buff vector<char> buffer
	 */
	virtual void loadTo(vector<char>& buff){
		buff.resize(sizeof(Head));
		memcpy(buff.data(),(char*)&head,sizeof(Head));
	}
};
class Pudge : public GameObj{
public:
	Pudge(){
		head.type = PUDGE_OBJ;
		logInfo("Pudge created[Called twice]");
		head.hook = NULL;
		head.hooker = NULL;
		head.birth_time = 0;
		head.kills=0;
		head.deaths = 0;
		head.hook_id = 0;
		head.hooker_id = 0;
		head.dead_since = 0;
	}
	void init(vector3 p,uint32_t _team,uint32_t _id){



		head.cir.init(p,PUDGE_RADIUS);
		head.target_cir.init(p,1);
		head.orientation = vector3(1,1,0);
		head.orientation.normalize();
		head.team = _team;
		head.id = _id;
		head.health=PUDGE_HP;
		head.cool_down=head.respawn_time=0;
		head.rot_active=head.is_hooked=head.is_killed=false;

		head.hook_range = HOOK_RANGE;
		head.idle_since = -1;
		head.speed=PUDGE_SPEED;
		head.move_start_time = -1;
		head.rot_start_time = -1;
		head.is_active = true;

		head.is_queued = false;
		head.is_queued_hook = false;

	}
	int getCoolDown(){			//Gives cool_down in seconds
		return (head.cool_down/1000);
	}
	int getHP(){				//Gives HP
		return head.health;
	}
	Pudge(const Pudge& p){head = p.head;}
	virtual ~Pudge(){if(this)delete this;}
	virtual Pudge* clone(){return new Pudge(*this);}
	virtual void processCommand(Command& c){
		if(!head.is_hooked && !head.is_killed){
			switch(c.type){
				case MOVE:
					applyMove(c);
					break;
				case ROT:
					applyRot(c);
					break;
				default:
					logError("Invalid Command");
					break;
			}
		}
		else{
			cout<<"Respawn time:"<<head.respawn_time<<endl;
			logDebug("%d %d",head.is_hooked,head.is_killed);
		}
	}
	void inflictDamage(float d,GameObj* hitter){
		head.health = head.health-d;

		//cout<<Timer::now()<<" =>";
		//cout<<"[HP]"<<head.health<<endl;

		if(head.health<=0){
			head.health=0;
			if(head.id != hitter->head.id)
				hitter->head.kills++;
				head.deaths++;
			die();
		}
	}
	void applyMove(Command c){

		if(head.move_start_time < 0)
			head.move_start_time = head.birth_time;
		head.target_cir.c = c.v;
		head.target_orientation = c.v-head.cir.c;

		head.target_orientation.normalize();
		float theta = acos(head.target_orientation.normalized()*head.orientation.normalized());
		if( theta > 0.01){
			//logInfo("theta : %f",theta);
			head.is_queued = true;
		}

		// cout<<"[Pudge-Move]";
		// c.v.print();
	}
	void applyRot(Command c){
		cout<<head.rot_active<<endl;
		head.rot_active = !head.rot_active;
		head.rot_start_time = head.birth_time;
	}
	void applyHook(Command c,GameObj* h){
		if(head.is_hooked)return;
		assert(h != 0 && h->head.id == head.id && "applyHook should get my own hook");
		head.hook = h;
		head.hook_id = h->head.id;
		stop();

		if(head.move_start_time < 0)
			head.move_start_time = head.birth_time;
		head.qTarget.c = c.v;
		head.is_queued_hook = true;
		head.target_orientation = c.v-head.cir.c;
		head.target_orientation.normalize();
		head.is_queued_hook = true;
		float theta = acos(head.target_orientation.normalized()*head.orientation.normalized());

		if( theta > 0.01){
			//logInfo("theta : %f",theta);
			head.is_queued = true;
		}


	}
	void stop(){
		head.move_start_time = -1;
		head.idle_since = head.birth_time;
		head.target_cir.c = head.cir.c;

		if(head.is_hooked){
			head.is_hooked = false;
			head.speed = PUDGE_SPEED;
		}
	}
	void die(){
		cout<<"I die"<<endl;
		someone_died=true;
		//head.kills++;
		head.is_killed = true;
		head.is_active = false;
		head.respawn_time=RESPAWN_TIME*1000;
		head.dead_since = head.birth_time;
		//head.is_moving = false;
	}
	void respawn(){
		cout<<"respawned";
		cout<<"dead since"<<head.birth_time-head.dead_since<<endl;
		if(head.team==0)
			init(vector3(500,-800,0),head.team,head.id);
		else
			init(vector3(-500,-800,0),head.team,head.id);

	}
	virtual void update(float dt=0){
		// cout<<"Time:"<<dt<<endl;

		head.birth_time += dt;


		if(head.rot_active && !head.is_killed)
			inflictDamage(ROT_DAMAGE/1000.0,this);

		if(head.cool_down > 0)head.cool_down -= dt;
		if(head.cool_down < 0)head.cool_down = 0;

			//cout<<"[Head.cir.c] before";head.cir.c.print();
		if(head.is_hooked){
			head.target_cir.c = head.hooker->head.cir.c;
		}

		if(!head.is_queued){
			head.cir.v = (head.target_cir.c-head.cir.c).normalized()*head.speed;
			head.cir.c += (head.cir.v*dt);
			if(head.is_queued_hook){
				cout<<"LOL"<<endl;
				head.is_queued_hook = false;
				head.hook->head.is_active=true;
				head.hook->head.is_rebound=false;
				head.hook->head.hook_start_time = head.hook->head.birth_time;
				head.hook->head.cir.init(head.cir.c,10);
				cout<<"[HOOK]initiated at ";head.cir.c.print();
				vector3 line_of_fire = head.qTarget.c - head.hook->head.cir.c;
				line_of_fire.normalize();
				line_of_fire *= HOOK_RANGE;
				head.hook->head.target_cir.init(head.cir.c+line_of_fire,1);		//Radius is zero
				head.hook->head.rebound_target.init(head.cir.c,1);
				head.cool_down = HOOK_COOLDOWN*1000;
		}
		}
		else{
			//rotate the pudge,find the angle from target orientation
			float dot = head.target_orientation.normalized()*head.orientation.normalized();
			float theta;
			if(dot >= 1)theta = 0;
			else theta = acos(dot);

			//cout<<"theta "<<theta<<" "<<head.target_orientation.normalized()*head.orientation.normalized()<<endl;
			if(abs(theta) < 0.01){
				// have reached the rget )rientation
			//	cout<<"Done"<<endl;

				head.is_queued = false;
			}
			else{
				//rotate by 1 degree,0.0174 rads
				float dir = 1;
				vector3 cr = head.target_orientation.normalized()^head.orientation.normalized();
				if(cr.z >= 0)dir = -1;
				else dir = 1;
				//cout<<"DIR "<<TURN_RATE*dir*dt<<endl;
				float rot = TURN_RATE*dir*dt*0.001;
				float x = head.orientation.x*cos(rot)-head.orientation.y*sin(rot);
				float y = head.orientation.x*sin(rot)+head.orientation.y*cos(rot);
				head.orientation.x = x; head.orientation.y = y;

				cr = head.target_orientation.normalized()^head.orientation.normalized();
				if(cr.z*dir >0){
           head.orientation = head.target_orientation;
           head.is_queued = false;
        }
				//head.is_queued = 0;
				//head.orientation.print();
			}

		}


			vector3 dist = head.target_cir.c - head.cir.c;
			float d = dist.mag();
			if(d < 10){

				stop();
			}

			if(head.is_killed){
				//cout<<"resp time "<<head.respawn_time<<endl;
				if(head.respawn_time > 0)head.respawn_time -= dt;
				if(head.respawn_time <= 0){
					head.is_killed = false;
					head.is_active = true;
					respawn();
				}
			}
	}
	virtual void loadFrom(vector<char>& buff){
		assert(buff.size() >= sizeof(Head) && "Pudge");
		memcpy((char*)&head,buff.data(),sizeof(Head));
	}
	/**
	 * @brief Load to buffer
	 * @details Loads the object into the buffer. This buffer is then passed over the network
	 *
	 * @param buff vector<char> buffer
	 */
	virtual void loadTo(vector<char>& buff){
		cout<<"Pudge load to called"<<endl;
		buff.resize(sizeof(Head));
		memcpy(buff.data(),(char*)&head,sizeof(Head));
	}
	virtual void draw(){
		if(!head.is_active)return;

		if(!head.rot_active)
			glColor3f(head.team, 1 , head.team);
		else
			glColor3f(head.team, 0.5 , head.team);
		glBegin( GL_TRIANGLE_FAN );
        glVertex2f(head.cir.c.x, head.cir.c.y);
        for( int n = 0; n <= 20; ++n )
        {
            float const t = 2*M_PI*n/20.0;
            glVertex2f(head.cir.c.x + sin(t)*head.cir.r, head.cir.c.y + cos(t)*head.cir.r);
        }
	    glEnd();
	    glLineWidth(2.5);
		glColor3f(1.0, 0.0, 0.0);
		glBegin(GL_LINES);
		glVertex2f(head.cir.c.x,head.cir.c.y);
		vector3 e = head.cir.c + head.orientation.normalized()*30;
		glVertex2fv(e);
		glEnd();
		 glLineWidth(2.5);
		glColor3f(0.0, 0.0, 1.0);
		glBegin(GL_LINES);
		glVertex2f(head.cir.c.x,head.cir.c.y);
		e = head.cir.c + head.target_orientation.normalized()*30;
		glVertex2fv(e);
		glEnd();


	}
};

class Hook : public GameObj{
public:
	Pudge* pudge;
	Hook(){
		cout<<"Hook cretes[should be called once]"<<endl;
		head.type=HOOK_OBJ;
		head.birth_time = 0;
		pudge=NULL;
	}
	virtual ~Hook(){	//virtual destructor
		if(this)
		delete this;
	}
	Hook(const Hook& h){
		head = h.head;
		//I don't want copy dude
		pudge=NULL;
	}
	virtual Hook* clone(){	//copy constructor
		return new Hook(*this);
	}
	void init(Pudge* p){
		//Initializing the hook w.r.t pudge
		pudge = p;
		head.cir.r=HOOK_RADIUS;	//Radius of the hook

		//On the assumption thatp-h-p-h pattern
		head.id = p->head.id;
		//Keep the head.speed same else we have to deal it with it in rebound state
		head.speed = HOOK_SPEED;
		head.is_active=head.is_rebound=false;
    head.hook_end_since=head.hook_start_time=-1;
	}
	virtual void processCommand(Command& c){
		assert(pudge && "Invalid hook! Pudge is NULL");
		assert(c.type == HOOK && "Invalid Command");
		if(pudge->head.cool_down>0)return;
		cout<<"[Hook]"<<endl;
		pudge->applyHook(c,this);
		cout<<"[Hook]"<<endl;


		// //pudge->head.target_cir.c = c.v;
		// pudge->head.orientation = c.v-head.cir.c;
		// // pudge->head.target_orientation.normalize();
		// // float theta = acos(pudge->head.target_orientation.normalized()*pudge->head.orientation.normalized());
		// // if( theta > 0.01){
		// // 	//logInfo("theta : %f",theta);
		// // 	pudge->head.is_queued = true;
		// // }



		// head.is_active=true;
		// head.is_rebound=false;
		// head.hook_start_time = head.birth_time;
		// head.cir.init(pudge->head.cir.c,10);
		// cout<<"[HOOK]initiated at ";pudge->head.cir.c.print();
		// vector3 line_of_fire = c.v - head.cir.c;
		// line_of_fire.normalize();
		// line_of_fire *= HOOK_RANGE;
		// head.target_cir.init(head.cir.c+line_of_fire,1);		//Radius is zero
		// head.rebound_target.init(pudge->head.cir.c,1);
		// pudge->head.cool_down = HOOK_COOLDOWN*1000;
	}
	virtual void update(float dt=0){
    head.birth_time += dt;
		if(head.is_active){
      if(head.is_rebound)
			{
        head.target_cir.c = pudge->head.cir.c;

        auto hookleft=head.target_cir.c-head.cir.c;
        if(hookleft.mag()<=200){
          if(head.hook_end_since<0)
            head.hook_end_since=head.birth_time;
        }else
          head.hook_end_since=-1;
      }else
        head.hook_end_since=-1;

			head.cir.v=(head.target_cir.c-head.cir.c).normalized()*head.speed;
			head.cir.c += (head.cir.v*dt);
			//cout<<pudge->head.cool_down<<endl;

			if(Physics::checkCollision(head.cir,head.target_cir,0))
			{

				cout<<"[Hook] reached target"<<endl;
				if(head.is_rebound){
					destroy();
				}
				else{
					cout<<"rebound"<<endl;
					rebound();
          head.hook_end_since=-1;
				}
			}
		}
	}
	void destroy(){
		head.is_active=false;
		cout<<"hook lasted for "<<head.birth_time-head.hook_start_time<<endl;
		head.hook_start_time = -1;

	}
	void rebound(){
		head.is_rebound=true;
		head.target_cir.c = head.rebound_target.c;
		//cout<<"[Hook] set my new target as ";
		// cout<<"Rebound is:";pudge->head.cir.c.print();
	}
	void catchIt(Pudge* p){
		if(!head.is_active)return;
		if(p->head.id == pudge->head.id)return;
		cout<<"[Hook] hooked "<<p->head.id<<endl;
		rebound();
		if(p->head.team!=pudge->head.team)
			p->inflictDamage(HOOK_DAMAGE,pudge);
		if(!p->head.is_hooked){
			p->head.is_hooked=true;
			p->head.hooked_since = p->head.birth_time;
			p->head.hooker = pudge;
			p->head.hooker_id = pudge->head.id;
			//p->head.is_moving=true;
			p->head.target_cir.c = pudge->head.cir.c;
			//cout<<"Target:";head.target_cir.c.print();
			p->head.speed = head.speed;
			//cout<<"latched onto it"<<endl;
		}
	}
	virtual void loadFrom(vector<char>& buff){
		assert(buff.size() >= sizeof(Head) && "Hook");
		memcpy((char*)&head,buff.data(),sizeof(Head));
	}
	/**
	 * @brief Load to buffer
	 * @details Loads the object into the buffer. This buffer is then passed over the network
	 *
	 * @param buff vector<char> buffer
	 */
	virtual void loadTo(vector<char>& buff){
		buff.resize(sizeof(Head));
		memcpy(buff.data(),(char*)&head,sizeof(Head));	}
	virtual void draw(){
		if(!head.is_active)return;
		glColor3f(0.2, 0.1, 0.0);
		glBegin( GL_TRIANGLE_FAN );
        glVertex2f(head.cir.c.x, head.cir.c.y);
        for( int n = 0; n <= 20; ++n ){
            float const t = 2*M_PI*n/20.0;
            glVertex2f(head.cir.c.x + sin(t)*head.cir.r, head.cir.c.y + cos(t)*head.cir.r);
        }
	    glEnd();
	 }
};
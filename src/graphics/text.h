#include "boilerplate.h"
#include <ft2build.h>
#include FT_FREETYPE_H

class Text{
  FT_Library ft;
  static Text *singleton;
  Text(){
    assert(!singleton);
    singleton=this;
    init();
  }
public:
  static Text* _(){
    if(!singleton)
      new Text();
    return singleton;
  }
   struct HUDvert{
    glm::vec2 pos;
    glm::vec2 tex;
  };
  float width,height;
  struct CharInfo{
    GLuint tex;
    float w,h,x,y,nx,ny;
  };
  vector<CharInfo> chars;
  GLDirectObject banner;
  void renderBanner(float x1,float y1,float x2,float y2,uint tex){
    assert(y2>=y1 && x2>=x1);
    banner.preRender();
    glm::mat4 screen=translate(mat4(1),vec3(-1+(x1+x2)/width,1-(y1+y2)/height,0))*scale(mat4(1),vec3((x2-x1)/width,(y2-y1)/height,1));
    glUniformMatrix4fv(0, 1, GL_FALSE, &screen[0][0]);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex);
    banner.render(GL_TRIANGLE_FAN);
    glBindTexture(GL_TEXTURE_2D,0);
  }
  void init(){
    width=SCREEN_WIDTH;
    height=SCREEN_HEIGHT;
    HUDvert verts[4];
    verts[0].pos=glm::vec2(-1,-1);verts[0].tex=glm::vec2(0,1);
    verts[1].pos=glm::vec2(1,-1);verts[1].tex=glm::vec2(1,1);
    verts[2].pos=glm::vec2(1,1);verts[2].tex=glm::vec2(1,0);
    verts[3].pos=glm::vec2(-1,1);verts[3].tex=glm::vec2(0,0);
    banner.loadShaders("shaders/hud-vert.glsl","shaders/plain-frag.glsl");
    banner.addAttrib("Position",offsetof(HUDvert,pos),2,GL_FLOAT);
    banner.addAttrib("TexCoord",offsetof(HUDvert,tex),2,GL_FLOAT);
    banner.bindBuffers(verts,sizeof(HUDvert),4);



    if(FT_Init_FreeType(&ft)) {
      logError("Could not init freetype library\n");
      assert(false);
    }
    FT_Face face;

    if(FT_New_Face(ft, "raleway.ttf", 0, &face)) {
      logError("Could not open font\n");
      assert(false);
    }
    FT_Set_Pixel_Sizes(face, 0, 14);
    vector<unsigned char> tmp;
    for(int i=32;i<128;i++){
      if(FT_Load_Char(face,char(i), FT_LOAD_RENDER)) {
        logError("Could not load character '%c'\n",i);
        assert(false);
      }
      FT_GlyphSlot g = face->glyph;
      g->bitmap.buffer;
      auto size=g->bitmap.width*g->bitmap.rows;
      tmp.clear();
      CharInfo info;
      info.w=g->bitmap.width;
      info.h=g->bitmap.rows;
      info.x=g->bitmap_left;
      info.y=-g->bitmap_top;
      info.nx=g->advance.x/64.f;
      info.ny=-g->advance.y/64.f;
      if(size==0)
      {
        tmp.push_back(0); tmp.push_back(0); tmp.push_back(0);
        tmp.push_back(0);
        info.tex=GetRenderer()->loadTex(1,1,tmp.data());
      }else{
        for(uint x=0;x<size;x++){
          tmp.push_back(0);
          tmp.push_back(0);
          tmp.push_back(0);
          tmp.push_back(g->bitmap.buffer[x]);
        }
        info.tex=GetRenderer()->loadTex(info.w,info.h,tmp.data());
      }
      chars.push_back(info);
    }
  }

  void calcBox(string text,float &w,float &h){
    int ch=0;
    float x=0,y=0;
    for(size_t i=0;i<text.length();i++){
      ch=text[i]-32;
      // cout<<(int)ch<<endl;
      assert(ch>=0 && ch<96);
      // x1=x+chars[ch].x;
      // y1=y+chars[ch].y;
      // x2=x1+chars[ch].w;
      // y2=y1+chars[ch].h;
      // logInfo("%f %f %f %f",x1,y1,x2,y2);
      // renderBanner(x1,y1,x2,y2,chars[ch].tex);
      x+=chars[ch].nx;
      y+=chars[ch].ny;
    }
    w=x;
    h=y;
  }

  void render(string text, float x1, float y1, float x2,float y2) {
    float px=2,py=2;
    float w,h;
    float s=1;
    calcBox(text,w,h);
    int ch=0;
    float w1=x2-x1-2*px;

    if(w>w1)
    {
      s=w1/w;
      w=w1;
      h=h*s;
    }
    float h1=y2-y1-2*py;
    if(h>(y2-y1-2*py)){
      float s1=h1/h;
      h=h1;
      w=w*s1;
      s=s*s1;
    }
    x1+=(x2-x1-w)/2+px;
    y1+=(y2-y1-h)/2+py;
    float x=x1,y=y1;

    for(size_t i=0;i<text.length();i++){
      ch=text[i]-32;
      // cout<<(int)ch<<endl;
      assert(ch>=0 && ch<96);
      x1=x+chars[ch].x*s;
      y1=y+chars[ch].y*s;
      x2=x1+chars[ch].w*s;
      y2=y1+chars[ch].h*s;
      // logInfo("%f %f %f %f",x1,y1,x2,y2);
      renderBanner(x1,y1,x2,y2,chars[ch].tex);
      x+=chars[ch].nx*s;
      y+=chars[ch].ny*s;
    }
  }
};
Text* Text::singleton=NULL;
void RenderText(string text,float x1,float y1,float x2,float y2){
  Text::_()->render(text,x1,y1,x2,y2);
}
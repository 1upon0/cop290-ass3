\contentsline {chapter}{\numberline {1}R\discretionary {-}{}{}E\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}M\discretionary {-}{}{}E}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Hierarchical Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Class Hierarchy}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Class List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}Class Documentation}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Address\discretionary {-}{}{}Message Class Reference}{7}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Member Function Documentation}{7}{subsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.1}load\discretionary {-}{}{}From}{7}{subsubsection.4.1.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.2}load\discretionary {-}{}{}To}{8}{subsubsection.4.1.1.2}
\contentsline {section}{\numberline {4.2}A\discretionary {-}{}{}I Class Reference}{8}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Member Function Documentation}{8}{subsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.1.1}decide\discretionary {-}{}{}Action}{8}{subsubsection.4.2.1.1}
\contentsline {subsubsection}{\numberline {4.2.1.2}dodge}{8}{subsubsection.4.2.1.2}
\contentsline {subsubsection}{\numberline {4.2.1.3}get\discretionary {-}{}{}Val}{9}{subsubsection.4.2.1.3}
\contentsline {subsubsection}{\numberline {4.2.1.4}incoming\discretionary {-}{}{}Hooks}{9}{subsubsection.4.2.1.4}
\contentsline {subsubsection}{\numberline {4.2.1.5}max\discretionary {-}{}{}Value\discretionary {-}{}{}Action}{9}{subsubsection.4.2.1.5}
\contentsline {subsubsection}{\numberline {4.2.1.6}surround}{9}{subsubsection.4.2.1.6}
\contentsline {section}{\numberline {4.3}Check\discretionary {-}{}{}Message Class Reference}{10}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Member Function Documentation}{10}{subsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.1.1}load\discretionary {-}{}{}From}{10}{subsubsection.4.3.1.1}
\contentsline {subsubsection}{\numberline {4.3.1.2}load\discretionary {-}{}{}To}{10}{subsubsection.4.3.1.2}
\contentsline {section}{\numberline {4.4}Circle Class Reference}{10}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Detailed Description}{11}{subsection.4.4.1}
\contentsline {section}{\numberline {4.5}Cmd\discretionary {-}{}{}Message Class Reference}{11}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Member Function Documentation}{11}{subsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.1.1}load\discretionary {-}{}{}From}{11}{subsubsection.4.5.1.1}
\contentsline {subsubsection}{\numberline {4.5.1.2}load\discretionary {-}{}{}To}{12}{subsubsection.4.5.1.2}
\contentsline {section}{\numberline {4.6}Command Class Reference}{12}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Member Function Documentation}{12}{subsection.4.6.1}
\contentsline {subsubsection}{\numberline {4.6.1.1}init}{12}{subsubsection.4.6.1.1}
\contentsline {section}{\numberline {4.7}Data Struct Reference}{12}{section.4.7}
\contentsline {section}{\numberline {4.8}Data\discretionary {-}{}{}Message Class Reference}{13}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}Member Function Documentation}{13}{subsection.4.8.1}
\contentsline {subsubsection}{\numberline {4.8.1.1}load\discretionary {-}{}{}From}{13}{subsubsection.4.8.1.1}
\contentsline {subsubsection}{\numberline {4.8.1.2}load\discretionary {-}{}{}To}{13}{subsubsection.4.8.1.2}
\contentsline {section}{\numberline {4.9}Game\discretionary {-}{}{}Engine Class Reference}{14}{section.4.9}
\contentsline {subsection}{\numberline {4.9.1}Constructor \& Destructor Documentation}{14}{subsection.4.9.1}
\contentsline {subsubsection}{\numberline {4.9.1.1}$\sim $\discretionary {-}{}{}Game\discretionary {-}{}{}Engine}{14}{subsubsection.4.9.1.1}
\contentsline {subsection}{\numberline {4.9.2}Member Function Documentation}{15}{subsection.4.9.2}
\contentsline {subsubsection}{\numberline {4.9.2.1}apply\discretionary {-}{}{}Command}{15}{subsubsection.4.9.2.1}
\contentsline {subsubsection}{\numberline {4.9.2.2}init}{15}{subsubsection.4.9.2.2}
\contentsline {subsubsection}{\numberline {4.9.2.3}simulate}{15}{subsubsection.4.9.2.3}
\contentsline {subsubsection}{\numberline {4.9.2.4}virtual\discretionary {-}{}{}Simulation}{15}{subsubsection.4.9.2.4}
\contentsline {section}{\numberline {4.10}Game\discretionary {-}{}{}Obj Class Reference}{15}{section.4.10}
\contentsline {subsection}{\numberline {4.10.1}Detailed Description}{16}{subsection.4.10.1}
\contentsline {subsection}{\numberline {4.10.2}Constructor \& Destructor Documentation}{16}{subsection.4.10.2}
\contentsline {subsubsection}{\numberline {4.10.2.1}Game\discretionary {-}{}{}Obj}{16}{subsubsection.4.10.2.1}
\contentsline {subsubsection}{\numberline {4.10.2.2}$\sim $\discretionary {-}{}{}Game\discretionary {-}{}{}Obj}{17}{subsubsection.4.10.2.2}
\contentsline {subsection}{\numberline {4.10.3}Member Function Documentation}{17}{subsection.4.10.3}
\contentsline {subsubsection}{\numberline {4.10.3.1}clone}{17}{subsubsection.4.10.3.1}
\contentsline {subsubsection}{\numberline {4.10.3.2}draw}{17}{subsubsection.4.10.3.2}
\contentsline {subsubsection}{\numberline {4.10.3.3}load\discretionary {-}{}{}From}{17}{subsubsection.4.10.3.3}
\contentsline {subsubsection}{\numberline {4.10.3.4}load\discretionary {-}{}{}To}{17}{subsubsection.4.10.3.4}
\contentsline {subsubsection}{\numberline {4.10.3.5}operator=}{18}{subsubsection.4.10.3.5}
\contentsline {subsubsection}{\numberline {4.10.3.6}process\discretionary {-}{}{}Command}{18}{subsubsection.4.10.3.6}
\contentsline {subsubsection}{\numberline {4.10.3.7}update}{18}{subsubsection.4.10.3.7}
\contentsline {section}{\numberline {4.11}Game\discretionary {-}{}{}State Class Reference}{18}{section.4.11}
\contentsline {subsection}{\numberline {4.11.1}Detailed Description}{19}{subsection.4.11.1}
\contentsline {subsection}{\numberline {4.11.2}Constructor \& Destructor Documentation}{19}{subsection.4.11.2}
\contentsline {subsubsection}{\numberline {4.11.2.1}Game\discretionary {-}{}{}State}{19}{subsubsection.4.11.2.1}
\contentsline {subsubsection}{\numberline {4.11.2.2}$\sim $\discretionary {-}{}{}Game\discretionary {-}{}{}State}{19}{subsubsection.4.11.2.2}
\contentsline {subsection}{\numberline {4.11.3}Member Function Documentation}{19}{subsection.4.11.3}
\contentsline {subsubsection}{\numberline {4.11.3.1}load\discretionary {-}{}{}From}{19}{subsubsection.4.11.3.1}
\contentsline {subsubsection}{\numberline {4.11.3.2}load\discretionary {-}{}{}To}{20}{subsubsection.4.11.3.2}
\contentsline {subsubsection}{\numberline {4.11.3.3}operator=}{20}{subsubsection.4.11.3.3}
\contentsline {section}{\numberline {4.12}Game\discretionary {-}{}{}State\discretionary {-}{}{}:\discretionary {-}{}{}:Head Struct Reference}{20}{section.4.12}
\contentsline {subsection}{\numberline {4.12.1}Detailed Description}{20}{subsection.4.12.1}
\contentsline {section}{\numberline {4.13}Game\discretionary {-}{}{}Obj\discretionary {-}{}{}:\discretionary {-}{}{}:Head Struct Reference}{20}{section.4.13}
\contentsline {subsection}{\numberline {4.13.1}Detailed Description}{21}{subsection.4.13.1}
\contentsline {section}{\numberline {4.14}Message\discretionary {-}{}{}:\discretionary {-}{}{}:Header Struct Reference}{21}{section.4.14}
\contentsline {section}{\numberline {4.15}Hook Class Reference}{22}{section.4.15}
\contentsline {subsection}{\numberline {4.15.1}Constructor \& Destructor Documentation}{22}{subsection.4.15.1}
\contentsline {subsubsection}{\numberline {4.15.1.1}$\sim $\discretionary {-}{}{}Hook}{22}{subsubsection.4.15.1.1}
\contentsline {subsubsection}{\numberline {4.15.1.2}Hook}{23}{subsubsection.4.15.1.2}
\contentsline {subsection}{\numberline {4.15.2}Member Function Documentation}{24}{subsection.4.15.2}
\contentsline {subsubsection}{\numberline {4.15.2.1}catch\discretionary {-}{}{}It}{24}{subsubsection.4.15.2.1}
\contentsline {subsubsection}{\numberline {4.15.2.2}clone}{24}{subsubsection.4.15.2.2}
\contentsline {subsubsection}{\numberline {4.15.2.3}draw}{24}{subsubsection.4.15.2.3}
\contentsline {subsubsection}{\numberline {4.15.2.4}init}{24}{subsubsection.4.15.2.4}
\contentsline {subsubsection}{\numberline {4.15.2.5}load\discretionary {-}{}{}From}{24}{subsubsection.4.15.2.5}
\contentsline {subsubsection}{\numberline {4.15.2.6}load\discretionary {-}{}{}To}{24}{subsubsection.4.15.2.6}
\contentsline {subsubsection}{\numberline {4.15.2.7}process\discretionary {-}{}{}Command}{26}{subsubsection.4.15.2.7}
\contentsline {subsubsection}{\numberline {4.15.2.8}update}{26}{subsubsection.4.15.2.8}
\contentsline {section}{\numberline {4.16}Input Class Reference}{26}{section.4.16}
\contentsline {section}{\numberline {4.17}Line Class Reference}{26}{section.4.17}
\contentsline {subsection}{\numberline {4.17.1}Detailed Description}{27}{subsection.4.17.1}
\contentsline {section}{\numberline {4.18}Logger Class Reference}{27}{section.4.18}
\contentsline {section}{\numberline {4.19}Map Class Reference}{27}{section.4.19}
\contentsline {subsection}{\numberline {4.19.1}Member Function Documentation}{28}{subsection.4.19.1}
\contentsline {subsubsection}{\numberline {4.19.1.1}is\discretionary {-}{}{}Bound}{28}{subsubsection.4.19.1.1}
\contentsline {section}{\numberline {4.20}Message Class Reference}{28}{section.4.20}
\contentsline {subsection}{\numberline {4.20.1}Member Function Documentation}{28}{subsection.4.20.1}
\contentsline {subsubsection}{\numberline {4.20.1.1}load\discretionary {-}{}{}From}{28}{subsubsection.4.20.1.1}
\contentsline {subsubsection}{\numberline {4.20.1.2}load\discretionary {-}{}{}To}{29}{subsubsection.4.20.1.2}
\contentsline {section}{\numberline {4.21}Network Class Reference}{29}{section.4.21}
\contentsline {subsection}{\numberline {4.21.1}Member Function Documentation}{30}{subsection.4.21.1}
\contentsline {subsubsection}{\numberline {4.21.1.1}connect\discretionary {-}{}{}To\discretionary {-}{}{}Host}{30}{subsubsection.4.21.1.1}
\contentsline {subsubsection}{\numberline {4.21.1.2}connect\discretionary {-}{}{}To\discretionary {-}{}{}Host}{30}{subsubsection.4.21.1.2}
\contentsline {subsubsection}{\numberline {4.21.1.3}connect\discretionary {-}{}{}To\discretionary {-}{}{}Peer}{30}{subsubsection.4.21.1.3}
\contentsline {subsubsection}{\numberline {4.21.1.4}create\discretionary {-}{}{}Node}{30}{subsubsection.4.21.1.4}
\contentsline {subsubsection}{\numberline {4.21.1.5}find}{31}{subsubsection.4.21.1.5}
\contentsline {subsubsection}{\numberline {4.21.1.6}same\discretionary {-}{}{}\_\discretionary {-}{}{}addr}{31}{subsubsection.4.21.1.6}
\contentsline {subsubsection}{\numberline {4.21.1.7}send\discretionary {-}{}{}To\discretionary {-}{}{}All}{31}{subsubsection.4.21.1.7}
\contentsline {subsubsection}{\numberline {4.21.1.8}send\discretionary {-}{}{}To\discretionary {-}{}{}Peer}{31}{subsubsection.4.21.1.8}
\contentsline {section}{\numberline {4.22}Network\discretionary {-}{}{}Player Class Reference}{31}{section.4.22}
\contentsline {subsection}{\numberline {4.22.1}Member Function Documentation}{33}{subsection.4.22.1}
\contentsline {subsubsection}{\numberline {4.22.1.1}event\discretionary {-}{}{}Connect}{33}{subsubsection.4.22.1.1}
\contentsline {subsubsection}{\numberline {4.22.1.2}event\discretionary {-}{}{}Disconnected}{33}{subsubsection.4.22.1.2}
\contentsline {subsubsection}{\numberline {4.22.1.3}event\discretionary {-}{}{}Receive}{33}{subsubsection.4.22.1.3}
\contentsline {subsubsection}{\numberline {4.22.1.4}init}{34}{subsubsection.4.22.1.4}
\contentsline {subsubsection}{\numberline {4.22.1.5}process\discretionary {-}{}{}Event}{34}{subsubsection.4.22.1.5}
\contentsline {subsubsection}{\numberline {4.22.1.6}push\discretionary {-}{}{}Command}{34}{subsubsection.4.22.1.6}
\contentsline {subsubsection}{\numberline {4.22.1.7}send\discretionary {-}{}{}To\discretionary {-}{}{}All}{34}{subsubsection.4.22.1.7}
\contentsline {subsubsection}{\numberline {4.22.1.8}send\discretionary {-}{}{}To\discretionary {-}{}{}Peer}{34}{subsubsection.4.22.1.8}
\contentsline {subsubsection}{\numberline {4.22.1.9}start}{34}{subsubsection.4.22.1.9}
\contentsline {subsubsection}{\numberline {4.22.1.10}update\discretionary {-}{}{}New\discretionary {-}{}{}Peer}{35}{subsubsection.4.22.1.10}
\contentsline {section}{\numberline {4.23}Node Struct Reference}{35}{section.4.23}
\contentsline {section}{\numberline {4.24}Peer\discretionary {-}{}{}Data Struct Reference}{35}{section.4.24}
\contentsline {section}{\numberline {4.25}Physics Class Reference}{35}{section.4.25}
\contentsline {subsection}{\numberline {4.25.1}Detailed Description}{36}{subsection.4.25.1}
\contentsline {section}{\numberline {4.26}Pudge Class Reference}{36}{section.4.26}
\contentsline {subsection}{\numberline {4.26.1}Constructor \& Destructor Documentation}{37}{subsection.4.26.1}
\contentsline {subsubsection}{\numberline {4.26.1.1}Pudge}{37}{subsubsection.4.26.1.1}
\contentsline {subsubsection}{\numberline {4.26.1.2}$\sim $\discretionary {-}{}{}Pudge}{37}{subsubsection.4.26.1.2}
\contentsline {subsection}{\numberline {4.26.2}Member Function Documentation}{37}{subsection.4.26.2}
\contentsline {subsubsection}{\numberline {4.26.2.1}apply\discretionary {-}{}{}Move}{37}{subsubsection.4.26.2.1}
\contentsline {subsubsection}{\numberline {4.26.2.2}apply\discretionary {-}{}{}Rot}{37}{subsubsection.4.26.2.2}
\contentsline {subsubsection}{\numberline {4.26.2.3}clone}{37}{subsubsection.4.26.2.3}
\contentsline {subsubsection}{\numberline {4.26.2.4}die}{38}{subsubsection.4.26.2.4}
\contentsline {subsubsection}{\numberline {4.26.2.5}draw}{38}{subsubsection.4.26.2.5}
\contentsline {subsubsection}{\numberline {4.26.2.6}get\discretionary {-}{}{}Cool\discretionary {-}{}{}Down}{38}{subsubsection.4.26.2.6}
\contentsline {subsubsection}{\numberline {4.26.2.7}get\discretionary {-}{}{}H\discretionary {-}{}{}P}{38}{subsubsection.4.26.2.7}
\contentsline {subsubsection}{\numberline {4.26.2.8}inflict\discretionary {-}{}{}Damage}{38}{subsubsection.4.26.2.8}
\contentsline {subsubsection}{\numberline {4.26.2.9}init}{38}{subsubsection.4.26.2.9}
\contentsline {subsubsection}{\numberline {4.26.2.10}load\discretionary {-}{}{}From}{38}{subsubsection.4.26.2.10}
\contentsline {subsubsection}{\numberline {4.26.2.11}load\discretionary {-}{}{}To}{39}{subsubsection.4.26.2.11}
\contentsline {subsubsection}{\numberline {4.26.2.12}process\discretionary {-}{}{}Command}{39}{subsubsection.4.26.2.12}
\contentsline {subsubsection}{\numberline {4.26.2.13}respawn}{39}{subsubsection.4.26.2.13}
\contentsline {subsubsection}{\numberline {4.26.2.14}stop}{39}{subsubsection.4.26.2.14}
\contentsline {subsubsection}{\numberline {4.26.2.15}update}{39}{subsubsection.4.26.2.15}
\contentsline {section}{\numberline {4.27}Threaded\discretionary {-}{}{}Entity Class Reference}{39}{section.4.27}
\contentsline {section}{\numberline {4.28}Threaded\discretionary {-}{}{}Job Class Reference}{40}{section.4.28}
\contentsline {section}{\numberline {4.29}Timer Class Reference}{40}{section.4.29}
\contentsline {subsection}{\numberline {4.29.1}Detailed Description}{40}{subsection.4.29.1}
\contentsline {subsection}{\numberline {4.29.2}Member Function Documentation}{41}{subsection.4.29.2}
\contentsline {subsubsection}{\numberline {4.29.2.1}is\discretionary {-}{}{}Timeout}{41}{subsubsection.4.29.2.1}
\contentsline {section}{\numberline {4.30}U\discretionary {-}{}{}I Class Reference}{41}{section.4.30}
\contentsline {section}{\numberline {4.31}Window\discretionary {-}{}{}:\discretionary {-}{}{}:Ui Struct Reference}{41}{section.4.31}
\contentsline {section}{\numberline {4.32}vector3 Class Reference}{41}{section.4.32}
\contentsline {subsection}{\numberline {4.32.1}Detailed Description}{42}{subsection.4.32.1}
\contentsline {subsection}{\numberline {4.32.2}Constructor \& Destructor Documentation}{42}{subsection.4.32.2}
\contentsline {subsubsection}{\numberline {4.32.2.1}vector3}{42}{subsubsection.4.32.2.1}
\contentsline {subsection}{\numberline {4.32.3}Member Function Documentation}{43}{subsection.4.32.3}
\contentsline {subsubsection}{\numberline {4.32.3.1}mag}{43}{subsubsection.4.32.3.1}
\contentsline {subsubsection}{\numberline {4.32.3.2}magsq}{43}{subsubsection.4.32.3.2}
\contentsline {subsubsection}{\numberline {4.32.3.3}normalize}{43}{subsubsection.4.32.3.3}
\contentsline {subsubsection}{\numberline {4.32.3.4}normalized}{43}{subsubsection.4.32.3.4}
\contentsline {subsubsection}{\numberline {4.32.3.5}operator$\ast $=}{43}{subsubsection.4.32.3.5}
\contentsline {subsubsection}{\numberline {4.32.3.6}operator$\ast $=}{43}{subsubsection.4.32.3.6}
\contentsline {subsubsection}{\numberline {4.32.3.7}operator+}{44}{subsubsection.4.32.3.7}
\contentsline {subsubsection}{\numberline {4.32.3.8}operator+=}{44}{subsubsection.4.32.3.8}
\contentsline {subsubsection}{\numberline {4.32.3.9}operator-\/}{44}{subsubsection.4.32.3.9}
\contentsline {subsubsection}{\numberline {4.32.3.10}operator-\/=}{44}{subsubsection.4.32.3.10}
\contentsline {subsubsection}{\numberline {4.32.3.11}operator/}{44}{subsubsection.4.32.3.11}
\contentsline {subsubsection}{\numberline {4.32.3.12}operator/=}{44}{subsubsection.4.32.3.12}
\contentsline {subsubsection}{\numberline {4.32.3.13}operator$^\wedge $}{45}{subsubsection.4.32.3.13}
\contentsline {subsubsection}{\numberline {4.32.3.14}print}{45}{subsubsection.4.32.3.14}
\contentsline {section}{\numberline {4.33}Window Class Reference}{45}{section.4.33}
\contentsline {section}{\numberline {4.34}Worker\discretionary {-}{}{}Thread Class Reference}{46}{section.4.34}

#version 330

in vec3 frag_TexCoord;

uniform samplerCube gColorMap;

out vec4 FragColor;

void main()
{
    FragColor = textureCube(gColorMap,
      frag_TexCoord);
    // FragColor.x=0.5;
}

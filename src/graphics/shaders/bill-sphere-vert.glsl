#version 330

in vec3 Position;
in vec2 TexCoord;
in vec3 Normal;

out vec2 frag_TexCoord;
out vec3 frag_Normal;


uniform mat4 gMV=mat4(1.0);
uniform mat4 gP=mat4(1.0);
uniform mat4 gWorld=mat4(1.0);

void main()
{
    vec4 PosL = vec4(Position,1.0);
    mat4 billMV=gMV;
    billMV[0].xyz=vec3(1,0,0);
    billMV[1].xyz=vec3(0,1,0);
    billMV[2].xyz=vec3(0,0,1);
    gl_Position = gP* billMV * PosL;
    frag_TexCoord = TexCoord;
    vec4 NormalL =  vec4(Normal, 0.0);
    frag_Normal = (gWorld * NormalL).xyz;
}
#include "testnetwork.h"
#include "timer.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>

using namespace std;
	NetworkPlayer playa;

int main(){

	srand(0);
	Timer timer,recon_timer;
	string str="";
	string client_port = "";
	string host_ip = "";
	string host_port = "";
	cout << "host/client : " ;
	cin >> str;
	int max= 3;
	if(str == "h")
	{
		// cout << "Specify port : ";
		// cin >> client_port;
		// int lport=atoi(client_port.c_str());
		// str = "";
		cout<<"enter your ip";
		//cin>>ip;
		ip = "10.251.216.106";
		playa.init(true,"",0);
		

	}
	else if(str == "c")
	{
		string str;
		//cout<<"enter ip : ";
		
		//cin>>str;
		//cout << "Specify  port : ";

		//cin >> client_port;
		client_port = "1111";
		playa.init(false,"10.251.216.106", atoi(client_port.c_str()));

	}
	else{
		cout << "invalid operation!" << endl;
		exit(0);
	}
	bool flag=1;
	timer.start(50);
	while(1){
		// cout<<"processing"<<endl;
		// usleep(500);
		playa.processEvent();
		if(playa.connectedPeers() == 3 && !playa.ingame && playa.isHost){
			usleep(100000);
			playa.start();
			cout<<"started"<<endl;
		}
		if(playa.ingame){
			if(timer.isTimeout()){
				//cout<<"pushed"<<endl;
				Command cmd;
			int choice = rand()%100;
			if(choice < 50)cmd.init(playa.my_data.id,0);
			else cmd.init(playa.my_data.id,1);
			playa.pushCommand(cmd);
			timer.start(50);
			}
			//cout<<"sending"<<int(cmd.head.id)<<endl;
			//cout<<"pushed"<<endl;
			playa.sync();
		}
		if(playa.isDC && !playa.isRC){
			cout<<"Enter 1 to reconnect"<<endl;
			int n;
			cin>>n;
			cout<<n<<" --"<<endl;
			if(n == 1){
				logInfo("trying to reconnect\n");

				recon_timer.start(5000);

				playa.isRC = true;
				playa.reconnect();
			}
		}
		if(playa.isRC){
			if(recon_timer.isTimeout()){
				logInfo("failed to connect to the tried peer... trying the next peer");
				playa.peer_to_try++;
				playa.reconnect();

				recon_timer.start(5000);
			}
		}
	}
	return 0;
}
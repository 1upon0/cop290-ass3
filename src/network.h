#pragma once
#include <iostream>
#include <vector>
#include <cstring>
#include <cstdlib>
#include "node2.h"
#include "timer.h"
#include "command.h"
#include "gameEngine.h"
#include "log.h"
#include "ai.h"
AI ai;
int is_ai = 0;

#define TN 150
bool fakka = true;
string ip;


class NetworkPlayer{
public:
	//uint32_t id;

	bool received_pause,failed_reconnection;
	bool ingame,isDC,checkcc,isRC,retry,get_state,countdown;
	PeerData my_data;
	int n_peers;
	Network network_player;
	Timer network_timer;
	vector<Command> my_commands;
	vector<Command> peer_commands;
	vector<int> check;
	uint32_t peer_to_try;
	uint64_t start_time;
	ENetPeer *recon_peer;	// the peer i am trying to reconnect to
	//state=1 : active
	//state=0 : dead
	//pair<int,int>* check;
	NetworkPlayer();
	string intToIP(uint32_t);
	string getIP(uint32_t);
	void init(bool,string,uint16_t);
	void start(uint64_t);
	bool isHost;
	uint32_t maxID;// relevant only if isHost is true
	bool checkID(uint32_t id);
	void sendToPeer(Message *message,ENetPeer* peer);
	void sendToAll(Message *message);
	void sendCommandToAll();
	int processEvent();
	bool checkArray();
	void sync();
	void getState();
	void pushCommand(Command);
	void reconnect();
	int eventConnect(ENetEvent &event);
	int eventReceive(ENetEvent &event);
	int eventDisconnected(ENetEvent &event);
	void updateNewPeer(ENetPeer* peer,uint32_t id);
		int connectedPeers(){
		//assert(node != 0 && "size of peers of null node");
		int cnt = 0;
		for(auto& peer:network_player.peers){
			if(peer.second.state == 2)cnt++;
		}
		return cnt;
	}
	int totalPeers(){
		return network_player.peers.size();
	}
	void checkConnected();
	void sendPauseMessage();
};
NetworkPlayer::NetworkPlayer(){
	my_commands.resize(0);
	peer_commands.resize(0);
	check.resize(0);
	//check = NULL;
	ingame = false;
	maxID = 1;
	isDC = false;
	checkcc = false;
	n_peers =0;
	isRC = false;
	retry = false;
	peer_to_try = 0;
	get_state = false;
	start_time = 0;
	received_pause=false;
	failed_reconnection=false;
}

string NetworkPlayer::intToIP(uint32_t paramInt)
{
     //vector<unsigned char> arrayOfByte(4);
     string s;
     for (int i = 0; i < 4; i++){
         unsigned char ch =(paramInt >> (i * 8));
     	
     	s += to_string(int(ch));
     	//cout<<s<<endl;
     	if(i<3)
     	s+=".";
     }  
     return s;
}
string NetworkPlayer::getIP(uint32_t id){
	cout<<"asked for ip of id "<<id<<endl;
	if(id == my_data.id){
		return "YOU";
	}
	for(auto& peer:network_player.peers){
		if(peer.second.id == id){
			string str=intToIP(peer.first.host);
			cout<<"found "<<str<<endl;
			str += ":";
			str += to_string(peer.first.port);
			return str;
		}
	}
	cout<<"not found"<<endl;
	return "";
}
bool NetworkPlayer::checkID(uint32_t id){
	for(auto& peer:network_player.peers){
		PeerData pdata = peer.second;
		if(pdata.id == id){
			return false;
		}
	}	
	return true;
}

/**
 * @brief send the new peer the list of connected peers
 * 
 * @param peer [description]
 * @param id [description]
 */
void NetworkPlayer::updateNewPeer(ENetPeer* peer,uint32_t id){
	AddressMessage AM;
	vector<pair<pair<uint32_t,uint16_t>,uint32_t> > peer_list;
	pair<uint32_t,uint16_t> temp;
	for(auto& peer1:network_player.peers){
		if(peer1.second.state == 0)continue;
		if(network_player.same_addr(peer1.first,peer->address)) continue;
		temp = make_pair(peer1.first.host,peer1.first.port);
		ENetAddress addr;enet_address_set_host(&addr,ip.c_str());
		if(temp.first == 16777343)temp.first = addr.host;
		logInfo(ip.c_str());
		logInfo("assigned%d",temp.first);
		peer_list.push_back(make_pair(temp,peer1.second.id));
	}
	AM.init(peer_list,id);
	sendToPeer(&AM,peer);
}

/**
 * @brief push new command to my_ommands vector
 * 
 * @param cmd [description]
 */
void NetworkPlayer::pushCommand(Command cmd){
	my_commands.push_back(cmd);
	//cout<<my_commands.size()<<endl;
}

/**
 * @brief send message to a peer if connected
 * 
 * @param message [description]
 * @param peer [description]
 */
void NetworkPlayer::sendToPeer(Message *message,ENetPeer *peer){
	int idx=network_player.find(peer->address);
	assert(idx!=-1 && "peer not present");
	if(network_player.peers[idx].second.state == 0){cout<<"peer not active, not sending message"; return;}
	vector<char> buff;
	message->loadTo(buff);
	network_player.sendToPeer(buff,peer);
}


/**
 * @brief send message to all connected peers
 * 
 * @param message [description]
 */
void NetworkPlayer::sendToAll(Message *message){
	vector<char> buff;
	message->loadTo(buff);
	network_player.sendToAll(buff);
}


bool NetworkPlayer::checkArray(){
	int flag=1;
	//Checks the messages of the active peer
	for(int i=0;i<network_player.peers.size();i++){
		if(network_player.peers[i].second.state==0)continue; //ignore dc'ed peers
		if(check.at(network_player.peers[i].second.id) == 0){
		//	cout<<"culprit"<<int(i)<<endl;
			flag=0;
			break;
		}
		//cout<<int(network_player.peers[i].second.id)<<endl;
	}
	return flag;
}


bool compare(Command c1,Command c2){
	return c1.timestamp < c2.timestamp;
}

/**
 * @brief sync all the players
 */
void NetworkPlayer::sync(){

	if(checkArray() || network_timer.isTimeout()){
		if(network_timer.isTimeout())cout<<"timeout sending my commmands"<<endl;
		sendCommandToAll();
		//TODO - send the peer_commands to the game-engine
		//	cout<<"peer cmds"<<peer_commands.size()<<endl;
		peer_commands.insert(peer_commands.end(),my_commands.begin(),my_commands.end());
		my_commands.clear(); // safe :)
		sort(peer_commands.begin(),peer_commands.end(),compare);
		//GameEngine::_()->applyCommand(peer_commands);
		for(auto& cmd:peer_commands){
			//cout<<int(cmd.id)<<":"<<cmd.type<<endl;
			if(cmd.id == 4)
				cout<<"got it"<<endl;
		}
		GameEngine::_()->virtualSimulation(peer_commands,Timer::now());
	//	cout<<"-------------------------------------------------------"<<endl;
		peer_commands.clear();
		fill(check.begin(),check.end(),0);
		fakka = false;
		network_timer.start(TN);
	}
}

/**
 * @brief create message from all the commands queued and send message to all players
 */
void NetworkPlayer::sendCommandToAll(){
	//Loading the command to buffer
	//cout<<"s"<<endl;
	CmdMessage cm;
	cm.init(my_commands,my_data.id);
	//cout<<cm.commands.size()<<"aaaaaaa"<<my_commands.size()<<endl;
	sendToAll(&cm);
}

/**
 * @brief initialize a netwwork connection
 * 
 * @param _isHost [description]
 * @param host_ip [description]
 * @param host_port [description]
 */
void NetworkPlayer::init(bool _isHost,string host_ip,uint16_t host_port){
	network_player.init();
	network_player.createNode();
	isHost = _isHost;
	my_data.state = 1;
	if(!isHost){
		network_player.connectToHost(host_ip,host_port);
	}
	else my_data.id = 1;
	//now you should start processing events

}

/**
 * @brief process an event triggered by a peer
 * @return [description]
 */
int NetworkPlayer::processEvent(){
	ENetEvent event;
	int rv = 0;
	if(enet_host_service(network_player.node->self, &event, 0)>0){
		//if(isDC) return 0;
		switch(event.type){
		case ENET_EVENT_TYPE_CONNECT:
			rv = eventConnect(event);
			break;
		case ENET_EVENT_TYPE_RECEIVE:
			rv = eventReceive(event);
			enet_packet_destroy(event.packet);
			break;
		case ENET_EVENT_TYPE_DISCONNECT:
			rv = eventDisconnected(event);
			break;
		default:cout<<"???"<<endl;
			break;
		}
		assert(rv>=0 && "Error in event handling!!");
	}
	return rv;
}

/**
 * @brief start game after max. players allowed are connected
 * 
 * @param _start_time [description]
 */
void NetworkPlayer::start(uint64_t _start_time){
	GameEngine::_()->init(connectedPeers()+1);
	start_time = _start_time;
	countdown = true;
	if(is_ai){
		cout<<"Bot is assigned"<<endl;
		ai.bot=GameEngine::_()->getPudge(my_data.id,&(GameEngine::_()->original));
	}
	for(int i=0;i<GameEngine::_()->original.head.n;i++){
		if(GameEngine::_()->original.v[i]->head.type == HOOK_OBJ && ((Hook*)(GameEngine::_()->original.v[i]))->pudge==NULL)
			cout<<"It is NULL indeed"<<endl;
	}

	//network_timer.start(tn);
	cout<<"to start at "<<start_time<<endl;
	check.resize(totalPeers()+2);
	for(int i=0;i<totalPeers();i++){
		check.at(i) = 1;
	}

	// send a start game message to all peers
	if(isHost){
		Message m;
		m.head.id = my_data.id;
		m.head.r1 = 69;
		m.head.r3 = _start_time;
		sendToAll(&m);
	}
	network_timer.start(TN);
}

/**
 * @brief connection event triggered by a peer
 * 
 * @param event [description]
 * @return [description]
 */
int NetworkPlayer::eventConnect(ENetEvent &event){
	cout<<"cn"<<endl;
	int idx = network_player.find(event.peer->address);
	if(idx != -1){
		//old peer
		//cout<<idx<<" "<<network_player.peers[idx].second.state<<endl;
		assert(network_player.peers[idx].second.state==0 && "Peer already connected,unexpected behaviour!");
	}
	if(event.data == 0){
		//echo
		assert(idx != -1 && "echo, should be already in the list");
		network_player.peers[idx].second.state = 2;
		cout<<"Connection to :"<<network_player.peers[idx].first.host<<" " <<event.peer->address.host<<" has completed"<<endl;
		if(checkcc){
			checkConnected();
		}
		if(isRC){
			isRC = false;
			isDC = false;
		}
	}
	else{
		//a connection request
		if(idx == -1){// new guy
			// if i am the host, i dont expect any id in the event data, rather i have to assign an id
			cout<<"New guy"<<endl;
			if(isHost){
				cout<<"giving him an id: "<<maxID+1<<endl;
				PeerData new_peer;
				new_peer.id = maxID+1;maxID++;
				new_peer.state = 1;
				network_player.peers.push_back(make_pair(event.peer->address,new_peer));
				//send message telling the new peer about his id
				updateNewPeer(event.peer,new_peer.id);

				cout<<"sent the list"<<endl;
			}
			else{
				assert(checkID(event.data) && event.data >0 && "invalid id");		//id should not have been assigned to a peer
				PeerData new_peer;
				new_peer.id = event.data;
				new_peer.state = 1;
				network_player.peers.push_back(make_pair(event.peer->address,new_peer));
				//cout<<connectedPeers()<<" "<<check.size()<<endl;
				cout<<"told me his id is :"<<event.data<<endl;
			}

		}
		else{
			network_player.peers[idx].second.state = 1;
			cout<<network_player.peers[idx].second.id<<" asked to reconnect"<<endl;
			if(event.data == 100){

				//aww i am the first one he reconnected with

				//give him the list of all the peers to connect to
				updateNewPeer(event.peer,network_player.peers[idx].second.id);
				cout<<"sent the list"<<endl;
			}
			cout<<"Connected to :"<<network_player.peers[idx].first.port<<"again :))"<<endl;
		}

	}
	enet_peer_timeout(event.peer,32,500,1000);
	enet_peer_ping_interval(event.peer,70);
	return 0;
	//cout<<"connected to "<<event.peer.address.port<<endl;
}

/**
 * @brief recieve event triggered
 * @details [long description]
 * 
 * @param event [description]
 * @return [description]
 */
int NetworkPlayer::eventReceive(ENetEvent &event){
	//TODO
	//cout<<"rec"<<endl;
	vector<char> buff((char*)event.packet->data,(char*)event.packet->data+event.packet->dataLength);
	Message msg;
	msg.loadFrom(buff);

	switch(msg.head.type){
		case CMD:{
			//cout<<"command"<<endl;
			//cout<<"rec cmd"<<endl;
			//cout<<"r";
			CmdMessage cm;
			//cout<<"loading to buff"<<endl;
			cm.loadFrom(buff);
			for(auto& command:cm.commands){
				peer_commands.push_back(command);
			}
			//assert(m.head.id<=connectedPeers() && "Wrong id in the message");
			//cout<<int(cm.head.id)<<" "<<check.size()<<endl;
			check[cm.head.id]=1;

			break;
		}
		case ADDRESS:{
				cout<<"address"<<endl;
				AddressMessage m;
				m.loadFrom(buff);
				cout<<"received a list of peers to connect to"<<endl;
				cout<<"total "<<m.v.size()<<" peers to connect to"<<endl;
				n_peers = m.v.size() +1;
				checkcc = true;
				if(checkcc){
					checkConnected();
				}
				ENetAddress address;
				for(int i=0;i<m.v.size();i++){
					address.host = m.v[i].first.first;
					address.port = m.v[i].first.second;
					network_player.connectToPeer(address,m.head.id,m.v[i].second);
				}
				my_data.id =  m.head.id;
				break;
		}
		case SIMPLE:{

				if(msg.head.r1 ==69){
					cout<<"received start message"<<endl;
					start(msg.head.r3);
				}
				if(msg.head.r1 == 88){
					cout<<"someone completed their connection"<<endl;
					int idx = network_player.find(event.peer->address);
					assert(idx != -1 && "invalid complete connection msg");
					network_player.peers[idx].second.state = 2;

				}
				if(msg.head.r1 == 99){
					//send the game state
					//TODO replace this with an actual Game State message
					DataMessage dat;
					vector<char> buff;
					GameEngine::_()->copy.loadTo(buff);

					//dat.head.id = my_data.id;
					dat.init(buff,my_data.id);
					dat.head.r3 = 111;
					// Message m;
					// m.head.id = my_data.id;
					// m.head.r1 = 111;

					sendToPeer(&dat,event.peer);
				}
				if(msg.head.r1==100){
					//TODO pause
					GameEngine::_()->is_pause = !(GameEngine::_()->is_pause);
					received_pause=true;
				}

				break;
		}
		case DATA:{

				if(msg.head.r3 == 111){
					//now my game can be resumed again
					// TODO replace my game state with the one i received

					DataMessage dat;
					dat.loadFrom(buff);
					GameEngine::_()->copy.loadFrom(dat.v);
					GameEngine::_()->original.loadFrom(dat.v);
					cout<<"received game state... resuming my game"<<endl;
					//clear all the vectors
					peer_commands.clear();
					my_commands.clear();
					//isDC = false;
					ingame = true;
				}
		}
	}

	return 0;
}

/**
 * @brief disconnection event triggered
 * @details [long description]
 * 
 * @param event [description]
 * @return [description]
 */
int NetworkPlayer::eventDisconnected(ENetEvent &event){

	int idx = network_player.find(event.peer->address);
	assert(idx != -1 && "dc form a peer not in the list");
	int cnnt = connectedPeers();
	network_player.peers[idx].second.state = 0;
	//enet_peer_reset(event.peer);
	cout<<"dced from"<<" "<<network_player.peers[idx].second.id<<endl;
	if(isRC)return 0;
	if(connectedPeers() == 0){
		if(cnnt==1 && isHost) return 0;
		cout<<"disconnected from everyone... dcing myself"<<endl;
		isDC = true;
		ingame=false;
		isHost = false;

		peer_to_try = 0;
		get_state = true;
	}
	
	

	//exit(1);
}

/**
 * @brief reconnect an old peer
 */
void NetworkPlayer::reconnect(){
	if(peer_to_try >= totalPeers()){
		logInfo("out of peers to try to connect to");
		isRC = false;
		peer_to_try = 0;
		failed_reconnection = true;
		return;
	}

	cout<<"Now i will try to reconnect :)"<<endl;

	ENetAddress addr=network_player.peers[peer_to_try].first;
	cout<<"[Peer]Sending reconnection request to peer:";
	recon_peer = enet_host_connect(network_player.node->self,&addr,2,100);
	assert(recon_peer != NULL && "Could not connect to host!");
	cout<<recon_peer->address.port<<endl;

	enet_peer_timeout(recon_peer,32,300,500);
	enet_peer_ping_interval(recon_peer,50);
}

/**
 * @brief send pause message to all the peers
 */
void NetworkPlayer::sendPauseMessage(){
	Message m;	//simple message
	m.init(my_data.id);	//100 means pause
	m.head.r1=100;
	sendToAll(&m);
}
void NetworkPlayer::getState(){


for(auto& peer:network_player.peers){
		if(peer.second.state == 2){
			Message m;
			m.head.id = my_data.id;
			m.head.r1 = 99;
			ENetAddress addr = peer.first;
			for(int i=0;i<network_player.node->self->connectedPeers;i++){
				ENetPeer *epeer = network_player.node->self->peers+i;
				if(network_player.same_addr(epeer->address,addr))
				sendToPeer(&m,epeer);
				break;
			}
			break;
		}
	}
}
/**
 * @brief check if the player is connected to all other peers
 */
void NetworkPlayer::checkConnected(){

	if(n_peers == connectedPeers()){
		cout<<"completed"<<endl;
		Message m;
		m.head.id = my_data.id;
		m.head.r1 = 88;
		sendToAll(&m);
		checkcc = false;
		if(get_state){
			//ask any peer for the game state
			for(auto& peer:network_player.peers){
				if(peer.second.state == 2){
					Message m;
					m.head.id = my_data.id;
					m.head.r1 = 99;
					ENetAddress addr = peer.first;
					for(int i=0;i<network_player.node->self->connectedPeers;i++){
						ENetPeer *epeer = network_player.node->self->peers+i;
						if(network_player.same_addr(epeer->address,addr))
						sendToPeer(&m,epeer);
						break;
					}
					break;
				}
			}
		}
	}
}
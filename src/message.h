#pragma once
#include <iostream>
#include <cstring>
#include <cctype>
#include <cstdint>
#include <cassert>
#include <vector>
#include <algorithm>
#include "command.h"
using namespace std;

#define SIMPLE 0
#define DATA 1
#define CHECK 2
#define ADDRESS 3
#define CMD 4


class Message{
public:
	struct Header{
		uint32_t id;
		uint32_t r1,r2;
		uint64_t r3;
		uint32_t type;
	};
	Header head;
	Message(){
		head.type=SIMPLE;
		init(0);
	}
	void init(uint32_t _id){
		head.id = _id;
		head.r1=head.r2=head.r3=0;
	}

	/**
	 * @brief Load to buffer
	 * @details Loads the object into the buffer. This buffer is then passed over the network
	 * 
	 * @param buff vector<char> buffer
	 */
  	virtual void loadTo(vector<char>& buff){
  		buff.resize(sizeof(head));
  		memcpy(buff.data(),(char*)&head,sizeof(head));
	}

	/**
	 * @brief Loading from the vector<char>
	 * @details Loads the object from the buffer. This buffer is received over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	virtual void loadFrom(const vector<char>& buff){
    	assert(buff.size() >= sizeof(head));
		memcpy((char*)&head,buff.data(),sizeof(head));
	}
};


class CmdMessage : public Message{
public:
	vector<Command> commands;
	CmdMessage(){
		commands.resize(0);
	}
	void init(vector<Command> & _commands,uint32_t _id){
		commands = _commands;
		head.id = _id;
		head.type = CMD;
		head.r1 = commands.size();
	}

	/**
	 * @brief Load to buffer
	 * @details Loads the object into the buffer. This buffer is then passed over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	virtual void loadTo(vector<char>& buff){
		buff.resize(sizeof(head)+sizeof(Command)*commands.size());
		memcpy(buff.data(),(char*)&head,sizeof(head));
		for(int i=0;i<commands.size();i++){
			memcpy(buff.data()+sizeof(head)+i*sizeof(Command),(char*)&commands[i],sizeof(Command));
		}
	} 

	/**
	 * @brief Loading from the vector<char>
	 * @details Loads the object from the buffer. This buffer is received over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	virtual void loadFrom(vector<char>& buff){
		assert(buff.size() >= sizeof(head));
		memcpy((char*)&head,buff.data(),sizeof(head));
		assert(buff.size()>= sizeof(head)+head.r1*sizeof(Command));
		commands.resize(head.r1);
		memcpy(commands.data(),buff.data()+sizeof(head),(head.r1)*sizeof(Command));
	}
};


class DataMessage : public Message{
public:
	vector<char> v;
	DataMessage(){
		head.type = DATA;
	}
	void init(vector<char>& _v,uint32_t _id,uint32_t n_command=0){
		head.id = _id;
		v = _v;
		head.r1=_v.size();
		head.r2=n_command;
		head.r3=0;
	}

	/**
	 * @brief Load to buffer
	 * @details Loads the object into the buffer. This buffer is then passed over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	virtual void loadTo(vector<char>& buff){
		buff.resize(sizeof(head) + v.size());
		memcpy(buff.data(),(char*)&head,sizeof(head));
		copy(v.data(),v.data()+v.size(),buff.data()+sizeof(head));
	}

	/**
	 * @brief Loading from the vector<char>
	 * @details Loads the object from the buffer. This buffer is received over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	virtual void loadFrom(const vector<char>& buff){
		assert(buff.size() >= sizeof(head));
		memcpy((char*)&head,buff.data(),sizeof(head));
		v.resize(head.r1);
		copy(buff.data()+sizeof(head),buff.data()+sizeof(head) + v.size(),v.data());
	}
};


class CheckMessage : public Message{
public:
	char check[1000];
	CheckMessage(){
		head.type = CHECK;
		check[0]='\0';
	}
	void init(string _check,uint32_t _id){
		head.id=_id;
		strcpy(check,_check.c_str());
		head.r1=head.r2=head.r3=0;
	}

	/**
	 * @brief Load to buffer
	 * @details Loads the object into the buffer. This buffer is then passed over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	virtual void loadTo(vector<char>& buff){
		buff.resize(sizeof(head) + sizeof(check));
		memcpy(buff.data(),(char*)&head,sizeof(head));
		copy(check,check+sizeof(check),buff.data()+sizeof(head));
	}

	/**
	 * @brief Loading from the vector<char>
	 * @details Loads the object from the buffer. This buffer is received over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	virtual void loadFrom(const vector<char>& buff){
		assert(buff.size() >= sizeof(head)+sizeof(check));
		memcpy((char*)&head,buff.data(),sizeof(head));
		copy(buff.data()+sizeof(head),buff.data()+sizeof(head)+sizeof(check),check);
	}
};
class AddressMessage : public Message{
public:
	vector< pair<pair<uint32_t,uint16_t>,uint32_t> > v;
	AddressMessage(){
		head.type=ADDRESS;
		v.resize(0);
	}
	void init(vector<pair< pair<uint32_t,uint16_t>,uint32_t> >& _v,uint32_t _id){
		head.id = _id;
		v = _v;
		head.r1=_v.size();
		head.r2=head.r3=0;
	}

	/**
	 * @brief Load to buffer
	 * @details Loads the object into the buffer. This buffer is then passed over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	virtual void loadTo(vector<char>& buff){
		buff.resize(sizeof(head) + v.size()*sizeof(pair<pair<uint32_t,uint16_t>,uint32_t>));
		memcpy(buff.data(),(char*)&head,sizeof(head));
		char* buf = buff.data() + sizeof(head);
		for(auto addr : v){
			memcpy(buf,(char*)&addr,sizeof(pair<pair<uint32_t,uint16_t>,uint32_t>));
			buf += sizeof(pair<pair<uint32_t,uint16_t>,uint32_t>);
		}
	}

	/**
	 * @brief Loading from the vector<char>
	 * @details Loads the object from the buffer. This buffer is received over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	virtual void loadFrom(const vector<char>& buff){
		assert(buff.size()>=sizeof(head));
		memcpy((char*)&head,buff.data(),sizeof(head));
		char* buf = (char*)buff.data() + sizeof(head);
		v.resize(head.r1);
		for(int i=0;i<head.r1;i++){
			v[i] = *(pair<pair<uint32_t,uint16_t>,uint32_t>*)(buf);
			buf += sizeof(pair<pair<uint32_t,uint16_t>,uint32_t>);
		}
	}
};
#include "../message.h"

int main(){
	vector<char> buff;

	cout<<"Testing Message"<<endl;
	Message m1,_m1;
	m1.init(123);
	m1.loadTo(buff);	
	_m1.loadFrom(buff);
	cout<<"Id:"<<_m1.head.id<<endl;

	// cout<<"\nTesting DataMessage"<<endl;
	// DataMessage m2,_m2;
	// string str2 = "This is a command";
	// vector<char> v(str2.begin(),str2.end());
	// m2.init(v,123);
	// m2.loadTo(buff);
	// _m2.loadFrom(buff);
	// string _str2(_m2.v.begin(),_m2.v.end());
	// cout<<_str2<<endl;
	// cout<<"Id:"<<_m2.head.id<<endl;

	cout<<"\nTesting CheckMessage"<<endl;
	CheckMessage m3,_m3;
	string str3 = "This is a check message";
	m3.init(str3,123);
	m3.loadTo(buff);
	_m3.loadFrom(buff);
	string _str3(_m3.check);
	cout<<_str3<<endl;
	cout<<"Id:"<<_m3.head.id<<endl;

	cout<<"\nTesting AddressMessage"<<endl;
	AddressMessage m4,_m4;
	vector<pair<uint32_t,uint16_t> > v4;
	v4.push_back(make_pair(1,1));
	v4.push_back(make_pair(2,3));
	m4.init(v4,123);
	m4.loadTo(buff);
	_m4.loadFrom(buff);
	for(int i=0;i<_m4.head.r1;i++){
		cout<<_m4.v[i].first<<" "<<_m4.v[i].second<<endl;
	}
	cout<<"Id:"<<_m4.head.id<<endl;
	cout<<"\nTesting CmdMessage\n";
	CmdMessage cm;
	vector<Command> cmd;
	Command c1,c2;
	c1.init(1,1);c2.init(2,0);
	cmd.push_back(c1);cmd.push_back(c2);
	cm.init(cmd,0);
	vector<char> buff1;
	cm.loadTo(buff1);
	cm.loadFrom(buff1);
	cout<<cm.commands[0].id+48<<" "<<cm.commands[1].id+48<<endl;
	

}
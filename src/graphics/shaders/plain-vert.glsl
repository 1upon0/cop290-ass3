#version 330

in vec3 Position;
in vec2 TexCoord;

out vec2 frag_TexCoord;

uniform mat4 gMVP = mat4(1.0);
void main()
{
    gl_Position = gMVP*vec4(Position,1.0);
    frag_TexCoord = TexCoord;
}
#include <stdio.h>
#include <string.h>
#include <enet/enet.h>
#include <iostream>
#define HOST "localhost"
#define PORT (7000)
#define BUFFERSIZE (1000)
using namespace std;

#include <unistd.h>

char  buffer[BUFFERSIZE];

ENetHost  *client;
ENetAddress  address;
ENetEvent  event;
ENetPeer  *peer;
ENetPacket  *packet;

int  main(int argc, char ** argv) {
	int connected=0;

	if (argc != 2) {
		printf("Usage: client username\n");
		exit(1);
	}

	if (enet_initialize() != 0) {
		printf("Could not initialize enet.\n");
		fflush(stdout);
		return 0;
	}

	client = enet_host_create(NULL, 1, 2, 5760/8, 1440/8);
	cout<<"Port:"<<client->address.port<<endl;
	cout<<"Host:"<<client->address.host<<endl;
	
	if (client == NULL) {
		cout<<"Could not create client"<<endl;
		return 0;
	}

	enet_address_set_host(&address, HOST);
	address.port = PORT;

	peer = enet_host_connect(client, &address, 2, 69);

	if (peer == NULL) {
		printf("Could not connect to server\n");
		return 0;
	}

	if (enet_host_service(client, &event, 500) > 0 &&
		event.type == ENET_EVENT_TYPE_CONNECT) {
		cout<<"data"<<event.data<<endl;
		printf("Connection to %s succeeded.\n", HOST);
		fflush(stdout);
		connected++;

		strncpy(buffer, argv[1], BUFFERSIZE);
		packet = enet_packet_create(buffer, strlen(buffer)+1,
			ENET_PACKET_FLAG_RELIABLE);
		enet_peer_send(peer, 0, packet);

	}
	else
	{
		enet_peer_reset(peer);
		printf("Could not connect to %s.\n", HOST);
		return 0;
	}

	while (1)
	{
		while (enet_host_service(client, &event, 1000) > 0)
		{
			switch (event.type) {
				case ENET_EVENT_TYPE_RECEIVE:
					cout<<(char*)event.packet->data<<endl;
					// puts( (char*) event.packet->data);
					// fflush(stdout);
					break;
				case ENET_EVENT_TYPE_DISCONNECT:
					connected=0;
					printf("You have been disconnected.\n");
					return 2;
			}
		}
		cout<<"Peer count:"<<client->peerCount<<endl;
          cout<<"Connected peers:"<<client->connectedPeers<<endl;

		// if (connected) {
			

		// 	if (strncmp("q", buffer, BUFFERSIZE) == 0) {
		// 		connected=0;
		// 		enet_peer_disconnect(peer, 0);
		// 		continue;
		// 	} 

		// 	packet = enet_packet_create(buffer, strlen(buffer)+1,
		// 		ENET_PACKET_FLAG_RELIABLE);
		// 	enet_peer_send(peer, 0, packet);
		// }
	}

	enet_deinitialize();
}
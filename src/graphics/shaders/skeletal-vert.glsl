#version 330 

in vec3 Position; 
in vec2 TexCoord; 
in vec3 Normal; 
in ivec4 BoneIDs;
in vec4 Weights;
in ivec4 BoneIDs2;
in vec4 Weights2;
in uint MeshID;           

out vec2 TexCoord0;
out vec3 Normal0; 
out vec3 WorldPos0; 
flat out uint frag_MeshID;

const int MAX_BONES = 100;

uniform mat4 gWVP=mat4(1.0);
uniform mat4 gWorld=mat4(1.0);
uniform mat4 gBones[MAX_BONES];

void main()
{ 
  float tot=0;
   for(int i=0;i<4;i++)tot+=Weights[i];
   for(int i=0;i<4;i++)tot+=Weights2[i];
    mat4 BoneTransform = gBones[BoneIDs[0]] * Weights[0];
    BoneTransform += gBones[BoneIDs[1]] * Weights[1];
    BoneTransform += gBones[BoneIDs[2]] * Weights[2];
    BoneTransform += gBones[BoneIDs[3]] * Weights[3];
    BoneTransform += gBones[BoneIDs2[0]] * Weights2[0];
    BoneTransform += gBones[BoneIDs2[1]] * Weights2[1];
    BoneTransform += gBones[BoneIDs2[2]] * Weights2[2];
    BoneTransform += gBones[BoneIDs2[3]] * Weights2[3];

    BoneTransform = BoneTransform/tot;

    vec4 PosL =  BoneTransform *vec4(Position,1.0);
    gl_Position = gWVP * PosL;
    TexCoord0 = TexCoord;
    vec4 NormalL =  BoneTransform *vec4(Normal, 1.0);
    Normal0 = (gWorld * NormalL).xyz;
    WorldPos0 = (gWorld * PosL).xyz; 
    frag_MeshID=MeshID;
}
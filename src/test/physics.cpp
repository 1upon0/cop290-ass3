#include <iostream>
#include "../physics.h"

using namespace std;

int main(int argc,char** argv)
{
	Circle c1;
	Circle c2;
	Circle c3;
	Circle c4;
	c1.init(vec3(0,0,0),2,vec3(1,1,0));
	c2.init(vec3(10,10,0),12,vec3(-1,-1,0));
	c3.init(vec3(5,0,0),3,vec3(0,sqrt(2),0));
	c4.init(vec3(-10,10,0),15,vec3(1,1,0));
	
	bool collided_c1_c2 = Physics::checkCollision(c1,c2);
	bool collided_c1_c3 = Physics::checkCollision(c1,c3);
	bool collided_c1_c4 = Physics::checkCollision(c1,c4);
	bool collided_c2_c3 = Physics::checkCollision(c2,c3);
	bool collided_c2_c4 = Physics::checkCollision(c2,c4);
	bool collided_c3_c4 = Physics::checkCollision(c3,c4);
	
	cout << "pass_test_c1_c2 : " << (collided_c1_c2==0) << endl;
	cout << "pass_test_c1_c3 : " << (collided_c1_c3==1) << endl;
	cout << "pass_test_c1_c4 : " << (collided_c1_c4==1) << endl;
	cout << "pass_test_c2_c3 : " << (collided_c2_c3==1) << endl;
	cout << "pass_test_c2_c4 : " << (collided_c2_c4==1) << endl;
	cout << "pass_test_c3_c4 : " << (collided_c3_c4==0) << endl;
	
}
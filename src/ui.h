#pragma once
#include "input.h"
#include "vector3.h"
using namespace std;

class UI : public Input{
	//Need to have an id - Inherited from input

	virtual Command getInput(){
		//Get the input and perform check
		switch(){
			case :{
				Command c;
				//get x,y
				c.init(id,MOVE,vector3(x,y,0));
				return c;
			}
			case :{
				Command c;
				//get x,y
				c.init(id,ROT);
				return c;
			}
			case :{
				Command c;
				//get x,y
				c.init(id,HOOK,vector3(x,y,0));
				return c;
			}
			default:
			break;

		}
	}
}
#version 330

in vec3 Position;
in vec2 TexCoord;
in vec3 Normal;

out vec2 frag_TexCoord;
out vec3 frag_Normal;
out vec3 frag_WorldPos;


uniform mat4 gMVP=mat4(1.0);
uniform mat4 gModel=mat4(1.0);

void main()
{
    vec4 PosL = vec4(Position,1.0);
    gl_Position = gMVP * PosL;
    frag_TexCoord = TexCoord;
    vec4 NormalL =  vec4(normalize(Normal), 0.0);
    frag_Normal = normalize((gModel * NormalL).xyz);
    frag_WorldPos = (gModel * PosL).xyz;
}
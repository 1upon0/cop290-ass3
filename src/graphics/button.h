#pragma once
// #include <GL/glut.h>
// #include <GL/freeglut.h>
#include <string>
#include <functional>
using namespace std;

class Button
{
public:
	//(x1,y1) - upper left
	//(x2,y2) - lower right
	int x1,y1,x2,y2;
	int state;
	enum {state_idle=0,state_hover,state_pressed};

	//Button name
	string name;
	bool highlight;
	std::function<void(void)> callback;
	Button(){
		highlight = 0;
	}
	void draw(){
		//TODO - Draw of the button
		/**
	 * On hovering the Button is highlighted.
	 */
	if (state>=state_hover || highlight)
		glColor3f(0.6f,0.6f,0.6f);
	else
		glColor3f(0.4f,0.4f,0.4f);
	glBegin(GL_QUADS);
		glVertex3f(x1,y1,0);
		glVertex3f(x2,y1,0);
		glVertex3f(x2,y2,0);
		glVertex3f(x1,y2,0);
	glEnd();

	/**
	 * Boundary appears when the Button is clicked.
	 */
	glLineWidth(3);
	if (state>=state_pressed)
		glColor3f(0.4f,0.4f,0.4f);
	else
		if(highlight)
			glColor3f(0.4,0.4,0.4);
		else
			glColor3f(0.8f,0.8f,0.8f);

	glBegin(GL_LINE_STRIP);
		glVertex3f(x1+1,y1+1,0);
		glVertex3f(x2-1,y1+1,0);
		glVertex3f(x2-1,y2-1,0);
		glVertex3f(x1+1,y2-1,0);
		glVertex3f(x1+1,y1+1,0);
	glEnd();
	glLineWidth(1);
	/**
	 * To draw the text on the Button.
	 */
	glColor3f(1.0f, 1.0f, 1.0f);
	// glRasterPos2i((x2+x1-glutBitmapLength(GLUT_BITMAP_TIMES_ROMAN_24,(const unsigned char*)(name.c_str())))/2.0,(y2+y1)/2.0);


	// glutBitmapString(GLUT_BITMAP_HELVETICA_12, (unsigned char*)(void*)(name.c_str()));

	}
	void init(string str,int a=0,int b=0,int c=0,int d=0){
		x1=a;y1=b;
		x2=c;y2=d;
		state=state_idle;
		name = str;
		highlight = 0;
	}
	void updateState(int x,int y,bool b0,bool b1){
		// cout<<"Update state"<<endl;
		int prev_state=state;
		if(!b0)
			state=state_idle;
		if(x>x1 && x<x2 && y>y1 && y<y2)
		{
			if(!b0)
				state=state_hover;
			if(b0 && prev_state != state_idle){
				state=state_pressed;
			}
		}else if(prev_state!=state_pressed)
			state=state_idle;
		if(state==state_pressed && prev_state!=state){
			printf("Button pressed : %s\n",name.c_str());
			// (*callBack)(NULL);
		}else if(state!=state_pressed && prev_state==state_pressed){
			if(name[0]=='p')
				highlight = !highlight;
		  printf("Button released : %s\n",name.c_str());
      callback();
		}
	}
};
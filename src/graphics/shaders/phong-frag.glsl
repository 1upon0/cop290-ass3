#version 330

const int MAX_LIGHTS = 5;
const int MAX_MAPS = 20;

in vec2 frag_TexCoord;
in vec3 frag_Normal;
in vec3 frag_WorldPos;

struct DirLight
{
    vec3 color;
    float ambient;
    float diffuse;
    vec3 dir;
};

struct PointLight
{
    vec3 color;
    float ambient;
    float diffuse;
    vec3 pos;
    float attn_const;
    float attn_linear;
    float attn_exp;
};


uniform int gNumPointLights=0;
uniform int gNumDirLights=0;
uniform DirLight gDirLights[MAX_LIGHTS];
uniform PointLight gPointLights[MAX_LIGHTS];
uniform sampler2D gColorMap;
uniform vec3 gEyeWorldPos;
uniform float gMatSpecIntensity;
uniform float gMatSpecPower;


vec4 CalcLight(vec3 color,float ambient,float diffuse,vec3 dir,vec3 normal)
{
    float DiffuseFactor = dot(normal, -dir);
    vec3 final = color*ambient;
    if (DiffuseFactor > 0.0) {
        final += color * DiffuseFactor * diffuse;

        vec3 VertexToEye = normalize(gEyeWorldPos - frag_WorldPos);
        vec3 LightReflect = reflect(dir, normal);
        float SpecularFactor = dot(VertexToEye, LightReflect);
        SpecularFactor = pow(SpecularFactor, gMatSpecPower);
        if (SpecularFactor > 0.0) {
            final += color * gMatSpecIntensity * SpecularFactor;
        }
    }
    return vec4(final,1.0);
}

vec4 CalcDirLight(DirLight l,vec3 normal)
{
    return CalcLight(l.color,l.ambient,l.diffuse,normalize(l.dir),normal);
}

vec4 CalcPointLight(PointLight l, vec3 normal)
{
    vec3 dir = frag_WorldPos - l.pos;
    float r = length(dir);
    dir = dir/r;

    vec4 color = CalcLight(l.color,l.ambient,l.diffuse, dir, normal);
    float attn =  l.attn_const +
                         l.attn_linear * r +
                         l.attn_exp * r * r;

    return color/attn;
}


out vec4 FragColor;

void main()
{
    vec3 norm_normal = normalize(frag_Normal);
    vec4 tot_light = vec4(0);
    for (int i = 0 ; i < gNumDirLights ; i++)
      tot_light += CalcDirLight(gDirLights[i],norm_normal);
    for (int i = 0 ; i < gNumPointLights ; i++)
      tot_light += CalcPointLight(gPointLights[i],norm_normal);
    tot_light.w=1.0;
    FragColor = texture(gColorMap, frag_TexCoord.xy)*tot_light;//vec4((norm_normal+vec3(1,1,1))/2.0,1);//
}

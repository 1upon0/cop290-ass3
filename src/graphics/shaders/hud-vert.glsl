#version 330

in vec2 Position;
in vec2 TexCoord;

out vec2 frag_TexCoord;

uniform mat4 gScreen = mat4(1.0);
void main()
{
    gl_Position = gScreen*vec4(Position,0,1.0);
    frag_TexCoord = TexCoord;
}
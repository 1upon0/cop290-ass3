#pragma once
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <log.h>
#include <map>
using namespace std;

#include <stdlib.h>
#include <string.h>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <assimp/scene.h>
#include <GL/glew.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb-image.h>

#define clamp(x,a,b) (((x)>(b))?(b):(((a)>(x))?(a):(x)))
class Renderer{
  static Renderer* singleton;
  Renderer(){
    assert(!singleton);
    gProgramID=0;
    singleton=this;
  }
public:
  GLuint gProgramID;
  static Renderer* _()
  {
    if(!singleton)
      new Renderer();
    return singleton;
  }
  GLuint loadTex(string file){
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    int w,h;
    logInfo("Loading %s",file.c_str());
    unsigned char *data = stbi_load(file.c_str(), &w, &h,0,4);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA,w,h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    // When MAGnifying the image (no bigger mipmap available), use LINEAR filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // When MINifying the image, use a LINEAR blend of two mipmaps, each filtered LINEARLY too
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      // Generate mipmaps, by the way.
    glGenerateMipmap(GL_TEXTURE_2D);

    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    return textureID;
  }
   GLuint loadTex(int w,int h,void *data){
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA,w,h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    // When MAGnifying the image (no bigger mipmap available), use LINEAR filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // When MINifying the image, use a LINEAR blend of two mipmaps, each filtered LINEARLY too
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      // Generate mipmaps, by the way.
    glGenerateMipmap(GL_TEXTURE_2D);

    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    return textureID;
  }
  GLuint loadShaders(const char * vertex_file_path,const char * fragment_file_path){

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
    if(VertexShaderStream.is_open()){
      std::string Line = "";
      while(getline(VertexShaderStream, Line))
        VertexShaderCode += "\n" + Line;
      VertexShaderStream.close();
    }else{
      logError("Error opening %s", vertex_file_path);
      return 0;
    }

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if(FragmentShaderStream.is_open()){
      std::string Line = "";
      while(getline(FragmentShaderStream, Line))
        FragmentShaderCode += "\n" + Line;
      FragmentShaderStream.close();
    }

    GLint Result = GL_FALSE;
    int InfoLogLength;

    // Compile Vertex Shader
    logDebug("Compiling shader : %s", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);

    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (Result == GL_FALSE){
      std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
      glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
      logWarn(&VertexShaderErrorMessage[0]);
    }



    // Compile Fragment Shader
    logDebug("Compiling shader : %s", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);

    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if(Result == GL_FALSE){
      std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
      glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
      logWarn(&FragmentShaderErrorMessage[0]);
    }



    // Link the program
    logDebug("Linking program");
    gProgramID = glCreateProgram();
    glAttachShader(gProgramID, VertexShaderID);
    glAttachShader(gProgramID, FragmentShaderID);
    glLinkProgram(gProgramID);

    // Check the program
    glGetProgramiv(gProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(gProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (Result == GL_FALSE && InfoLogLength > 0 ){
      std::vector<char> ProgramErrorMessage(InfoLogLength+1);
      glGetProgramInfoLog(gProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
      logWarn(&ProgramErrorMessage[0]);
    }

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    return gProgramID;
  }


};
Renderer* Renderer::singleton=NULL;
Renderer* GetRenderer(){
  return Renderer::_();
}

glm::mat4x4 convertToGLM(const aiMatrix4x4 &a){
  return glm::transpose(glm::make_mat4(reinterpret_cast<const float*>(&a)));
}
glm::vec3 convertToGLM(const aiVector3D &a){
  return glm::vec3(a.x,a.y,a.z);
}
using namespace glm;


class GLObject{
public:
  GLuint gVBO,gIBO,gProgramID;
  size_t n_verts,n_indices,vertex_size,index_size;
  struct Attribute{
    string name;
    GLint gIndex;
    size_t offset,n_components;
    GLenum type;
    GLboolean normalized;
    Attribute(){
      offset=0;gIndex=0;
      n_components=3;
      type=GL_FLOAT;
      normalized=GL_FALSE;
    }
  };
  vector<Attribute> attribs;
  void loadShaders(string vertex,string frag){
    gProgramID=GetRenderer()->loadShaders(vertex.c_str(),frag.c_str());
  }
  void addAttrib(string name,size_t offset,size_t n_components=4,GLenum type=GL_FLOAT,GLboolean normalized=false){
    Attribute tmp;
    tmp.gIndex = glGetAttribLocation(gProgramID,name.c_str());
    if(tmp.gIndex<0){
      logError("Vertex attribute '%s' not found in shader",name.c_str());
    }
    tmp.name = name;
    tmp.n_components = n_components;
    tmp.type=type;
    tmp.normalized=normalized;
    tmp.offset=offset;
    attribs.push_back(tmp);
  }


  void bindBuffers(void *vertex_data,size_t _vertex_size,size_t _n_verts,void *index_data,size_t _index_size,size_t _n_indices){
    n_verts=_n_verts;
    n_indices=_n_indices;
    vertex_size=_vertex_size;
    index_size=_index_size;

    glGenBuffers( 1, &gVBO );
    glBindBuffer( GL_ARRAY_BUFFER, gVBO );
    glBufferData( GL_ARRAY_BUFFER, n_verts*vertex_size, vertex_data, GL_STATIC_DRAW );

    //Create IBO
    glGenBuffers( 1, &gIBO );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, gIBO );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER,n_indices*index_size, index_data, GL_STATIC_DRAW );
  }
  void preRender(){
    glUseProgram(gProgramID);
  }
  void render(GLenum type=GL_TRIANGLES){
    //Enable vertex position
    glBindBuffer( GL_ARRAY_BUFFER, gVBO );
    for(auto &attrib:attribs){
      glEnableVertexAttribArray(attrib.gIndex);
      if(attrib.type==GL_UNSIGNED_INT||attrib.type==GL_INT)
        glVertexAttribIPointer(attrib.gIndex, attrib.n_components, attrib.type, vertex_size,(void*) attrib.offset );
      else
      glVertexAttribPointer(attrib.gIndex, attrib.n_components, attrib.type, attrib.normalized, vertex_size,(void*) attrib.offset );
    }
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, gIBO );
    glDrawElements( type, n_indices,index_size==sizeof(GLuint)?GL_UNSIGNED_INT:GL_UNSIGNED_SHORT, NULL );

    //Disable vertex position
    for(auto &attrib:attribs){
      glDisableVertexAttribArray(attrib.gIndex);
    }
    glUseProgram(0);
  }
};


class GLDirectObject{//Uses the vert buffer directly instead of an index buffer
public:
  GLuint gVBO,gProgramID;
  size_t n_verts,vertex_size;
  struct Attribute{
    string name;
    GLint gIndex;
    size_t offset,n_components;
    GLenum type;
    GLboolean normalized;
    Attribute(){
      offset=0;gIndex=0;
      n_components=3;
      type=GL_FLOAT;
      normalized=GL_FALSE;
    }
  };
  vector<Attribute> attribs;
  void loadShaders(string vertex,string frag){
    gProgramID=GetRenderer()->loadShaders(vertex.c_str(),frag.c_str());
  }
  void addAttrib(string name,size_t offset,size_t n_components=4,GLenum type=GL_FLOAT,GLboolean normalized=false){
    Attribute tmp;
    tmp.gIndex = glGetAttribLocation(gProgramID,name.c_str());
    if(tmp.gIndex<0){
      logError("Vertex attribute '%s' not found in shader",name.c_str());
    }
    tmp.name = name;
    tmp.n_components = n_components;
    tmp.type=type;
    tmp.normalized=normalized;
    tmp.offset=offset;
    attribs.push_back(tmp);
  }


  void bindBuffers(void *vertex_data,size_t _vertex_size,size_t _n_verts){
    n_verts=_n_verts;
    vertex_size=_vertex_size;

    glGenBuffers( 1, &gVBO );
    glBindBuffer( GL_ARRAY_BUFFER, gVBO );
    glBufferData( GL_ARRAY_BUFFER, n_verts*vertex_size, vertex_data, GL_STATIC_DRAW );
  }
  void preRender(){
    glUseProgram(gProgramID);
  }
  void render(GLenum primitive=GL_TRIANGLES){
    //Enable vertex position
    glBindBuffer( GL_ARRAY_BUFFER, gVBO );
    for(auto &attrib:attribs){
      glEnableVertexAttribArray(attrib.gIndex);
      if(attrib.type==GL_UNSIGNED_INT||attrib.type==GL_INT)
        glVertexAttribIPointer(attrib.gIndex, attrib.n_components, attrib.type, vertex_size,(void*) attrib.offset );
      else
      glVertexAttribPointer(attrib.gIndex, attrib.n_components, attrib.type, attrib.normalized, vertex_size,(void*) attrib.offset );
    }
    glDrawArrays(primitive,0,n_verts);

    //Disable vertex position
    for(auto &attrib:attribs){
      glDisableVertexAttribArray(attrib.gIndex);
    }
    glUseProgram(0);
  }
};

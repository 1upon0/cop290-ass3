#pragma once
#include <iostream>
#include <vector>
#include <cstring>
#include <cstdlib>
#include "vector3.h"
#include "physics.h"

class Map{
public:
	Line* boundary;
	Line* river;
	int32_t xmin,xmax;
	int32_t ymin,ymax;
	int32_t rmin,rmax;
	Map(){
		xmin=ymin=xmax=ymax=0;
		rmin=0;
		rmax=0;
	}

	/**
	 * @brief initialize the map with boundaries and a river in between
	 */
	void init(int32_t _xmin=0,int32_t _ymin=0,int32_t _xmax=400,int32_t _ymax=400,int32_t _rmin=-1,int32_t _rmax=-1)
	{
		xmin = _xmin;
		ymin = _ymin;
		xmax = _xmax;
		ymax = _ymax;
		if(_rmin==-1) rmin = (xmax+xmin)/2 - 180;
		else rmin = _rmin;

		if(_rmax==-1) rmax = ((xmax+xmin)/2 + 180);
		else rmax = _rmax;

		boundary = new Line[4];
		boundary[0].init(vector3(1,0,0),-xmin);
		boundary[1].init(vector3(0,-1,0),ymax);
		boundary[2].init(vector3(-1,0,0),xmax);
		boundary[3].init(vector3(0,1,0),-ymin);

		river =new Line[2];
		river[0].init(vector3(-1,0,0),rmin);
		river[1].init(vector3(1,0,0),-rmax);

		// cout<<xmin<<" "<<xmax<<endl;
		// cout<<ymin<<" "<<ymax<<endl;
		// cout<<rmin<<" "<<rmax<<endl;
	}

	/**
	 * @brief check whether a circle lies within the map boundaries and outside in the river
	 *
	 * @param c [description]
	 * @return true if circle location is valid else false
	 */
	bool isBound(Circle& c){
		for(int i=0;i<4;i++){
			// c.c.print();
			if(Physics::checkLineCollision(c,boundary[i])){
				// cout<<i<<endl;
				return false;
			}
		}
		if(c.c.x < (xmax+xmin)/2){	//Left arena
			if(Physics::checkLineCollision(c,river[0]))
			{return false;}
		}
		else{		//Right arena
			if(Physics::checkLineCollision(c,river[1]))
			{return false;}
		}
		return true;
	}
};

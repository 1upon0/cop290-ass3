#pragma once
#include <vector>
#include <cstdlib>
#include "network.h"
#include "command.h"
#include "gameEngine.h"
#include <SDL2/SDL.h>
#include <unistd.h>
#include <iomanip>
#include "timer.h"

#include "button.h"
#include "banner.h"
#include "bar.h"

#include "ai.h"
using namespace std;

#define INIT_SCREEN 1
#define START_SCREEN 2
#define GAME_SCREEN 3
#define PAUSE_SCREEN 4
#define DISCONNECTED_SCREEN 5
#define END_SCREEN 6

static int64_t initial_time = Timer::now();
const int SCREEN_WIDTH = 1366;//854;
const int SCREEN_HEIGHT = 768;//480;

class Window{
private:
	static Window* singleton;

	Window(){
		cout<<Timer::now()<<" = Constructor"<<endl;
		assert(!singleton && "GameEngine Singleton");
		singleton=this;

		screen_state=INIT_SCREEN;
		start=false;
		quit=false;
		GameEngine::_()->map.init(-1000,-1000,1000,1000);
		srand(0);
	}

public:
	bool quit;
	int cnt =0;
	NetworkPlayer np;
	uint32_t screen_state;
	Timer t;
	Timer recon_timer;
	vector<char> buff;
	uint32_t n_players;
	bool is_host;
	string host_ip;
	string host_port;
	bool start;
	struct Ui{
		//Buttons
		vector<Button> init_buttons;
    	vector<Button> start_buttons;
		vector<Button> game_buttons;
    	vector<Button> pause_buttons;
    	vector<Button> dc_buttons;
    	vector<Button> exit_buttons;
		//Banners
		vector<Banner> init_banner;
		vector<Banner> start_banner;
		vector<Banner> game_banner;
		vector<Banner> pause_banner;
		vector<Banner> dc_banner;
		vector<Banner> exit_banner;
	} ui;
	static Window* _(){
		if(!singleton)
			new Window;
		return singleton;
	}
	void createHuman(){
		is_ai=0;	//poor human :P
		//Transit to the START_SCREEN
		//Here we give host option to start and peer have to wait, not getting option to start
		screen_state= START_SCREEN;
	}

	/**
	 * @brief Start game in AI mode
	 */
	void createAI(){
		is_ai=1;	//Super ai :P
		//Transit to the START_SCREEN
		//Here we give host option to start and peer have to wait, not getting option to start
		screen_state=START_SCREEN;
	}


	void createGame(){
		//[Host] - on start button press
		//On start packet receive
		// screen_state=GAME_SCREEN;
		n_players = np.connectedPeers()+1;
		start=true;
	}

	/**
	 * @brief display pause screen
	 */
	void pauseGame(){
		if(np.received_pause){
			np.received_pause=false;
			screen_state=PAUSE_SCREEN;
			return;
		}
		cout<<"Hey there! Pause called"<<endl;
		GameEngine::_()->is_pause = !(GameEngine::_()->is_pause);	//Toggle
		np.sendPauseMessage();
		cout<<"PAUSE:"<<GameEngine::_()->is_pause<<endl;
		if(GameEngine::_()->is_pause)
			screen_state=PAUSE_SCREEN;
		else
			screen_state=GAME_SCREEN;
	}

	/**
	 * @brief call reconnect function
	 */
	void reconnectGame(){
		//Do the reconnect stuff
		logInfo("Trying to reconnect\n");
		recon_timer.start(5000);
		np.isRC = true;
		np.reconnect();
		ui.dc_banner[0].name = "Trying to reconnect!!";
	}

	/**
	 * @brief display exit screen
	 */
	void exitGame(){
		quit=true;
	}
	/**
	 * @brief Disconnected_Screen
	 */
	void disconnectedGame(){
		//Do the disconnect stuff
		screen_state=DISCONNECTED_SCREEN;
	}

	void initUi(){
		//All the buttons stuff
		Button tmp;
		tmp.init("AI MODE",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.3,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.375);
		tmp.callback =  std::bind(&Window::createAI,this);
		ui.init_buttons.push_back(tmp);
		tmp.init("HUMAN MODE",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.4,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.475);
		tmp.callback =  std::bind(&Window::createHuman,this);
		ui.init_buttons.push_back(tmp);
		if(is_host){
			tmp.init("START",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.5,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.575);
			tmp.callback = std::bind(&Window::createGame,this);
			ui.start_buttons.push_back(tmp);
		}
		tmp.init("EXIT",5,5,5+SCREEN_WIDTH*0.2,5+SCREEN_HEIGHT*0.050);
		tmp.callback = std::bind(&Window::exitGame,this);
		ui.game_buttons.push_back(tmp);
		tmp.init("PAUSE",5,5+SCREEN_HEIGHT*0.075,5+SCREEN_WIDTH*0.2,5+SCREEN_HEIGHT*0.125);
		tmp.callback = std::bind(&Window::pauseGame,this);
		ui.game_buttons.push_back(tmp);

		tmp.init("EXIT",5,5,5+SCREEN_WIDTH*0.2,5+SCREEN_HEIGHT*0.050);
		tmp.callback = std::bind(&Window::exitGame,this);
		ui.pause_buttons.push_back(tmp);
		tmp.init("UNPAUSE",5,5+SCREEN_HEIGHT*0.075,5+SCREEN_WIDTH*0.2,5+SCREEN_HEIGHT*0.125);
		tmp.callback = std::bind(&Window::pauseGame,this);
		ui.pause_buttons.push_back(tmp);

		tmp.init("EXIT",5,5,5+SCREEN_WIDTH*0.2,5+SCREEN_HEIGHT*0.050);
		tmp.callback = std::bind(&Window::exitGame,this);
		ui.dc_buttons.push_back(tmp);
		tmp.init("RECONNECT",5,5+SCREEN_HEIGHT*0.075,5+SCREEN_WIDTH*0.2,5+SCREEN_HEIGHT*0.125);
		tmp.callback = std::bind(&Window::reconnectGame,this);
		ui.dc_buttons.push_back(tmp);

		tmp.init("EXIT",5,5,5+SCREEN_WIDTH*0.2,5+SCREEN_HEIGHT*0.050);
		tmp.callback = std::bind(&Window::exitGame,this);
		ui.exit_buttons.push_back(tmp);
		//All the banners stuff
		Banner b;
		b.init("Countdown :",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.3,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.375);
		ui.start_banner.push_back(b);
		b.init("----",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.4,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.475);
		ui.start_banner.push_back(b);

		if(!is_host){
			b.init("Waiting for host to start",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.5,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.575);
			ui.start_banner.push_back(b);
		}
		//Assuming that order of buttons is maintained
		b.init("HEALTH : 100",5,5+SCREEN_HEIGHT*0.150,5+SCREEN_WIDTH*0.2,5+SCREEN_HEIGHT*0.200);
		ui.game_banner.push_back(b);
		b.init("COOL DOWN : 0",5,5+SCREEN_HEIGHT*0.225,5+SCREEN_WIDTH*0.2,5+SCREEN_HEIGHT*0.275);
		ui.game_banner.push_back(b);
		b.init("SCORE : 0 0",5,5+SCREEN_HEIGHT*0.300,5+SCREEN_WIDTH*0.2,5+SCREEN_HEIGHT*0.350);
		ui.game_banner.push_back(b);

		b.init("Press RECONNECT",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.3,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.375);
		ui.dc_banner.push_back(b);
	}

	void createFinalScreen(){
		Banner b;
		pair<int,int> p = GameEngine::_()->getScore();
		if(p.first > p.second){
			b.init("TEAM 1 WON!!",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.3,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.350);
			ui.exit_banner.push_back(b);
		}
		else if(p.first < p.second){
			b.init("TEAM 2 WON!!",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.3,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.350);
			ui.exit_banner.push_back(b);
		}
		else{
			b.init("TIE!!",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.3,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.350);
			ui.exit_banner.push_back(b);
		}
		b.init("Team 1",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*0.360,SCREEN_WIDTH*0.4,SCREEN_HEIGHT*0.410);
		ui.exit_banner.push_back(b);
		int count=1;
		for(int i=0;i<GameEngine::_()->original.v.size();i++){
			if(i%2)continue;
			Pudge *p = (Pudge*)GameEngine::_()->original.v[i];
			if(p->head.team == 0){
				count++;
				string str = np.getIP(p->head.id)+": "+ to_string(p->head.kills) + "/" + to_string(p->head.deaths);
				b.init(str,SCREEN_WIDTH*0.22,SCREEN_HEIGHT*(0.3 + 0.060*count),SCREEN_WIDTH*0.4,SCREEN_HEIGHT*(0.350 +0.060*count));
				ui.exit_banner.push_back(b);
			}
		}
		count++;
		b.init("Team 2",SCREEN_WIDTH*0.22,SCREEN_HEIGHT*(0.3 + 0.060*count),SCREEN_WIDTH*0.4,SCREEN_HEIGHT*(0.350 +0.060*count));
		ui.exit_banner.push_back(b);
		for(int i=0;i<GameEngine::_()->original.v.size();i++){
			if(i%2)continue;
			Pudge *p = (Pudge*)GameEngine::_()->original.v[i];
			if(p->head.team == 1){
				count++;
				string str = np.getIP(p->head.id)+": "+ to_string(p->head.kills) + "/" + to_string(p->head.deaths);
				b.init(str,SCREEN_WIDTH*0.22,SCREEN_HEIGHT*(0.3 + 0.060*count),SCREEN_WIDTH*0.4,SCREEN_HEIGHT*(0.350 +0.060*count));
				ui.exit_banner.push_back(b);
			}
		}
		screen_state=END_SCREEN;
	}
	void updateUi(int left,int right,int x,int y){
	    switch(screen_state)
	    {
			case INIT_SCREEN:
				for(auto &but:ui.init_buttons)
				but.updateState(x,y,left!=0,right!=0);
				break;
			case START_SCREEN:
				for(auto &but:ui.start_buttons)
				but.updateState(x,y,left!=0,right!=0);
				break;
			case GAME_SCREEN:
				for(auto &but:ui.game_buttons)
				but.updateState(x,y,left!=0,right!=0);
				break;
			case PAUSE_SCREEN:
				for(auto &but:ui.pause_buttons)
				but.updateState(x,y,left!=0,right!=0);
				break;
			case DISCONNECTED_SCREEN:
				for(auto &but:ui.dc_buttons)
				but.updateState(x,y,left!=0,right!=0);
				break;
			case END_SCREEN:
				for(auto &but:ui.exit_buttons)
				but.updateState(x,y,left!=0,right!=0);
				break;
			default:
				assert(false && "Invalid screen!");
	    }
	}
	void init(int& argc,char** argv){
		assert(argc==5 && "Invalid make command");

		is_host = atoi(argv[1]);
		n_players = atoi(argv[2]);
		host_ip = string(argv[3]);
		host_port = string(argv[4]);

		if(is_host){
			ip=host_ip;
			np.init(true,"",0);
		}
		else{
			np.init(false,host_ip, atoi(host_port.c_str()));
		}
		t.start(200);
		initUi();
	}
	void processNetwork(){
		np.processEvent();
		if(np.connectedPeers() == (n_players-1) && !np.ingame && np.isHost && !np.countdown && start){
			np.start(Timer::now() + 200);
			t.start(1000);
			cout<<"Started"<<endl;
		}
		if(np.ingame){
			np.sync();
		}
		if(np.countdown){
			if(Timer::now() > np.start_time){
				cout<<"Start now"<<endl;
				np.countdown = false;
				np.ingame = true;
				screen_state = GAME_SCREEN;
			}
		}
		if(np.isDC && !np.isRC){
			if(screen_state!=DISCONNECTED_SCREEN)
				disconnectedGame();
		}
		if(np.isRC){
			if(recon_timer.isTimeout()){
				logInfo("failed to connect to the tried peer... trying the next peer");
				np.peer_to_try++;
				np.reconnect();
				recon_timer.start(5000);
			}
		}
	}
	void updateStartScreen(){
		if(np.countdown){
			// cout<<(np.start_time-Timer::now())/1000<<endl;
			ui.start_banner[0].name = string("Countdown:");
			ui.start_banner[1].name = to_string((np.start_time-Timer::now())/1000);
		}
		else{
			ui.start_banner[0].name = string("Connected peers:");
			ui.start_banner[1].name = to_string((np.connectedPeers()));
		}
	}
	void updateGameScreen(){
		if(np.received_pause==true){
			cout<<"Hey pause"<<endl;
			pauseGame();
		}
		if(someone_died){
			someone_died=false;
			//np.getState();
		}
		//TODO we have to move this to gui later if required
		if(t.isTimeout() && np.ingame && is_ai){
			Command c = ai.decideAction();
			GameEngine::_()->applyCommand(c);
			np.pushCommand(c);
			t.start(400);
		}
		int64_t final_time = Timer::now();
		uint32_t n = GameEngine::_()->numberOfSimulations(final_time-initial_time + GameEngine::_()->accumulated);
		GameEngine::_()->accumulated =(final_time+GameEngine::_()->accumulated-initial_time)-n*GameEngine::_()->tp;
		for(int i=0;i<n;i++){
			GameEngine::_()->simulate();
		}
		initial_time=final_time;
		GameEngine::_()->checkAllCollision();

		//We have to update the banner here
		Pudge *p = GameEngine::_()->getPudge(np.my_data.id,&(GameEngine::_()->original));
		pair<int,int> _score = GameEngine::_()->getScore();
		if(_score.first == KILLS || _score.second==KILLS){
			createFinalScreen();
		}
		ui.game_banner[0].name = string("HEALTH: ") + to_string(p->head.health);
		string cd = to_string(float(p->head.cool_down/1000.0));
		ui.game_banner[1].name = string("COOL DOWN: ") + string(cd.begin(),cd.begin()+3);
		ui.game_banner[2].name = string("SCORE: ") + to_string(_score.first) + "/" + to_string(_score.second);
	}
	void updateDisconnectedScreen(){
		if(np.ingame){
			screen_state = GAME_SCREEN;
		}
		if(np.failed_reconnection){
			np.failed_reconnection=false;
			ui.dc_banner[0].name = "Failed to reconenct! Try Again";
		}
	}
	void displayCallback();
	vector3 getMapCords(float x,float y);
	void mouseCallback(int left,int right,int x,int y){
		vector3 map_cords = getMapCords(x,y);
		map_cords.y = map_cords.z;
		map_cords.z = 0;
		// cout<<"MAP CORDS ";
		// map_cords.print();

		if(left){
  			if(np.ingame && !(is_ai) && !GameEngine::_()->is_pause){
	  			Command c;
	  			c.init(np.my_data.id,MOVE,map_cords);
	  			c.timestamp = Timer::now();
	  			GameEngine::_()->applyCommand(c);
	  			np.pushCommand(c);
  			}
  		}
		if(right){
  			if(np.ingame && !(is_ai) && !GameEngine::_()->is_pause){
	  			Command c;
		  		c.init(np.my_data.id,HOOK,map_cords);
		  		c.timestamp = Timer::now();
		  		GameEngine::_()->applyCommand(c);
		  		np.pushCommand(c);
  			}
		}
		updateUi(left,right,x,y);
	}
	void handleKeys(unsigned char key, int x, int y ){
		if( key == 'q' ){
			SDL_Quit();
		}
		if(key=='p')
			pauseGame();
		if(key=='r'){
			cout<<np.ingame<<" "<<is_ai<<" "<<GameEngine::_()->is_pause<<endl;
			if(np.ingame && !(is_ai) && !GameEngine::_()->is_pause){
	  			cout<<"ROT command"<<endl;
	  			Command c;
	  			c.init(np.my_data.id,ROT);
	  			c.timestamp = Timer::now();
	  			GameEngine::_()->applyCommand(c);
		  		np.pushCommand(c);
			}
		}
		if(key=='c')
			reconnectGame();
		if(key=='s')
			createGame();
		if(key=='a')
			createAI();
		if(key=='h')
			createHuman();
	}
};
#include "graphics/gui.h"

void Window::displayCallback(){
  if(screen_state!=INIT_SCREEN){
    processNetwork();
  }
// cout<<"Screen state:"<<int(screen_state)<<endl;
  auto gui=GUI::_();
  switch(screen_state){
    case INIT_SCREEN:
      gui->initScreen();
      break;
    case START_SCREEN:
      gui->startScreen();
      updateStartScreen();
      break;
    case GAME_SCREEN:
      gui->gameScreen();
      updateGameScreen();
      break;
    case PAUSE_SCREEN:
      gui->pauseScreen();
      break;
    case DISCONNECTED_SCREEN:
      gui->disconnectedScreen();
      updateDisconnectedScreen();
      break;
    case END_SCREEN:
      gui->endScreen();
      break;
    default:
      assert(false && "Invalid screen!");
  }
}

vector3 Window::getMapCords(float x,float y){
  auto gui=GUI::_();
  float clipnear = gui->proj_near,fovy = gui->proj_fov;
  //TODO get from gui
  vector3 eye(gui->eye_pos.x,gui->eye_pos.y,gui->eye_pos.z) ,
    lookat(gui->look_at.x,gui->look_at.y,gui->look_at.z);
  vector3 up(gui->up_dir.x,gui->up_dir.y,gui->up_dir.z),
  view = lookat-eye;
  view.normalize();
  //cout<<"V ";view.print();
  vector3 h = view^up,

  v = h^view;
  //cout<<"h vector";h.print();
  //cout<<"v vector";v.print();
  float wWidth = gui->width,wHeight = gui->height;
  h.normalize();
  v.normalize();
  //cout<<"fov"<<fovy<<endl;

  //TODO above calculated vector3s should be recalculated only when camera is moved
  float rad = fovy*M_PI/180.0,
  vlength = tan(rad)*clipnear*0.5,
  hlength = vlength * (wWidth /wHeight);
  //cout<<"x "<<x<<"y "<<y<<endl;
  v *= vlength;
  h *= hlength;
  //cout<<"h vector";h.print();
  //cout<<"v vector";v.print();
  x = 2*x/wWidth-1;
  y = 1-2*y/wHeight;
  //cout<<"??x "<<x<<"??y "<<y<<endl;
  // scale mouse coordinates so that half the view port width and height
  // becomes 1

  //cout<<"**x "<<x<<"**y "<<y<<endl;
  pair<vector3,vector3> res;
  res.first = eye + view*clipnear + h*x + v*y;
  res.second = res.first - eye;
  // res.first.print();
  // res.second.print();
  //now calculate the corresponding map coordinates
  if(fabs(res.second.y) < 1e-6){
    cout<<"Cannot get map coordinates line parallel to x-z plane"<<endl;
    exit(1);
  }
  float t = -res.first.y/res.second.y;
  vector3 p = res.first + t*res.second;
  // p=p*1.1;
  //cout<<"WTF"<<t<<endl;
  return p;
}
Window* Window::singleton=NULL;
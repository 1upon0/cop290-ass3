#include "ai.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main(int argc,char** argv)
{
	srand(time(NULL));
	Player player1(0);
	Player player2(1);
	player1.team=0;
	player2.team=1;
	vector<Player> players_vec;
	players_vec.push_back(player1);
	players_vec.push_back(player2);
	while(1)
	{
		for(int i=0;i<players_vec.size();i++)
		{
			players_vec.at(i).decideAction(players_vec.at(i),players_vec);
		}
	}
	return 0;
}
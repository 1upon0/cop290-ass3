#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <enet/enet.h>
#include <iostream>
#define HOST "localhost"
#define PORT (7000)
#define BUFFERSIZE (1000)
#define N_USERS (10)

ENetAddress  address;
ENetHost   *server;
ENetEvent  event;
ENetPacket      *packet;

char buffer[BUFFERSIZE];
using namespace std;
void disconnectPeer(ENetPeer *peer){
  ENetEvent dc_event;
   enet_peer_disconnect_now(event.peer,0);
   while(enet_host_service(server,&dc_event,2000)) {
      switch(dc_event.type){
        case ENET_EVENT_TYPE_RECEIVE:
          enet_packet_destroy(dc_event.packet);
          break;
        case ENET_EVENT_TYPE_DISCONNECT:
          cout<<"disconnected"<<endl;
          return;
        default:  cout<<"waiting..."<<endl;

      }
   }
   cout<<"Forcing disconnection"<<endl;
   enet_peer_reset(peer);
   cout<<"disconnected."<<endl;
}
int  main(int argc, char ** argv) {
int  i;

  if (enet_initialize() != 0) {
    printf("Could not initialize enet.");
    return 0;
  }

  address.host = ENET_HOST_ANY;
  address.port = PORT;

  server = enet_host_create(&address, 2, 2, 0, 0);

  if (server == NULL) {
    printf("Could not start server.\n");
    return 0;
  }
  int n_users = 0,cdown=30;
  bool cont = true;
  while (cont) {
    
    while (enet_host_service(server, &event, 1000) > 0 &&cont) {
      switch (event.type) {

        case ENET_EVENT_TYPE_CONNECT:
        if(n_users == N_USERS){
          cout<<"someone still trying to connect, requesting for disconnection..."<<endl;
            disconnectPeer(event.peer);
        }
        else{
          printf("Connected to %d",&event.peer->address.host);
          n_users++;
          if(n_users == N_USERS){
             sprintf(buffer, "All users have connected.I am beginning a count down..\n");
             packet = enet_packet_create(buffer, strlen(buffer)+1, 0);
              enet_host_broadcast(server, 1, packet);
              enet_host_flush(server);
          }
          cout<<"data"<<event.data<<endl;
        }
          break;

        case ENET_EVENT_TYPE_RECEIVE:
          if (event.peer->data == NULL) {
            cout<<"received"<<endl;
            event.peer->data = malloc(strlen((char*) event.packet->data)+1);
            strcpy((char*) event.peer->data, (char*) event.packet->data);
              sprintf(buffer, "%s has connected\n", (char*) event.packet->data);
              fflush(stdout);
            packet = enet_packet_create(buffer, strlen(buffer)+1, 0);
            enet_host_broadcast(server, 1, packet);
       // cout<<"received a dummy packet, everything is good with "<<event.peer->address.host<<endl;
            enet_host_flush(server);
          } 
          break;

        case ENET_EVENT_TYPE_DISCONNECT:
        
          sprintf(buffer, "%s has disconnected.", (char*)
              event.peer->data);
          packet = enet_packet_create(buffer, strlen(buffer)+1, 0);
          enet_host_broadcast(server, 1, packet);
           enet_host_flush(server);
          free(event.peer->data);
          event.peer->data = NULL;
          n_users--;
          break;

        }
       
      }
      if(n_users == N_USERS){

       
         if(cdown ==0){
          sprintf(buffer, "Bye Bye\n");
          packet = enet_packet_create(buffer, strlen(buffer)+1, ENET_PACKET_FLAG_RELIABLE);
          enet_host_broadcast(server, 1, packet);
           enet_host_flush(server);
          cont = false;
         }else{
         sprintf(buffer, "%d seconds left.\n", 
              cdown);
          packet = enet_packet_create(buffer, strlen(buffer)+1, ENET_PACKET_FLAG_RELIABLE);
          enet_host_broadcast(server, 1, packet);
           enet_host_flush(server);
          cdown--;
        }
        }
        else{
          cout<<"Peer count:"<<server->peerCount<<endl;
          cout<<"Connected peers:"<<server->connectedPeers<<endl;
        }
        // else{cout<<"imma waiting"<<endl;}
    }
  
  
  enet_host_destroy(server);
  enet_deinitialize();
}

#version 330

in vec3 Position;
in vec2 TexCoord;
in vec3 Normal;

out vec2 frag_TexCoord;
out vec3 frag_Normal;
out vec3 frag_WorldPos;

uniform vec4 gAxis=vec4(0,1,0);
uniform mat4 gWVP=mat4(1.0);
uniform mat4 gWorld=mat4(1.0);

void main()
{
    vec4 PosL = vec4(Position,1.f);
    gl_Position = mat4(vec4(1.f,gWVP[0].yz*gAxis.x,gWVP[0].w),vec4(gWVP[1].x*gAxis.y,1.f,gWVP[1].z*gAxis.y,gWVP[1].w),vec4(gWVP[2].xy*gAxis.z,1.f,gWVP[2].w),gWVP[3]) * PosL;
    frag_TexCoord = TexCoord;
    vec4 NormalL =  vec4(Normal, 1.f);
    frag_Normal = (gWorld * NormalL).xyz;
    frag_WorldPos = (gWorld * PosL).xyz;
}
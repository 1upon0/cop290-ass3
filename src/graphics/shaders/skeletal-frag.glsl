#version 330

const int MAX_POINT_LIGHTS = 2;
const int MAX_SPOT_LIGHTS = 2;
const int MAX_MAPS = 20;

in vec2 TexCoord0;
in vec3 Normal0;                                            
in vec3 WorldPos0;            
flat in uint frag_MeshID;

struct VSOutput
{
    vec2 TexCoord;
    vec3 Normal;                                                                   
    vec3 WorldPos;                                                                 
};



// struct BaseLight
// {
//     vec3 Color;
//     float AmbientIntensity;
//     float DiffuseIntensity;
// };

// struct DirectionalLight
// {
//     BaseLight Base;
//     vec3 Direction;
// };
                                                                                    
// struct Attenuation                                                                  
// {                                                                                   
//     float Constant;                                                                 
//     float Linear;                                                                   
//     float Exp;                                                                      
// };                                                                                  
                                                                                    
// struct PointLight                                                                           
// {                                                                                           
//     BaseLight Base;                                                                  
//     vec3 Position;                                                                          
//     Attenuation Atten;                                                                      
// };                                                                                          
                                                                                            
// struct SpotLight                                                                            
// {                                                                                           
//     PointLight Base;                                                                 
//     vec3 Direction;                                                                         
//     float Cutoff;                                                                           
// };                                                                                          
                                                                                            
// uniform int gNumPointLights=0;                                                                
// uniform int gNumSpotLights=0;                                                                
// uniform DirectionalLight gDirectionalLight;                                                 
// uniform PointLight gPointLights[MAX_POINT_LIGHTS];                                          
// uniform SpotLight gSpotLights[MAX_SPOT_LIGHTS];                                             
uniform sampler2D gColorMaps[MAX_MAPS];                                                                
uniform vec3 gEyeWorldPos;                                                                  
uniform float gMatSpecularIntensity;                                                        
uniform float gSpecularPower; 


// vec4 CalcLightInternal(BaseLight Light, vec3 LightDirection, VSOutput In)            
// {                                                                                           
//     vec4 AmbientColor = vec4(Light.Color, 1.0) * Light.AmbientIntensity;                   
//     float DiffuseFactor = dot(In.Normal, -LightDirection);                                     
                                                                                            
//     vec4 DiffuseColor  = vec4(0, 0, 0, 0);                                                  
//     vec4 SpecularColor = vec4(0, 0, 0, 0);                                                  
                                                                                            
//     if (DiffuseFactor > 0.0) {                                                                
//         DiffuseColor = vec4(Light.Color, 1.0) * Light.DiffuseIntensity * DiffuseFactor;    
                                                                                            
//         vec3 VertexToEye = normalize(gEyeWorldPos - In.WorldPos);                             
//         vec3 LightReflect = normalize(reflect(LightDirection, In.Normal));                     
//         float SpecularFactor = dot(VertexToEye, LightReflect);                              
//         SpecularFactor = pow(SpecularFactor, gSpecularPower);                               
//         if (SpecularFactor > 0.0) {                                                           
//             SpecularColor = vec4(Light.Color, 1.0) *                                       
//                             gMatSpecularIntensity * SpecularFactor;                         
//         }                                                                                   
//     }                                                                                       
                                                                                            
//     return (AmbientColor + DiffuseColor + SpecularColor);                                   
// }                                                                                           
                                                                                            
// vec4 CalcDirectionalLight(VSOutput In)                                                      
// {                                                                                           
//     return CalcLightInternal(gDirectionalLight.Base, gDirectionalLight.Direction, In);  
// }                                                                                           
                                                                                            
// vec4 CalcPointLight(PointLight l, VSOutput In)                                       
// {                                                                                           
//     vec3 LightDirection = In.WorldPos - l.Position;                                           
//     float Distance = length(LightDirection);                                                
//     LightDirection = normalize(LightDirection);                                             
                                                                                            
//     vec4 Color = CalcLightInternal(l.Base, LightDirection, In);                         
//     float Attenuation =  l.Atten.Constant +                                                 
//                          l.Atten.Linear * Distance +                                        
//                          l.Atten.Exp * Distance * Distance;                                 
                                                                                            
//     return Color / Attenuation;                                                             
// }                                                                                           
                                                                                            
// vec4 CalcSpotLight(SpotLight l, VSOutput In)                                         
// {                                                                                           
//     vec3 LightToPixel = normalize(In.WorldPos - l.Base.Position);                             
//     float SpotFactor = dot(LightToPixel, l.Direction);                                      
                                                                                            
//     if (SpotFactor > l.Cutoff) {                                                            
//         vec4 Color = CalcPointLight(l.Base, In);                                        
//         return Color * (1.0 - (1.0 - SpotFactor) * 1.0/(1.0 - l.Cutoff));                   
//     }                                                                                       
//     else {                                                                                  
//         return vec4(0,0,0,0);                                                               
//     }                                                                                       
// }                                                                                           
                            

out vec4 FragColor;
                                                                
void main()
{                                    
    VSOutput In;
    In.TexCoord = TexCoord0;
    In.Normal   = normalize(Normal0);
    In.WorldPos = WorldPos0;

  
    // vec4 TotalLight = CalcDirectionalLight(In);                                         
                                                                                            
    // for (int i = 0 ; i < gNumPointLights ; i++) {                                           
    //     TotalLight += CalcPointLight(gPointLights[i], In);                              
    // }                                                                                       
    switch(frag_MeshID){
      case 1u:FragColor = texture(gColorMaps[1], In.TexCoord.xy);break;
      case 2u:FragColor = texture(gColorMaps[2], In.TexCoord.xy);break;
      case 3u:FragColor = texture(gColorMaps[3], In.TexCoord.xy);break;
      case 4u:FragColor = texture(gColorMaps[4], In.TexCoord.xy);break;
      case 5u:FragColor = texture(gColorMaps[5], In.TexCoord.xy);break;
      case 6u:FragColor = texture(gColorMaps[6], In.TexCoord.xy);break;
      case 7u:FragColor = texture(gColorMaps[7], In.TexCoord.xy);break;
      case 8u:FragColor = texture(gColorMaps[8], In.TexCoord.xy);break;
      case 9u:FragColor = texture(gColorMaps[9], In.TexCoord.xy);break;
      case 10u:FragColor = texture(gColorMaps[10], In.TexCoord.xy);break;
      case 11u:FragColor = texture(gColorMaps[11], In.TexCoord.xy);break;
      case 12u:FragColor = texture(gColorMaps[12], In.TexCoord.xy);break;
      default:
        FragColor = texture(gColorMaps[0], In.TexCoord.xy);
    }
}

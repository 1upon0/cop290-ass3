#pragma once
#include "boilerplate.h"
#include <fstream>
#include <sstream>


class RigidObj{
  public:
  struct Vert{
    vec3 pos;
    vec2 tex;
    vec3 norm;
    GLuint mesh_id;
    Vert(){
      mesh_id=0;
    }
  };
  struct Face{
    GLuint indexes[3];
  };

  vector<Vert> vert_buff;
  vector<Face> face_buff;
  vector<GLuint> textures;
  map<uint,mat4x4> mesh_transform;
  GLObject obj;
  Assimp::Importer aiImporter;

    vector<string> mesh_names;
  RigidObj(){
  }
  void traverseNodes(aiNode *n,int depth=0,mat4x4 base=glm::mat4(1.f)){
    // if(n->mNumMeshes==0)
    //   logInfo("Found meshless node %s",n->mName.C_Str());
    // else
    //   logInfo("Found node %s with %d meshes",n->mName.C_Str(),n->mNumMeshes);
    mat4x4 new_base=base*convertToGLM(n->mTransformation);
    for(size_t i=0;i<n->mNumMeshes;i++){
      if(mesh_names.size()<=n->mMeshes[i])
        mesh_names.resize(n->mMeshes[i]+1);
      mesh_names[n->mMeshes[i]]=n->mName.C_Str();
      if(mesh_transform.find(n->mMeshes[i]) == mesh_transform.end())
        mesh_transform.insert(make_pair(n->mMeshes[i],new_base));
    }
    for(size_t i=0;i<n->mNumChildren;i++){
      traverseNodes(n->mChildren[i],depth+1,new_base);
    }
  }
  void load(const char* file,const char *vertshader="shaders/phong-vert.glsl",const char *fragshader="shaders/phong-frag.glsl")
  {
    auto scene = aiImporter.ReadFile(file,aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_FlipUVs  );
    assert(scene && "load mesh");
    traverseNodes(scene->mRootNode);
    vector<int> MeshTexSerials;
    for(size_t i=0;i<scene->mNumMeshes;i++){

      mat4x4 base=glm::mat4(1.f);
      auto id_transform=mesh_transform.find(i);
      if(id_transform!=mesh_transform.end()){
        base=id_transform->second;
      }
      auto &mesh=scene->mMeshes[i];
      assert(mesh);

      MeshTexSerials.push_back(i);

      GLuint starting_id=vert_buff.size();
      for(size_t j=0;j<mesh->mNumVertices;j++){
        Vert vert;
        vert.mesh_id=i;
        vert.pos=convertToGLM(mesh->mVertices[j]);
        vert.pos=glm::vec3(base*glm::vec4(vert.pos,1));
        // logInfo("%f %f %f\n",vert.pos.x,vert.pos.y,vert.pos.z);
        if(mesh->HasNormals())
          vert.norm=convertToGLM(mesh->mNormals[j]);
        if(mesh->mTextureCoords[0]){
          vert.tex=vec2(convertToGLM(mesh->mTextureCoords[0][j]));
          // vert.tex.x=vert.tex.x;
          // vert.tex.y=1-vert.tex.y;
        }
        vert_buff.push_back(vert);
      }
      for(size_t j=0;j<mesh->mNumFaces;j++){
        auto &face=mesh->mFaces[j];
        if(face.mNumIndices!=3)
          continue;
          // logError("All faces should be triangles!");
        face_buff.push_back({{face.mIndices[0]+starting_id,
          face.mIndices[1]+starting_id,
          face.mIndices[2]+starting_id}});
      }
    }
    obj.loadShaders(vertshader,fragshader);
    obj.addAttrib("Position",offsetof(Vert,pos),3,GL_FLOAT);
    obj.addAttrib("TexCoord",offsetof(Vert,tex),2,GL_FLOAT);
    obj.addAttrib("Normal",offsetof(Vert,norm),3,GL_FLOAT);
    obj.addAttrib("MeshID",offsetof(Vert,mesh_id),1,GL_UNSIGNED_INT);
    obj.bindBuffers(vert_buff.data(),sizeof(Vert),vert_buff.size(),face_buff.data(),sizeof(GLuint),face_buff.size()*3);

    assert(GL_MAX_TEXTURE_UNITS>=scene->mNumMeshes);

    glUseProgram(obj.gProgramID);
    glUniform1iv(glGetUniformLocation(obj.gProgramID, "gColorMaps"),scene->mNumMeshes,MeshTexSerials.data());

    // for(size_t i=0;i<MeshTexSerials.size();i++){
    //   textures.push_back(GetRenderer()->loadTex(string("res/material/")+mesh_names[i]+".tga"));
    // }
    glUseProgram(0);
  }
  void preRender(){
    obj.preRender();
    for(size_t i=0;i<textures.size();i++){
      glActiveTexture(GL_TEXTURE0 + i); // Texture unit 0
      glBindTexture(GL_TEXTURE_2D, textures[i]);
    }
  }
  void render(){
    obj.render();
    for(size_t i=0;i<textures.size();i++){
      glActiveTexture(GL_TEXTURE0 + i); // Texture unit 0
      glBindTexture(GL_TEXTURE_2D,0);
    }
  }
};




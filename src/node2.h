#pragma once
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <vector>
#include <enet/enet.h>
// #include <sys/socket.h>
// #include <netinet/in.h>
// #include <arpa/inet.h>
#include <algorithm>
// #include "timer.h"

#include <cstring>
#include "message.h"
using namespace std;
struct Node{
	ENetHost *self;
	uint16_t port;
};
struct PeerData{
	uint32_t id;
	int state;
};
class Network{
public:
	ENetPeer *host;
	Node *node;
	string host_ip;
	uint16_t host_port;
	int bandwidth;
	vector<pair<ENetAddress,PeerData> > peers;

	Network();
	void initENet();
	void init();
	void createNode(int n_connections=32);
	void connectToHost(string host_ip,uint16_t host_port);
	void connectToHost(ENetAddress);

	void connectToPeer(ENetAddress,uint32_t id,uint32_t);
	int process();
	int same_addr(ENetAddress &a,ENetAddress &b);
	int find(ENetAddress &ddr);
	int eventConnect(ENetEvent* event);
	int eventReceive(ENetEvent* event);
	int eventDisconnected(ENetEvent *event);
	void sendToAll(const vector<char>& buff);
	void sendToPeer(const vector<char>& buff,ENetPeer *peer);

};
Network::Network(){
	node=NULL;
	host=NULL;
	host_ip = "";
	host_port = 0;
	bandwidth = 0;
	peers.resize(0);
}


void Network::init(){
	assert(enet_initialize()==0 && "Cannot initialize the ENet library");
    atexit(enet_deinitialize);
}

/**
 * @brief create host
 * 
 * @param n_connections maximum allowable players
 */
void Network::createNode(int n_connections){
	node = new Node;
	//Initializing the node
	bandwidth = 2*n_connections;		//because each peer requires two connections
	node->self=NULL;

	//Initializing address
	ENetAddress address;
	address.host = ENET_HOST_ANY;
	address.port = 1110;
	while(node->self == NULL){
		address.port++;
		cout<<"Trying port: "<<address.port<<endl;
		node->self = enet_host_create(&address,bandwidth,2,0,0);
	}
	node->port = address.port;
	cout<<"Port assigned: "<<node->port<<endl;
}


/**
 * @brief connects to the specified host before starting the processing of events
 * 
 * @param _host_ip [description]
 * @param _host_port [description]
 */
void Network::connectToHost(string _host_ip,uint16_t _host_port){

	assert(_host_ip !="" && _host_port >0 && "invalid ip or port for host");
	host_ip = _host_ip;
	host_port = _host_port;

	ENetAddress address;
	enet_address_set_host(&address,host_ip.c_str());
	address.port = host_port;
	connectToHost(address);
}

/**
 * @brief sends connection request to server in a non blocking manner
 * 
 * @param addr server address
 */
void Network::connectToHost(ENetAddress addr){
	cout<<"[Peer]Sending connection request to host:";
	host = enet_host_connect(node->self,&addr,2,1000);
	assert(host!=NULL && "Could not connect to host!");
	cout<<host->address.port<<endl;
	int idx = find(addr);
		if(idx == -1){
		PeerData pdata;
		pdata.state = 0,pdata.id = 1;
		peers.push_back(make_pair(addr,pdata));
	}
	else{
		assert(peers[idx].second.state ==0 && "trying to connect to an active host");

	}
}

/**
 * @brief connect to another peer
 * 
 * @param addr [description]
 * @param my_id [description]
 * @param p_id [description]
 */
void Network::connectToPeer(ENetAddress addr,uint32_t my_id,uint32_t p_id){
	ENetPeer *peer;
	cout<<"[Peer]Sending connection request to peer:";
	peer = enet_host_connect(node->self,&addr,2,uint8_t(my_id));
	assert(peer!=NULL && "Could not connect to peer!");
	cout<<peer->address.host<<endl;
	int idx = find(addr);
	if(idx == -1){
		PeerData pdata;
		pdata.state = 0,pdata.id = p_id;
		peers.push_back(make_pair(addr,pdata));
	}
	else{
		assert(peers[idx].second.state ==0 && "trying to connect to an active peer");

	}
}

/**
 * @brief compare two addresses
 * 
 * @param a [description]
 * @param b [description]
 * 
 * @return 1 if same else 0
 */
int Network::same_addr(ENetAddress &a, ENetAddress &b)
{
	int same_host = a.host == b.host;
	int same_port = a.port == b.port;
	return same_host && same_port;
}

/**
 * @brief find peer in connected peer list
 * 
 * @param addr [description]
 * @return [description]
 */
int Network::find(ENetAddress &addr){
	//cout<<"find in "<<node->peers.size()<<endl;
	for(int i=0;i<peers.size();i++){
		//cout<<node->peers[i].first->address.port<<" "<<peer->address.port<<endl;
		if(same_addr(peers[i].first,addr)){
			return i;
		}
	}
	return -1;
}

/**
 * @brief send buffer to all the connected peers
 * 
 * @param buff [description]
 */
void Network::sendToAll(const vector<char>& buff){
	ENetPacket *packet;
	packet = enet_packet_create(buff.data(),buff.size(),ENET_PACKET_FLAG_RELIABLE);
	enet_host_broadcast(node->self,0,packet);
	enet_host_flush(node->self);
}

/**
 * @brief send buffer to a peer
 * 
 * @param buff [description]
 * @param peer [description]
 */
void Network::sendToPeer(const vector<char>& buff,ENetPeer *peer){
	ENetPacket *packet;
	packet = enet_packet_create(buff.data(),buff.size(),ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(peer,0,packet);
	enet_host_flush(node->self);
}

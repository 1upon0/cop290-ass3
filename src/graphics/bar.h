#pragma once
// #include <GL/glut.h>
// #include <GL/freeglut.h>
#include <string>
#include <functional>
using namespace std;

class Bar
{
public:
	int x1,y1,x2,y2;
	int x3;		//Shows the position	
	float min,max,val;
	bool opp_team;	//Need to decide the color on basis of team
	string name;	//Bar name - can be health, cool_down...
	
	Bar(bool _opp_team=false){
		opp_team=_opp_team;
	}
	void init(string _name,int _x1,int _y1,int _x2,int _y2,float _val=50,float _min=0,float _max=100){
		x1=_x1,y1=_y1,x2=_x2,y2=_y2;
		name = _name;
		calcPos(_val,_min,_max);
	}
	int calcPos(float _val=50,float _min=0,float _max=100){
		val=_val,min=_min,max=_max;
		if(val>max)
			val = max;
		if(val<min)
			val = min;
		return x3=x1+(x2-x1)*(val-min)/float(max-min);
	}
	void draw(){
		//TODO - Draw
	}
};
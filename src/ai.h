#pragma once
#include <iostream>
#include <vector>
#include <math.h>
#include <cstring>
#include <algorithm>
#include <utility>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "gameEngine.h"
#include "physics.h"
#include "network.h"
#include "gameObject.h"
#include "map.h"

using namespace std;
ofstream out("ans.txt");
#define RAD_OF_INFLUENCE 300

/**
 * @brief compare pairs based on their first element, if equal don't check the second element
 * @return true if first element of first arguement is less than first element of second arguement
 */
bool mycompare(pair<float,int> firstElem,pair<float, int> secondElem){
	return firstElem.first < secondElem.first;
}

class AI{
public:
	Pudge* bot;

	AI(){
		bot=NULL;
	}
	void init(Pudge* p){
		assert(p!=NULL && "AI_PUDGE is NULL");
		bot = p;
	}

	/**
	 * @brief calculating all the hooks coming towards point (x,y)
	 *
	 * @return pair of hooks hit by allies and hooks hit by enemies
	 */
	pair<int,int> incomingHooks(float x, float y)
	{
		int ally_hooks=0;
		int enemy_hooks=0;
		float dist_pudge = sqrt(pow(x-bot->head.cir.c.x,2)+pow(y-bot->head.cir.c.y,2));
		float delta = dist_pudge/float(PUDGE_SPEED);
		Circle c1;
		vector3 v = vector3(x,y,0);
		c1.init(v,bot->head.cir.r);

		Circle c2;
		vector3 v2;
		float pudge_slope;
		float hook_slope;
		float pudge_theta;
		float hook_theta;
		float dist;
		float delta_theta;
		for(int i=0;i<GameEngine::_()->original.head.n;i++){
			if(GameEngine::_()->original.v.at(i)->head.type==HOOK_OBJ){
				Hook* h = (Hook*)GameEngine::_()->original.v.at(i);
				// cout << "hello : " << (h->pudge == NULL) <<  endl;
				if(h->pudge->head.id != bot->head.id){
					if((h->head.is_active)&&(!(h->head.is_rebound))){
						if((x-h->head.cir.c.x)!=0){
							pudge_slope = (y-h->head.cir.c.y)/float(x-h->head.cir.c.x);
							pudge_theta = atan(pudge_slope);
						}
						else
							pudge_theta = M_PI/2.0;
						if((h->head.cir.v.x)!=0){
							hook_slope = (h->head.cir.v.y)/float(h->head.cir.v.x);
							hook_theta = atan(hook_slope);
						}
						else
							hook_theta = M_PI/2.0;
						dist = sqrt(pow(x-h->head.cir.c.x,2)+pow(y-h->head.cir.c.y,2));


						if((bot->head.cir.r + h->head.cir.r)/dist > 1)
							delta_theta = M_PI/2.0;
						else
							delta_theta = asin((bot->head.cir.r + h->head.cir.r)/dist) ;

						if(delta_theta > M_PI/2.0)
							delta_theta -= M_PI;
						cout << "hook_theta, delta_theta,pudge_theta at (" << x << "," << y << ") : " << hook_theta << "," << delta_theta << "," << pudge_theta << endl;
						if((hook_theta > pudge_theta-delta_theta)or(hook_theta < pudge_theta+delta_theta)){
							if(h->pudge->head.team != bot->head.team)
								enemy_hooks++;
							else
								ally_hooks++;
						}
					}
				}
			}
		}
		return make_pair(ally_hooks,enemy_hooks);
	}


	/**
	 * @brief function to dodge any incoming hooks, if possible
	 *
	 * @param bot [bot that has to dodge the hooksS]
	 * @return vector normal to the mean direction of all incoming hooks
	 */
	vector3 dodge(Pudge* bot)
	{
		vector3 nearest;
		float avg_x=0;
		float avg_y=0;
		int enemy_hooks=0;
		float min_dist=100000;
		Hook* min_h;
		float pudge_slope;
		float hook_slope;
		float pudge_theta;
		float hook_theta;
		float dist;
		float delta_theta;
		for(int i=0;i<GameEngine::_()->original.head.n;i++)
		{
			if(GameEngine::_()->original.v.at(i)->head.type==HOOK_OBJ)
			{
				Hook* h = (Hook*)GameEngine::_()->original.v.at(i);
				// cout << "hello : " << (h->pudge == NULL) <<  endl;
				if(h->pudge->head.id != bot->head.id){
					if((h->head.is_active)&&(!(h->head.is_rebound))){
						if((bot->head.cir.c.x-h->head.cir.c.x)!=0){
							pudge_slope = (bot->head.cir.c.y-h->head.cir.c.y)/float(bot->head.cir.c.x-h->head.cir.c.x);
							pudge_theta = atan(pudge_slope);
						}
						else
							pudge_theta = M_PI/2.0;
						if((h->head.cir.v.x)!=0){
							hook_slope = (h->head.cir.v.y)/float(h->head.cir.v.x);
							hook_theta = atan(hook_slope);
						}
						else
							hook_theta = M_PI/2.0;
						dist = sqrt(pow(bot->head.cir.c.x-h->head.cir.c.x,2)+pow(bot->head.cir.c.y-h->head.cir.c.y,2));
						if((bot->head.cir.r + h->head.cir.r)/dist > 1)
							delta_theta = M_PI/2.0;
						else
							delta_theta = asin((bot->head.cir.r + h->head.cir.r)/dist) ;

						if(delta_theta > M_PI/2.0)
							delta_theta -= M_PI;
						//cout << "hook_theta, delta_theta,pudge_theta at (" << bot->head.cir.c.x << "," << bot->head.cir.c.y << ") : " << hook_theta << "," << delta_theta << "," << pudge_theta << endl;
						if((hook_theta > pudge_theta-delta_theta)or(hook_theta < pudge_theta+delta_theta)){
							if(h->pudge->head.team != bot->head.team)
							{
								if(dist < min_dist)
									min_h = h;
								avg_x += h->head.cir.v.normalized().x;
								avg_y += h->head.cir.v.normalized().y;
								enemy_hooks++;
							}
						}
					}
				}
			}
		}
		vector3 ans;
		if(enemy_hooks>0)
		{
			if((avg_y==0)&&(avg_x==0))
				ans = vector3(-min_h->head.cir.v.y,min_h->head.cir.v.x,0);
			else
				ans = vector3(-avg_y/float(enemy_hooks),avg_x/float(enemy_hooks),0);
			//cout << "dodge answer : ";ans.print();
		}
		else
		{
			ans = vector3(0,0,0);
			//cout << "no incoming" << endl;
		}
		return ans;
	}

	/**
	 * @brief calculate the enemies in neighbourhood of a bot
	 *
	 * @return no. of enemies surrounding the bot
	 */
	int surround(Pudge* bot)
	{
		int ans=0;
		float distance=0;
		int x_diff;
		int y_diff;
		for(int i=0;i<GameEngine::_()->original.head.n;i++){
			if(GameEngine::_()->original.v.at(i)->head.type==PUDGE_OBJ){
				Pudge* p = (Pudge*)GameEngine::_()->original.v.at(i);
				if((p->head.id != bot->head.id)&&(p->head.is_active)){
					x_diff = (p->head.cir.c.x - bot->head.cir.c.x);
					y_diff = (p->head.cir.c.y - bot->head.cir.c.y);
					distance = (sqrt((x_diff*x_diff)+(y_diff*y_diff)));
					if((distance < ROT_RADIUS+p->head.cir.r)and(p->head.team != bot->head.team)and(distance != 0))
						ans++;
				}
			}
		}
		return ans;
	}


	/**
	 * @brief get value of a point (x,y) depending on several factors
	 * @details factors affecting the value : incoming hooks, surrounding enemies, chance to hook enemy or ally, health
	 *
	 * @param x [description]
	 * @param y [description]
	 *
	 * @return value evaluated
	 */
	float getVal(int x,int y){
		int enemy_hooks=0;
		int ally_hooks=0;
		vector<pair<float,int> > distance;
		int value=0;
		int enemy_surround = 0;
		int ally_surround=0;
		int enemy_in_range=0;
		Circle c1 ;
		Circle c2;
		vector3 v;
		vector3 v2;
		int x_diff;
		int y_diff;
		for(int i=0;i < GameEngine::_()->original.head.n;i++){
			if(GameEngine::_()->original.v.at(i)->head.type == PUDGE_OBJ){
				Pudge* p = (Pudge*)GameEngine::_()->original.v.at(i);
				if((p->head.id != bot->head.id)&&(p->head.is_active)){
					v = (x,y,0);
					float delta = (sqrt(pow(x-bot->head.cir.c.x,2)+pow(y-bot->head.cir.c.y,2)))/float(PUDGE_SPEED);
					c1.init(v,bot->head.cir.r,vector3(0,0,0));
					v2 = p->head.cir.c + (delta * p->head.cir.v);
					c2.init(v2,p->head.cir.r,p->head.cir.v);
					if(Physics::checkCollision(c1,c2)){
						return -100000;
					}
					else{
						x_diff = (p->head.cir.c.x + p->head.cir.v.x - x);
						y_diff = (p->head.cir.c.y + p->head.cir.v.y - y);
						float dist = sqrt((x_diff*x_diff)+(y_diff*y_diff));
						distance.push_back(make_pair(dist,i));
						if((dist < ROT_RADIUS+p->head.cir.r)and(p->head.team != bot->head.team))
							enemy_surround++;
						else if ((dist < ROT_RADIUS+p->head.cir.r)and(p->head.team == bot->head.team))
							ally_surround++;
						if((dist < bot->head.hook_range+p->head.cir.r)and(p->head.team != bot->head.team)){
							enemy_in_range++;
						}
					}
				}
			}
		}
		sort(distance.begin(),distance.end(),mycompare);
		if((!enemy_surround)&&(bot->head.rot_active)&&((x == bot->head.cir.c.x)&&(y == bot->head.cir.c.y)))
			return 100000;
		if(enemy_surround > 2)
			value -= (400);
		if(distance.size() > 0){
			Pudge* pu = (Pudge*)GameEngine::_()->original.v.at(distance.at(0).second);
			if(distance.at(0).first < bot->head.hook_range+pu->head.cir.r)
			{
				if(pu->head.rot_active)
					value -= 500;
				if(pu->head.team != bot->head.team)
				{
					// cout << "check : " << ((x == bot->head.cir.c.x)&&(y == bot->head.cir.c.y)) << endl;
					// cout << "hello : " << ((enemy_in_range)&&(bot->head.cool_down)) <<endl;
					if((x == bot->head.cir.c.x)&&(y == bot->head.cir.c.y)&&(bot->head.health > PUDGE_HP/3.0)&&(!bot->head.cool_down)&&(!enemy_surround)){
						value += 500;
					}
					if((enemy_surround)&&(bot->head.cool_down)){
						value -= 100*enemy_in_range * enemy_surround * 200/distance.at(0).first;
					}
					// else if((!enemy_surround)&&(bot->head.cool_down)){
					// 	value -= (100*enemy_in_range * 200)/distance.at(0).first;
					// }
					if((bot->head.health > PUDGE_HP/3.0)and(!bot->head.cool_down))
					{
						value += 500;
					}
				}
				else if((ally_surround)&&(!enemy_surround)){
					value -= 100 * ally_surround * 200/distance.at(0).first;
				}
				else
				{
					if(surround(pu) > 2)
						value += 200;
					else if((surround(pu)==1)&&(pu->head.health <= PUDGE_HP/2.0))
						value += 100;
				}
			}
		}
		pair<int,int> in_hooks = incomingHooks(x,y);
		ally_hooks = in_hooks.first;
		enemy_hooks = in_hooks.second;
		//cout << "incomingHooks at (" << x << "," << y <<") : " << enemy_hooks << endl;
		// if((enemy_hooks)and(bot->head.health > PUDGE_HP/3))
		// 	value -= 200;
		if((enemy_hooks))
			value -= 500;
		if(ally_hooks > 0){
			if((bot->head.health < PUDGE_HP/4.0)or(enemy_surround>2))
				value += 500;
		}
		return value;
	}



	/**
	 * @brief [main function that is called for  taking command from a bot]
	 * @return [the appropriate command to be performed by the bot]
	 */
	Command decideAction(){
		assert(bot!=NULL && "Bot is NULL");
		vector<pair<float,int> > graph_points ;
		float max_val=-90000;
		float val=0;
		float new_x=0,new_y=0;

		Circle c;
		for(int i=0;i<8;i++){
			new_x = bot->head.cir.c.x+(RAD_OF_INFLUENCE*cos(2*i*M_PI/8.0));
			new_y = bot->head.cir.c.y+(RAD_OF_INFLUENCE*sin(2*i*M_PI/8.0));
			c.init(vector3(new_x,new_y,0),bot->head.cir.r);
			if((GameEngine::_()->map.isBound(c))){
				val = getVal(new_x,new_y);
				if(val == max_val){
					graph_points.push_back(make_pair(max_val,i));
				}
				else if(val > max_val){
					max_val=val;
					graph_points.clear();
					graph_points.push_back(make_pair(max_val,i));
				}
			}
		}
		val = getVal(bot->head.cir.c.x,bot->head.cir.c.y);
		//cout << "value at native point :  " << val << endl;
		//cout << "native point : " << bot->head.cir.c.x << "," << bot->head.cir.c.y << endl;
		//cout<<"[Bot address]"<<bot<<endl;
		//cout<<"[Bot id]"<<bot->head.id<<endl;
		//cout<<"[Pudge address]"<<GameEngine::_()->original.v[2*(bot->head.id-1)]<<endl;
		// cout<<"[Pudge address]"<<GameEngine::_()->original.v[2*(bot->head.id-1)]<<endl;

		if(val==max_val){
			graph_points.push_back(make_pair(max_val,8));
		}
		else if(val > max_val){
			max_val=val;
			graph_points.clear();
			graph_points.push_back(make_pair(max_val,8));
		}

		//Sort as per value
		//sort(graph_points.begin(),graph_points.end(),mycompare);
		int r;

		if(graph_points.size() > 0){
			r = rand()%(graph_points.size());
			int i = graph_points.at(r).second;
			//cout << "graph_points.back().first : " << graph_points.at(r).first << endl;
			//cout << "graph_points.back().second : " << i << endl;
			if((surround(bot)>2))
			{
					Command c;
					c.init(bot->head.id,ROT,vector3(bot->head.cir.c.x,bot->head.cir.c.y,0));
					//cout << "in decideAction used rot ability by pudge id " << bot->head.id << " at location (" << bot->head.cir.c.x << "," << bot->head.cir.c.y << "," << bot->head.cir.c.z << ")" << endl;
					return c;

			}
			else if((incomingHooks(bot->head.cir.c.x,bot->head.cir.c.y).second)&&(bot->head.health <= PUDGE_HP/2.0))
			{
				//cout << "[DODGING]" << endl;
				vector3 target_vec = dodge(bot);
				target_vec = (RAD_OF_INFLUENCE*target_vec);
				//cout << "target_vec output : ";target_vec.print();

				Circle c2;
				c2.init(bot->head.cir.c + target_vec,bot->head.cir.r,bot->head.cir.v);
				if(!GameEngine::_()->map.isBound(c2)){
					//cout << "reverse direction" << endl;
					target_vec = (vector3(-target_vec.x,-target_vec.y,0));
				}
				target_vec = (bot->head.cir.c + target_vec);
				c2.init(target_vec,bot->head.cir.r,bot->head.cir.v);
				if((GameEngine::_()->map.isBound(c2))){
					Command c;
					c.init(bot->head.id,MOVE,target_vec);
					//cout << "dodging hence move pudge id " << bot->head.id << " to location (" << target_vec.x << "," << target_vec.y << "," << 0 << ")" << endl;
					return c;
				}
			}
			if(i==8){
				cout << "inside maxValueAction" << endl;
				return maxValueAction();
			}
			else{
				float new_x = bot->head.cir.c.x+ RAD_OF_INFLUENCE*cos(2*i*M_PI/8.0);
				float new_y = bot->head.cir.c.y + RAD_OF_INFLUENCE*sin(2*i*M_PI/8.0);
				Command c;
				Circle c1;
				c1.init(vector3(new_x,new_y,0),bot->head.cir.r);
				if(!(GameEngine::_()->map.isBound(c1))){
					return maxValueAction();
				}
				c.init(bot->head.id,MOVE,vector3(new_x,new_y,0));
				//cout << "MAX_VAL move pudge id " << bot->head.id << " to location (" << new_x << "," << new_y << "," << 0 << ") and max_value = " << max_val << endl;
				return c;
			}
		}
		else
		{
			Command c;
			c.init(bot->head.id,MOVE,bot->head.cir.c);
			cout << "MAX_VAL graphpoint size 0; move pudge id " << bot->head.id << " to location (" << bot->head.cir.c.x << "," << bot->head.cir.c.y << "," << 0 << ")" << endl;
			return c;
		}

	}


	/**
	 * @brief take most appropriate action at current position of bot
	 * @return decision to be taken
	 */
	Command maxValueAction()
	{
		vector<pair<float,int> > distance;
		int enemy_surround = 0;
		int ally_surround=0;
		int enemy_in_range=0;
		float dist=0;
		float avg_x=0;
		float avg_y=0;
		for(int i=0;i < GameEngine::_()->original.head.n;i++){
			if(GameEngine::_()->original.v.at(i)->head.type==PUDGE_OBJ){
				Pudge* p = (Pudge*)GameEngine::_()->original.v.at(i);
				if((p->head.id != bot->head.id)&&(p->head.is_active))
				{

					vector3 diff_vec = (p->head.cir.c)-(bot->head.cir.c);

					dist = sqrt((diff_vec.x * diff_vec.x)+(diff_vec.y * diff_vec.y));
					distance.push_back(make_pair(dist,i));
					if((dist < ROT_RADIUS+p->head.cir.r) && (p->head.team != bot->head.team))
					{
						enemy_surround++;
						avg_x += p->head.cir.c.x;
						avg_y += p->head.cir.c.y;
					}
					if((dist < ROT_RADIUS+p->head.cir.r) && (p->head.team == bot->head.team))
						ally_surround++;
					if((dist < bot->head.hook_range+p->head.cir.r)&&(p->head.team != bot->head.team)){
						enemy_in_range++;
					}
				}
			}
		}
		if(enemy_in_range>0){
			avg_x /= enemy_in_range;
			avg_y /= enemy_in_range;
		}
		pair<int,int> in_hooks = incomingHooks(bot->head.cir.c.x,bot->head.cir.c.y);
		int ally_hooks=in_hooks.first,enemy_hooks=in_hooks.second;
		sort(distance.begin(),distance.end(),mycompare);
		if(distance.size()>0)
		{
			int j = distance.at(0).second;
			Pudge* np = (Pudge*)GameEngine::_()->original.v.at(j);
			if((enemy_surround > 2)&&(!(bot->head.rot_active))&&(bot->head.cool_down)&&(bot->head.health < PUDGE_HP/2.0))
			{
				Command c;

				c.init(bot->head.id,ROT,vector3(bot->head.cir.c.x,bot->head.cir.c.y,0));
				//cout << "surrounded used rot ability by pudge id " << bot->head.id << " at location (" << bot->head.cir.c.x << "," << bot->head.cir.c.y << "," << bot->head.cir.c.z << ")" << endl;


				return c;									//if surrounded use rot ability
			}
			else if((enemy_surround==1)&&(np->head.health < bot->head.health/4.0)&&(!bot->head.rot_active))
			{
				Command c;
				c.init(bot->head.id,ROT,vector3(bot->head.cir.c.x,bot->head.cir.c.y,0));
				//cout << "used rot ability by pudge id " << bot->head.id << " at location (" << bot->head.cir.c.x << "," << bot->head.cir.c.y << "," << bot->head.cir.c.z << ")" << endl;
				return c;
			}
			else if((!enemy_surround)&&(bot->head.rot_active))
			{
				Command c;

				c.init(bot->head.id,ROT,vector3(bot->head.cir.c.x,bot->head.cir.c.y,0));
				//cout << "disable rot ability by pudge id " << bot->head.id << " at location (" << bot->head.cir.c.x << "," << bot->head.cir.c.y << "," << bot->head.cir.c.z << ")" << endl;
				return c;

			}
			else if((distance.at(0).first < bot->head.hook_range+np->head.cir.r)&&((bot->head.cool_down==0))&&((np->head.team != bot->head.team)or(surround(np) > 2)))
			{
				Command c;
				float delta = (distance.at(0).first/float(HOOK_SPEED));
				vector3 v2 = np->head.cir.c + (delta*np->head.cir.v);
				c.init(bot->head.id,HOOK,v2);
				//cout << "hook used by pudge id " << bot->head.id << " from location (" << bot->head.cir.c.x << "," << bot->head.cir.c.y << "," << 0 << ") and target location (" << v2.x<< "," << v2.y << "," << 0 << ")" << endl;
				return c;
			}
			else if((((bot->head.cool_down>0))&&(enemy_surround>0))or(np->head.rot_active))
			{
				vector3 ve = vector3(avg_x,avg_y,0);
				vector3 nve = (bot->head.cir.c - ve);
				vector3 target_vec;
				vector3 n_closest_vec;
				if((nve.x==0)&&(nve.y==0))
				{
					n_closest_vec = (bot->head.cir.c - np->head.cir.c);
					n_closest_vec = n_closest_vec.normalized();
				}
				else
					nve = nve.normalized();
				float new_x = 0;
				float new_y = 0;
				int r=RAD_OF_INFLUENCE;
				Circle check_c;
				check_c.init(vector3(new_x,new_y,0),bot->head.cir.r,bot->head.cir.v);
				while((!(GameEngine::_()->map.isBound(check_c)))&&(r>=1)){
					if((nve.x == 0)&&(nve.y==0))
					{
						target_vec = bot->head.cir.c + r*n_closest_vec;
					}
					else
					{
						target_vec = bot->head.cir.c + r*nve;
					}
					new_x = target_vec.x;
					new_y = target_vec.y;
					check_c.init(target_vec,bot->head.cir.r,bot->head.cir.v);
					r = r/4;
				}
				if(r==0){
					target_vec = bot->head.cir.c;
					new_x = target_vec.x;
					new_y = target_vec.y;
				}
				r=0;
				// int a;
				Circle c_cir;
				c_cir.init(target_vec,bot->head.cir.r,bot->head.cir.v);

				if((!(incomingHooks(new_x,new_y).second))&&(GameEngine::_()->map.isBound(c_cir))){
						target_vec = vector3(new_x,new_y,0);
						//cout << "no incoming and in bound : ";target_vec.print();
				}
				else{
					while((r<10)){
						// a = rand()%(2*RAD_OF_INFLUENCE) - RAD_OF_INFLUENCE;
						new_x = bot->head.cir.c.x + (RAD_OF_INFLUENCE*cos(2*r*M_PI/10.0));
						new_y = bot->head.cir.c.y + (RAD_OF_INFLUENCE*sin(2*r*M_PI/10.0));
						c_cir.init(vector3(new_x,new_y,0),bot->head.cir.r,bot->head.cir.v);
						if((!(incomingHooks(new_x,new_y).second))&&(GameEngine::_()->map.isBound(c_cir))){
							target_vec = vector3(new_x,new_y,0);
							//cout << "no incoming and in bound : ";target_vec.print();
							break;
						}
						r++;
					}
					r=0;
					while((!(GameEngine::_()->map.isBound(c_cir)))&&(r<10)){
						cout << "vector not in bound : "; target_vec.print();
						target_vec.x = bot->head.cir.c.x + (RAD_OF_INFLUENCE*cos(2*r*M_PI/10.0)/3.0);
						target_vec.y = bot->head.cir.c.y + (RAD_OF_INFLUENCE*sin(2*r*M_PI/10.0)/3.0);
						c_cir.init(target_vec,bot->head.cir.r,bot->head.cir.v);
						r++;
					}
					if(!(GameEngine::_()->map.isBound(c_cir))){
						target_vec = bot->head.cir.c;
					}
				}
				Command c;
				c.init(bot->head.id,MOVE,target_vec);
				//cout << "cooldown > 0 hence move pudge id " << bot->head.id << " to location (" << target_vec.x << "," << target_vec.y << "," << 0 << ")" << endl;
				return c;
			}
			else
			{
				//cout << "[DODGING] in maxValueAction" << endl;
				vector3 target_vec = dodge(bot);
				target_vec = (RAD_OF_INFLUENCE*target_vec);
				//cout << "target_vec output after max_dodge : ";target_vec.print();

				Circle c2;
				c2.init(bot->head.cir.c + target_vec,bot->head.cir.r,bot->head.cir.v);
				if(!GameEngine::_()->map.isBound(c2)){
					//cout << "reverse direction" << endl;
					target_vec = (vector3(-target_vec.x,-target_vec.y,0));
				}
				target_vec = (bot->head.cir.c + target_vec);
				c2.init(target_vec,bot->head.cir.r,bot->head.cir.v);

				if((!(GameEngine::_()->map.isBound(c2)))or(target_vec == bot->head.cir.c)){
					int k=0;
					float a;
					float b;
					while(k<10)
					{
						a = bot->head.cir.c.x + (RAD_OF_INFLUENCE*sin(2*k*M_PI/10));
						b = bot->head.cir.c.x + (RAD_OF_INFLUENCE*cos(2*k*M_PI/10));
						c2.init(vector3(a,b,0),bot->head.cir.r,bot->head.cir.v);
						if(GameEngine::_()->map.isBound(c2))
							break;
					}
					target_vec = vector3(a,b,0);
					if(!(GameEngine::_()->map.isBound(c2)))
					{
						target_vec = bot->head.cir.c;
					}
				}
					Command c;
					c.init(bot->head.id,MOVE,target_vec);
					cout << "dodging max_value hence move pudge id " << bot->head.id << " to location (" << target_vec.x << "," << target_vec.y << "," << 0 << ")" << endl;
					return c;
				// int k =0;
				// float new_x=0;
				// float new_y=0;
				// float pudge_slope;
				// float hook_slope;
				// float pudge_theta;
				// float hook_theta;
				// float dist;
				// float delta_theta;
				// while(k<10){
				// 	// int a = (rand()%(2*RAD_OF_INFLUENCE))-RAD_OF_INFLUENCE;
				// 	new_x = bot->head.cir.c.x + (RAD_OF_INFLUENCE*cos(2*k*M_PI/10.0));
				// 	new_y = bot->head.cir.c.y + (RAD_OF_INFLUENCE*sin(2*k*M_PI/10.0));
				// 	vector3 n_vec = vector3(new_x,new_y,0);
				// 	Circle c;
				// 	c.init(n_vec,bot->head.cir.r);
				// 	Circle c2;
				// 	vector3 v2;

				// 	if(GameEngine::_()->map.isBound(c)){
				// 		bool collision=false;
				// 		int in_hooks = 0;
				// 		for(int i=0;i<GameEngine::_()->original.head.n;i++){
				// 			if(GameEngine::_()->original.v.at(i)->head.type == PUDGE_OBJ){
				// 				Pudge* p = (Pudge*)GameEngine::_()->original.v.at(i);
				// 				if(p->head.id != bot->head.id){
				// 					collision = (collision or Physics::checkCollision(c,p->head.cir));
				// 				}
				// 			}
				// 			else{
				// 				Hook* h = (Hook*)GameEngine::_()->original.v.at(i);

				// 				if(h->pudge->head.team != bot->head.team){
				// 					if((h->head.is_active)&&(!(h->head.is_rebound))){
				// 						if((bot->head.cir.c.x-h->head.cir.c.x)!=0){
				// 							pudge_slope = (bot->head.cir.c.y-h->head.cir.c.y)/float(bot->head.cir.c.x-h->head.cir.c.x);
				// 							pudge_theta = atan(pudge_slope);
				// 						}
				// 						else
				// 							pudge_theta = M_PI/2.0;
				// 						if((h->head.cir.v.x)!=0){
				// 							hook_slope = (h->head.cir.v.y)/float(h->head.cir.v.x);
				// 							hook_theta = atan(hook_slope);
				// 						}
				// 						else
				// 							hook_theta = M_PI/2.0;
				// 						dist = sqrt(pow(bot->head.cir.c.x-h->head.cir.c.x,2)+pow(bot->head.cir.c.y-h->head.cir.c.y,2));
				// 						if((bot->head.cir.r + h->head.cir.r)/dist > 1)
				// 							delta_theta = M_PI/2.0;
				// 						else
				// 							delta_theta = asin((bot->head.cir.r + h->head.cir.r)/dist) ;

				// 						if(delta_theta > M_PI/2.0)
				// 							delta_theta -= M_PI;
				// 						cout << "hook_theta, delta_theta,pudge_theta at (" << bot->head.cir.c.x << "," << bot->head.cir.c.y << ") : " << hook_theta << "," << delta_theta << "," << pudge_theta << endl;
				// 						if((hook_theta > pudge_theta-delta_theta)or(hook_theta < pudge_theta+delta_theta)){
				// 							if(h->pudge->head.team != bot->head.team)
				// 							{
				// 								if(dist < min_dist)
				// 									min_h = h;
				// 								avg_x += h->head.cir.v.normalized().x;
				// 								avg_y += h->head.cir.v.normalized().y;
				// 								enemy_hooks++;
				// 							}
				// 						}
				// 					}
				// 				}
				// 			}
				// 		}
				// 		if((in_hooks == 0)&&(!collision))
				// 		{
				// 			break;
				// 		}
				// 	}
				// 		k++;
				// }
				// Circle c1;
				// c1.init(vector3(new_x,new_y,0),bot->head.cir.r);

				// int r=0;
				// vector3 target_vec = vector3(new_x,new_y,0.0);
				// while((!(GameEngine::_()->map.isBound(c1)))&&(r<10)){
				// 	cout << "vector not in bound : "; target_vec.print();
				// 	target_vec.x = bot->head.cir.c.x + (RAD_OF_INFLUENCE*cos(2*r*M_PI/10.0)/3.0);
				// 	target_vec.y = bot->head.cir.c.y + (RAD_OF_INFLUENCE*sin(2*r*M_PI/10.0)/3.0);
				// 	c1.init(target_vec,bot->head.cir.r,bot->head.cir.v);
				// 	r++;
				// }
				// if(!(GameEngine::_()->map.isBound(c1))){
				// 	target_vec = bot->head.cir.c;
				// }
				// Command c;

				// c.init(bot->head.id,MOVE,target_vec);
				// cout << "random move pudge id " << bot->head.id << " to location (" << new_x << "," << new_y << "," << 0 << ")" << endl;


				// return c;
			}
		}
		else{
			Command c;
			float new_x=0,new_y=0;
			int r = RAD_OF_INFLUENCE;
			int i=0;
			Circle check_c;
			check_c.init(vector3(new_x,new_y,0),bot->head.cir.r,bot->head.cir.v);
			while((!GameEngine::_()->map.isBound(check_c))&&(r>0))
			{
				// int a = (rand()%(2*r))-r;
				new_x = bot->head.cir.c.x+(r*cos(2*i*M_PI/8.0));
				new_y = (bot->head.cir.c.y+(r*sin(2*i*M_PI/8.0)));
				if(i==8)
				{
					r = r/2;
					i=0;
				}
				check_c.init(vector3(new_x,new_y,0),bot->head.cir.r,bot->head.cir.v);
				i++;
			}
			if(!(GameEngine::_()->map.isBound(check_c))){
					new_x = bot->head.cir.c.x;
					new_y = bot->head.cir.c.y;
			}

			//cout << "maxValueAction moveTo : (" << new_x << "," << new_y << ")" << endl;

			c.init(bot->head.id,MOVE,vector3(new_x,new_y,0));
			//cout << "single pudge move pudge id " << bot->head.id  << " to location (" << new_x << "," << new_y << "," << 0 << ")" << endl;


			return c;
		}
	}


};



#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <GL/glu.h>
#include "log.h"
#include "window.h"

int main( int argc, char** argv )
{
	auto gui=GUI::_();
  gui->initGUI(argc,argv);

	SDL_Event e;
	SDL_StartTextInput();

	while(!Window::_()->quit)
	{
		//Handle events on queue
		while( SDL_PollEvent( &e ) != 0 ){
			//User requests quit
			if( e.type == SDL_QUIT ){
				Window::_()->quit = true;
			}
			//Handle keypress with current mouse position
			else if( e.type == SDL_TEXTINPUT ){
				int x = 0, y = 0;
				SDL_GetMouseState( &x, &y );
				Window::_()->handleKeys( e.text.text[ 0 ], x, y );
			}
			else if(e.type == SDL_MOUSEMOTION){
        Window::_()->mouseCallback(e.motion.state&SDL_BUTTON(SDL_BUTTON_LEFT),e.motion.state&SDL_BUTTON(SDL_BUTTON_RIGHT),e.motion.x,e.motion.y);
			}else if(e.type==SDL_MOUSEWHEEL){
        GUI::_()->scrollWheel(e.wheel.y);
      }else if(e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP ){

        Window::_()->mouseCallback(e.button.state==SDL_PRESSED && e.button.button==SDL_BUTTON_LEFT,
          e.button.state==SDL_PRESSED && e.button.button==SDL_BUTTON_RIGHT,e.button.x,e.button.y);
      }
			else if(e.type == SDL_KEYDOWN){
				if(e.key.keysym.sym == SDLK_p){
					//TODO - pause
				}
			}
		}
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Window::_()->displayCallback();
		//Update screen
		SDL_GL_SwapWindow( gui->gWindow );
	}
  gui->destroy();
	return 0;
}
#include <iostream>
#include <vector>
#include <GL/glut.h>
#include <GL/freeglut.h>
#include <cstdlib>
#include "../network.h"
#include "../command.h" 
#include "../gameEngine.h"

#include <unistd.h>
#include <chrono>
#include <ctime>
#include "../timer.h"

#include "../ai.h"
using namespace std;

int cnt =0;
//This is clock for the display event loop
//We evaluate the number of simulations from it
static int64_t initial_time = Timer::now();

NetworkPlayer np;
bool display_start_screen=false;
Timer t;

Timer recon_timer;
vector<char> buff;
void processNetwork(){
	np.processEvent();
	if(np.connectedPeers() == 1 && !np.ingame && np.isHost && !np.countdown){
		np.start(Timer::now() + 6000);
		t.start(1000);
		cout<<"Started"<<endl;
	}
	if(np.ingame){
		np.sync();
	}
	if(np.countdown){
		if(Timer::now() > np.start_time){
			cout<<"Start now"<<endl;
			np.countdown = false;
			np.ingame = true;
		}
	}
	if(np.isDC && !np.isRC){
		cout<<"Enter 1 to reconnect"<<endl;
		int n;
		cin>>n;
		//cout<<n<<" --"<<endl;
		if(n == 1){
			logInfo("trying to reconnect\n");

			recon_timer.start(5000);

			np.isRC = true;
			np.reconnect();
		}
	}
	if(np.isRC){
		if(recon_timer.isTimeout()){
			logInfo("failed to connect to the tried peer... trying the next peer");
			np.peer_to_try++;
			np.reconnect();

			recon_timer.start(5000);
		}
	}
}

//To avoid floating point determinism, we have kept the frame rate constant. We just perform simulation as many times as possible
void displayStartScreen(){
	
}
void display(){
	if(display_start_screen){
		//displayStartScreen();
	}
	else{
		if(t.isTimeout() && np.ingame && is_ai){
			cout<<"Time out"<<endl;
			//Just at this moment the command is applied
			Command c = ai.decideAction();
			// c.init(ai.bot->head.id,MOVE,vec3(rand()%400,rand()%400,rand()%400));
			GameEngine::_()->applyCommand(c);
	 		np.pushCommand(c);
	 		t.start(500);
		}
		glClear(GL_COLOR_BUFFER_BIT);
		processNetwork();
		// if(np.ingame){
		// 	cnt++;
		// 	cout<<cnt<<endl;
			
		// }
		
		int64_t final_time = Timer::now();
		
		uint32_t n = GameEngine::_()->numberOfSimulations(final_time-initial_time + GameEngine::_()->accumulated);
		
		GameEngine::_()->accumulated =(final_time+GameEngine::_()->accumulated-initial_time)-n*GameEngine::_()->tp;
		for(int i=0;i<n;i++){
			GameEngine::_()->simulate();
		}

	initial_time=final_time;
	
	//GameEngine::_()->checkAllCollision();

	//This is the actual rendering part of all
	for(int i=0;i<GameEngine::_()->original.v.size();i++){
		GameEngine::_()->original.v[i]->draw();
	}
}
	glFlush();
}
void mouse(int button,int state,int x,int y)
{	if(!np.ingame) return;
if(!is_ai){
	cout<<x<<" "<<y<<endl;
	switch (button)
	{
	  	case GLUT_LEFT_BUTTON:
    	  	if(state == GLUT_DOWN){
    	  		//Just at this moment the command is applied

    	  		if(true)
    	  		{	cout<<"left"<<endl;
    	  			Command c;
    	  			c.init(np.my_data.id,MOVE,vector3(x,400-y,0));
    	  			c.timestamp = Timer::now();
    	  			GameEngine::_()->applyCommand(c);
    	  			np.pushCommand(c);
    	  		}
    	  	}
	  		break;
	  	case GLUT_RIGHT_BUTTON:
	  		if(state == GLUT_DOWN){
	  		// GameEngine::_()->is_pause = !(GameEngine::_()->is_pause);
	  		// np.sendPauseMessage();
	  			// //Just at this moment the command is applied
    	  		Command c;
    	  		c.init(np.my_data.id,HOOK,vector3(x,400-y,0));
    	  		c.timestamp = Timer::now();
    	  		GameEngine::_()->applyCommand(c);
    	  		np.pushCommand(c);
    	  	}
    	  	break;
	    default:
	    	break;
	}
}
}
void init(int& argc,char **argv){
	GameEngine::_()->map.init();
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(400,400);
	glutInitWindowPosition(50,50);
	glutCreateWindow("Test");
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0,400,0,400);
}
int main(int argc,char**argv){
	init(argc,argv);
	srand(0);


	int input_ai;
	cout<<"Input the AI:";

	//input_ai = 1; // for ai testing.......DELETE WHEN RUNNING IN MANUAL MODE!!!!!
	 cin>>input_ai;

	is_ai = input_ai;

	string str="",client_port = "",host_ip = "",host_port = "";
	
	cout << "host/client : ";
	cin >> str;
	
	int max= 3;
	if(str == "h")
	{
		cout<<"enter your ip";
		//cin>>ip;

		ip="localhost";
		ENetAddress addr;
		enet_address_set_host(&addr,"127.0.0.1");
		cout<<addr.host<<endl;
		np.init(true,"",0);	
	}
	else if(str == "c")
	{
		string str;
		cout<<"enter ip : ";
		// cin>>str;
		str="localhost";

		cout << "Specify  port : ";
		// cin >> client_port;
		client_port="1111";
		np.init(false,str, atoi(client_port.c_str()));
	}
	else{
		cout << "invalid operation!" << endl;
		exit(0);
	}
	t.start(3000);
	glutDisplayFunc(display);
	glutIdleFunc(display);
	glutMouseFunc(mouse);



	glutMainLoop();


	
	return 0;
}

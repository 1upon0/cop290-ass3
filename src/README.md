1. MAKEFILE commands :
	1. 'make game' to test Game Engine, Networking and AI.
		'make run-game' to execute.
		Enter IP and whether to play in human mode or AI mode when asked.
	2. 'make main' to test disconnection and reconnection events.
		'make run HOST=<h> PLAYERS=<p> IP=<ip> PORT=<port>' to execute where :
			<h> = 1 if running as host and 0 if running as a peer
			<p> = no. of players allowed in the game
			<ip> = I.P. address of the host
			<port> = port number of host
	3. 'cd graphics && make' to test the game along with graphics
		'cd graphics && make run HOST=<h> PLAYERS=<p> IP=<ip> PORT=<port>' to execute where :
			<h> = 1 if running as host and 0 if running as a peer
			<p> = no. of players allowed in the game
			<ip> = I.P. address of the host
			<port> = port number of host
	4. 'make doc' to create documentation pdf from latex. 
		
2. Keyboard Commands :
	1. 'p' to pause the game.
	2. 'r' to toggle the rot ability.
	3. 'c' to reconnect.
	4. 's' to initialize game and allow connections. 
	5. 'a' to play in AI mode.
	6. 'h' to play in Human mode.

3. Mouse :
	1. Left click on the desired location to move the pudge to that location.
	2. Right click to initiate hook in the direction of click.
	3. 'AI MODE' button to start game in AI mode.
	4. 'HUMAN MODE'button to start game in Human Mode.
	5. 'START' button to start the game.
	6. 'PAUSE' button to pause the game.
	7. 'RECONNECT' button to reconnect upon reestablishment of network.
	8. 'EXIT' button to end game.

4. Link to google drive : https://drive.google.com/drive/folders/0B5Su_TnII-b9fjFFMUlwWUhpMlNJYVpwLW1pbkgtNEU1Y0FWQ1FOYkd6WmdwTHBjTHpLZ1k

5. github repository link : https://bitbucket.org/1upon0/cop290-ass3
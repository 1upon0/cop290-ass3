#pragma once
#include <iostream>
#include <vector>
#include <cstring>
#include <cstdlib>

#include "timer.h"
#include "vector3.h"

#define MOVE 1
#define ROT 2
#define HOOK 3

class Command{
public:
	uint32_t id;
	uint32_t type;
	int64_t timestamp;
	vector3 v;
	Command(){
		id = 0;
		type = 0;
		//Actually time stamp is not important here. But imp at point of application
		timestamp = Timer::now();
	}
	/**
	 * @brief initialise a command object
	 * 
	 * @param _id pudge id which sent the command
	 * @param _type move, rot or hook
	 * @param _v position corresponding to the type
	 */
  void init(uint32_t _id,uint32_t _type,vector3 _v = vector3(0,0,0)){
		id = _id;
		v = _v;
		type = _type;
		timestamp = Timer::now();
	}
};

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "boilerplate.h"
#include "skeletal.h"
#include "text.h"
#include "skybox.h"
class GUI{
  static GUI * singleton;
  GUI(){
    assert(!singleton);
    singleton=this;
    width=SCREEN_WIDTH;
    height=SCREEN_HEIGHT;
    window=Window::_();
  }
public:
  Window *window;
  float width,height;
  static GUI *_(){
    if(!singleton)
      new GUI();

    return singleton;
  }
  //The window we'll be rendering to
  SDL_Window* gWindow = NULL;
  //OpenGL context
  SDL_GLContext gContext;
  Assimp::Importer aiImporter;
  SkeletalObj puj;

  GLDirectObject banner;
  GLObject gamemap;
  GLuint bg_tex,button_idle_tex,button_hover_tex,button_press_tex,banner_tex,map_tex;
  struct HUDvert{
    glm::vec2 pos;
    glm::vec2 tex;
  };
  struct MapVert{
    glm::vec3 pos;
    glm::vec3 norm;
    glm::vec2 tex;
  };
  SkyBox sky;
  struct HealthVert{
    glm::vec3 pos;
    glm::vec2 tex;
  };
  vector<HealthVert> health_verts;
  GLDirectObject health_obj;
  GLuint health_tex[4];
  void init_res(){
    //Initialize clear color
    glClearColor( 0.f,0.f, 0.1f, 1.f );

    puj.load("res/pudge_model.fbx");

    bg_tex=GetRenderer()->loadTex("res/hud/home-screen.png");
    banner_tex=GetRenderer()->loadTex("res/hud/banner.png");
    button_hover_tex=GetRenderer()->loadTex("res/hud/button_hover.png");
    button_idle_tex=GetRenderer()->loadTex("res/hud/button_idle.png");
    button_press_tex=GetRenderer()->loadTex("res/hud/button_press.png");
    map_tex=GetRenderer()->loadTex("res/map.png");
    HUDvert verts[4];
    verts[0].pos=glm::vec2(-1,-1);verts[0].tex=glm::vec2(0,1);
    verts[1].pos=glm::vec2(1,-1);verts[1].tex=glm::vec2(1,1);
    verts[2].pos=glm::vec2(1,1);verts[2].tex=glm::vec2(1,0);
    verts[3].pos=glm::vec2(-1,1);verts[3].tex=glm::vec2(0,0);
    banner.loadShaders("shaders/hud-vert.glsl","shaders/plain-frag.glsl");
    banner.addAttrib("Position",offsetof(HUDvert,pos),2,GL_FLOAT);
    banner.addAttrib("TexCoord",offsetof(HUDvert,tex),2,GL_FLOAT);
    banner.bindBuffers(verts,sizeof(HUDvert),4);


    vector<MapVert> mapverts;
    vector<uint> mapindices;
    auto m=GameEngine::_()->map;
    int w=100,h=100;
    for(int j=0;j<w;j++){
      float x=(2.0*j/w-1);
      for(int k=0;k<h;k++){
        float y=(1-2.0*k/h);

        float H=sin((x*x+y*y+fabs(y))*40)*5.f;
        float X=x*(m.xmax-m.xmin)/2.0;
        float Y=y*(m.ymax-m.ymin)/2.0;
        if(fabs(X)>(140) && fabs(X)<(190)){
          H+=20+fabs(fabs(X)-190);
        }else if(fabs(X)<(200-50)){
          H+=-40;
        }else{
          H=sin((x*x+y*y+fabs(x))*6.1)+cos((x*x+y*y+fabs(y))*10);
          H*=4;
          H+=sin(x*100)+cos(y*100);
        }
        mapverts.push_back(MapVert({glm::vec3(X,H,Y),glm::vec3(0,0,0),glm::vec2(x/2+0.5,0.5-y/2)}));
        if(k!=0 && j!=0){
          mapindices.push_back(j*h+k);
          mapindices.push_back((j-1)*h+k);
          mapindices.push_back((j-1)*h+(k-1));

          mapindices.push_back(j*h+k);
          mapindices.push_back((j-1)*h+k-1);
          mapindices.push_back(j*h+k-1);
        }
      }
    }
    // cout<<"Reached"<<mapverts.size()<<"\n";
    for(size_t i=0;i<mapindices.size();i+=3){
      // cout<<mapindices[i]<<" "<<mapindices[i+1]<<" "<<mapindices[i+2]<<".\n";
      auto &a=mapverts[mapindices[i]];
      auto &b=mapverts[mapindices[i+1]];
      auto &c=mapverts[mapindices[i+2]];
      vec3 n=glm::normalize(glm::cross(a.pos-b.pos,a.pos-c.pos));
      a.norm += n;
      b.norm += n;
      c.norm += n;
    }

    gamemap.loadShaders("shaders/phong-vert.glsl","shaders/phong-frag.glsl");
    gamemap.addAttrib("Position",offsetof(MapVert,pos),3,GL_FLOAT);
    gamemap.addAttrib("Normal",offsetof(MapVert,norm),3,GL_FLOAT);
    gamemap.addAttrib("TexCoord",offsetof(MapVert,tex),2,GL_FLOAT);
    gamemap.bindBuffers(mapverts.data(),sizeof(MapVert),mapverts.size(),mapindices.data(),sizeof(uint),mapindices.size());


    health_verts.push_back({vec3(-50,10,0),vec2(0,1)});
    health_verts.push_back({vec3(-50,-10,0),vec2(0,0)});
    health_verts.push_back({vec3(0,10,0),vec2(0.5,1)});
    health_verts.push_back({vec3(0,-10,0),vec2(0.5,0)});
    health_verts.push_back({vec3(0,10,0),vec2(0.5,1)});
    health_verts.push_back({vec3(0,-10,0),vec2(0.5,0)});
    health_verts.push_back({vec3(50,10,0),vec2(1,1)});
    health_verts.push_back({vec3(50,-10,0),vec2(1,0)});

    health_tex[0]=GetRenderer()->loadTex("res/health-green.png");
    health_tex[1]=GetRenderer()->loadTex("res/health-red.png");
    health_tex[2]=GetRenderer()->loadTex("res/health-blue.png");
    health_tex[3]=GetRenderer()->loadTex("res/health-yellow.png");

    health_obj.loadShaders("shaders/bill-sphere-vert.glsl","shaders/plain-frag.glsl");
    health_obj.addAttrib("Position",offsetof(HealthVert,pos),3);
    health_obj.addAttrib("TexCoord",offsetof(HealthVert,tex),2);
    health_obj.bindBuffers(health_verts.data(),sizeof(HealthVert),health_verts.size());

    sky.load("res/sky/waters_");
  }
  void destroy(){
    glDeleteProgram(puj.obj.gProgramID);

    SDL_StopTextInput();
    SDL_DestroyWindow(gWindow );
    gWindow = NULL;
    SDL_Quit();
  }

  glm::mat4x4 matVP,matV,matP;
  glm::vec3 eye_pos,look_at,up_dir;
  float proj_near,proj_far,proj_fov;

  void  initGUI(int argc,char **argv){
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
      logError( "SDL could not initialize! SDL Error: %s\n", SDL_GetError());
      assert(false);
    }
    //Use OpenGL 3.1 core
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 3 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
    //Create window
    gWindow = SDL_CreateWindow("Pudge Wars - COP290", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
    if( gWindow == NULL ){
      logError( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
      assert(false);
    }
    SDL_SetWindowFullscreen(gWindow,SDL_WINDOW_FULLSCREEN_DESKTOP);
    //Create context
    gContext = SDL_GL_CreateContext( gWindow );
    if( gContext == NULL ){
      logError( "OpenGL context could not be created! SDL Error: %s\n", SDL_GetError() );
      assert(false);
    }
    //Initialize GLEW
    glewExperimental = GL_TRUE;
    GLenum glewError = glewInit();
    if( glewError != GLEW_OK ){
      logError( "Error initializing GLEW! %s\n", glewGetErrorString( glewError ) );
    }
    //Use Vsync
    if( SDL_GL_SetSwapInterval(1) < 0){
      logError( "Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError() );
    }

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);


    eye_pos=vec3(1000,1000,1000);
    look_at=vec3(0,0,0);
    up_dir=vec3(0,1,0);
    proj_fov=45;
    proj_near=10;
    proj_far=5000;
    zoom=-400;right=top=0;

    calcVP();

    init_res();

    Window::_()->init(argc,argv);
  }
  void calcVP(){
    eye_pos.x=(1000+zoom)+right-top;
    eye_pos.z=(1000+zoom)-right-top;
    eye_pos.y=1000+zoom;
    look_at.x=right-top;
    look_at.z=-right-top;

    matP = glm::perspectiveFov(proj_fov,width,height, proj_near,proj_far);
      // Camera matrix
    matV       = glm::lookAt(eye_pos, look_at, up_dir);
    matVP=matP*matV;
  }
  void renderBanner(float x1,float y1,float x2,float y2,uint tex){
    assert(y2>=y1 && x2>=x1);
    banner.preRender();
    glm::mat4 screen=translate(mat4(1),vec3(-1+(x1+x2)/width,1-(y1+y2)/height,0))*scale(mat4(1),vec3((x2-x1)/width,(y2-y1)/height,1));
    glUniformMatrix4fv(0, 1, GL_FALSE, &screen[0][0]);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex);
    banner.render(GL_TRIANGLE_FAN);
  }
  void renderHealth(float x,float y,float z,float percentage=50,int team=0){
    health_verts[3].pos.x=health_verts[5].pos.x=health_verts[2].pos.x=health_verts[4].pos.x=clamp(percentage,0,100)-50;
    health_obj.bindBuffers(health_verts.data(),sizeof(HealthVert),health_verts.size());

    health_obj.preRender();
    glm::mat4 MV=matV*translate(mat4(1),vec3(x,y,z));
    glUniformMatrix4fv(glGetUniformLocation(health_obj.gProgramID,"gMV"), 1, GL_FALSE, &MV[0][0]);
    glUniformMatrix4fv(glGetUniformLocation(health_obj.gProgramID,"gP"), 1, GL_FALSE, &matP[0][0]);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, health_tex[team]);
    health_obj.render(GL_TRIANGLE_STRIP);

    // RenderText(str);
  }
  void renderButton(const Button &btn){
    uint tex=button_idle_tex;
    if(btn.state==Button::state_hover)
      tex=button_hover_tex;
    else if(btn.state==Button::state_pressed)
      tex=button_press_tex;
    renderBanner(btn.x1,btn.y1,btn.x2,btn.y2,tex);
    RenderText(btn.name,btn.x1,btn.y1,btn.x2,btn.y2);
  }
  void screenSaver(){
    static float theta=0;
    theta += 0.01;

    glm::mat4 MVP;

    renderMap(GameEngine::_()->map);
    // puj.setCurAnim("res/pudge_walkN.smd");
    // for(int i=0;i<20;i++){
    //     puj.calcBoneTransform(theta+i/20.0);
    //     puj.fillBoneBuffer();
    //     puj.preRender();
    //     MVP =  matVP * glm::rotate(theta*0.2f+i*3.1415f/10, glm::vec3(0,1,0)) * glm::translate(glm::mat4(1),vec3(1100,-70,0)) * glm::rotate(-3.1415f/2, glm::vec3(0,1,0));
    //     glUniformMatrix4fv(0, 1, GL_FALSE, &MVP[0][0]);
    //     puj.render();
    // }

    puj.setCurAnim("res/pudge_idle.smd");
    for(int i=-4;i<=4;i++){
      for(int j=-4;j<=4;j++){
        puj.calcBoneTransform((theta+1+i/8.0+j/8.0)*(1.5+i/16.0+j/16.0));
        puj.fillBoneBuffer();
        puj.preRender();
        MVP =  matVP * glm::translate(glm::mat4(1),vec3(i/4.0*1000,-70,j/4.0*1000)) * glm::rotate(-3.1415f/4*(i+2*j), glm::vec3(0,1,0));
        glUniformMatrix4fv(0, 1, GL_FALSE, &MVP[0][0]);
        puj.render();
      }
    }
  }
  void initScreen(){
    screenSaver();

    renderBanner(0,0,width,height,bg_tex);

    for(auto &btn:window->ui.init_buttons){
      renderButton(btn);
    }
    for(auto &ban:window->ui.init_banner){
      renderBanner(ban.x1,ban.y1,ban.x2,ban.y2,banner_tex);
      RenderText(ban.name,ban.x1,ban.y1,ban.x2,ban.y2);
    }
  }

  void startScreen(){
    screenSaver();
    renderBanner(0,0,width,height,bg_tex);
    for(auto &btn:window->ui.start_buttons){renderButton(btn);
    }
    for(auto &ban:window->ui.start_banner){
      renderBanner(ban.x1,ban.y1,ban.x2,ban.y2,banner_tex);
      RenderText(ban.name,ban.x1,ban.y1,ban.x2,ban.y2);
    }
  }

  float zoom;
  float right,top;
  void scrollWheel(int x){
    zoom+=-25*x;
    zoom=clamp(zoom,-900,500);
  }
  void gameScreen(){
    int x,y;
    SDL_GetMouseState( &x, &y );
    if(x<10)
      right-=10;
    if(x>width-10)
      right+=10;
    if(y<10)
      top+=10;
    if(y>height-10)
      top-=10;
    eye_pos.x=(1000+zoom)+right-top;
    eye_pos.z=(1000+zoom)-right-top;
    eye_pos.y=1000+zoom;
    look_at.x=right-top;
    look_at.z=-right-top;
    right=clamp(right,-1000,1000);
    top=clamp(top,-1000,1000);
    calcVP();

    auto &objs=GameEngine::_()->original.v;
    renderMap(GameEngine::_()->map);
    for(auto &obj:objs){
      if(obj->head.type==HOOK_OBJ){
        Hook *hook=(Hook*)obj;
        Pudge *pudge=hook->pudge;
        renderPudge(pudge,hook);
      }
    }
    for(auto &btn:window->ui.game_buttons){
      renderButton(btn);
    }
    for(auto &ban:window->ui.game_banner){
      renderBanner(ban.x1,ban.y1,ban.x2,ban.y2,banner_tex);
      RenderText(ban.name,ban.x1,ban.y1,ban.x2,ban.y2);
    }

  }
  void pauseScreen(){
    gameScreen();
    renderBanner(0,0,width,height,bg_tex);
    for(auto &btn:window->ui.pause_buttons){
      renderButton(btn);
    }
    for(auto &ban:window->ui.pause_banner){
      renderBanner(ban.x1,ban.y1,ban.x2,ban.y2,banner_tex);
      RenderText(ban.name,ban.x1,ban.y1,ban.x2,ban.y2);
    }
  }
  void disconnectedScreen(){
    screenSaver();
    renderBanner(0,0,width,height,bg_tex);
    for(auto &btn:window->ui.dc_buttons){renderButton(btn);
    }
    for(auto &ban:window->ui.dc_banner){
      renderBanner(ban.x1,ban.y1,ban.x2,ban.y2,banner_tex);
      RenderText(ban.name,ban.x1,ban.y1,ban.x2,ban.y2);
    }
  }
  void endScreen(){
    screenSaver();
    renderBanner(0,0,width,height,bg_tex);
    for(auto &btn:window->ui.exit_buttons){renderButton(btn);
    }
    for(auto &ban:window->ui.exit_banner){
      renderBanner(ban.x1,ban.y1,ban.x2,ban.y2,banner_tex);
      RenderText(ban.name,ban.x1,ban.y1,ban.x2,ban.y2);
    }
  }

  void renderMap(Map m){

    mat4x4 Model =glm::translate(mat4(1.f),vec3(0,0,0));//*glm::scale(mat4(1), vec3((m.xmax-m.xmin)/2.0,1,(m.ymax-m.ymin)/2.0));
    mat4x4 MVP = matVP*Model;
    gamemap.preRender();



    glUniform3f(glGetUniformLocation(gamemap.gProgramID,"gPointLights[0].color"),1,1,1);
    glUniform3f(glGetUniformLocation(gamemap.gProgramID,"gPointLights[0].pos"),-10,500,-10);
    glUniform1f(glGetUniformLocation(gamemap.gProgramID,"gPointLights[0].ambient"),0.1);
    glUniform1f(glGetUniformLocation(gamemap.gProgramID,"gPointLights[0].diffuse"),1);
    glUniform1f(glGetUniformLocation(gamemap.gProgramID,"gPointLights[0].attn_const"),0.001);
    glUniform1f(glGetUniformLocation(gamemap.gProgramID,"gPointLights[0].attn_linear"),0.001);
    glUniform1f(glGetUniformLocation(gamemap.gProgramID,"gPointLights[0].attn_exp"),0);


    glUniform3fv(glGetUniformLocation(gamemap.gProgramID,"gMatSpecPower"),1,value_ptr(eye_pos));

    glUniform1i(glGetUniformLocation(gamemap.gProgramID,"gNumPointLights"), 1);
    glUniform1f(glGetUniformLocation(gamemap.gProgramID,"gMatSpecIntensity"), 1);
    glUniform1f(glGetUniformLocation(gamemap.gProgramID,"gMatSpecPower"), 100);

    glUniformMatrix4fv(glGetUniformLocation(gamemap.gProgramID,"gMVP"), 1, GL_FALSE, &MVP[0][0]);
    glUniformMatrix4fv(glGetUniformLocation(gamemap.gProgramID,"gModel"), 1, GL_FALSE, &Model[0][0]);


    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, map_tex);
    gamemap.render();

    sky.render(matVP*glm::translate(mat4(1),vec3(0,0,0))*glm::scale(mat4(1),vec3(4000,4000,4000)));
  }
  void renderPudge(Pudge *p,Hook *h){
    double animTime=0;
    string anim="idle";
    vec3 pos;
    pos.x=p->head.cir.c.x;
    pos.y=-60;
    pos.z=p->head.cir.c.y;
    vec3 hook_pos;bool force_hook=false;float hook_angle=0;
    vector3 hookdir,orientation;
    if(h->head.is_active){
      force_hook=true;
      hookdir=h->head.cir.c-p->head.cir.c;
      float dist=hookdir.mag();
      orientation=p->head.orientation;
      if(dist>100){
        orientation.normalize();
        hookdir=hookdir/dist;

        hook_angle=acos(orientation*hookdir);
        if((orientation^hookdir).z<0)
          hook_angle*=-1;
        hook_pos.x=-(orientation^hookdir).z*dist;
        hook_pos.z=170;
        hook_pos.y=-(orientation*hookdir)*dist;
      }
    }
    if(p->head.is_killed){
      float t=(p->head.birth_time-p->head.dead_since);
      if(t<3000){
        anim="stun";
        animTime=t;
        pos.y-=t/1000.0*70;
      }else
        return;
    }else if(p->head.is_hooked){
      animTime=p->head.birth_time-p->head.hooked_since;
      anim="stun";
    }else if(p->head.move_start_time>=0){//walking
      animTime=p->head.birth_time-p->head.move_start_time;
      anim="walkN";
    }else if(h->head.is_active){
      if(h->head.hook_end_since>=0)
      {
        animTime=h->head.birth_time-p->head.hook_end_since;
        anim="meathook_end";
        force_hook=false;
      }else{
        if(fabs(hook_angle)>0.1){
          animTime=p->head.birth_time;
          anim="idle";
        }else{
          animTime=h->head.birth_time-h->head.hook_start_time;
          anim="meathook_start";
        }
      }
    }else{
      animTime=p->head.birth_time;//-p->head.idle_since;
      anim="idle";
    }
    puj.hook(force_hook,hook_pos,hook_angle);
    puj.setCurAnim(("res/pudge_"+anim+".smd").c_str());
    puj.calcBoneTransform(animTime/1000);
    puj.hook(false,vec3(0,0,0));
    puj.fillBoneBuffer();
    puj.preRender();


    vec3 base=vec3(-1,0,0);
    vec3 dir=glm::normalize(vec3(p->head.orientation.x,0,p->head.orientation.y));
    float dot=glm::dot(dir,base);
    float angle=acos(dot);
    if(glm::cross(dir,base).y>0)
      angle*=-1;
    glm::mat4x4 MVP = matVP*glm::translate(mat4(1.f),pos)*glm::rotate(angle, glm::vec3(0,1,0));
    glUniformMatrix4fv(0, 1, GL_FALSE, &MVP[0][0]);
    puj.render();

    renderHealth(pos.x,pos.y+250,pos.z,p->head.health*100.0/PUDGE_HP,(p->head.team==1)+(p->head.rot_active?2:0));
  }
};
GUI* GUI::singleton=NULL;
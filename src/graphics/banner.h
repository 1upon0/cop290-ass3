#pragma once
#include <string>
#include <functional>
using namespace std;

class Banner
{
public:
	int x1,y1,x2,y2;	//Bounding rectangle
	string name;	//Bar name - can be health, cool_down...
	Banner(){
		name="";
	}
	void init(string _name,int _x1,int _y1,int _x2,int _y2){
		x1=_x1,y1=_y1,x2=_x2,y2=_y2;
		name = _name;
	}
	void draw(){
		//TODO - Draw
	}
};
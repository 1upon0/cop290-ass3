#pragma once
#include <iostream>
#include <vector>
#include <cstring>
#include <cstdlib>
#include "node2.h"
//#include "timer.h"
#include "command.h" 
// #include "gameEngine.h"
#include "log.h"
#define TN 150
bool fakka = true;
string ip;
class NetworkPlayer{
public:
	//uint32_t id;
	bool ingame,isDC,checkcc,isRC,retry,get_state;
	PeerData my_data;
	int n_peers;
	Network network_player;
	Timer network_timer;
	vector<Command> my_commands;
	vector<Command> peer_commands;
	vector<int> check;
	uint32_t peer_to_try;
	ENetPeer *recon_peer;	// the peer i am trying to reconnect to
	//state=1 : active
	//state=0 : dead
	//pair<int,int>* check;
	NetworkPlayer();
	void init(bool,string,uint16_t);
	void start();
	bool isHost;
	uint32_t maxID;// relevant only if isHost is true
	bool checkID(uint32_t id);
	void sendToPeer(Message *message,ENetPeer* peer);
	void sendToAll(Message *message);
	void sendCommandToAll();
	int processEvent();
	bool checkArray();
	void sync();
	void pushCommand(Command);
	void reconnect();
	int eventConnect(ENetEvent &event);
	int eventReceive(ENetEvent &event);
	int eventDisconnected(ENetEvent &event);
	void updateNewPeer(ENetPeer* peer,uint32_t id);
		int connectedPeers(){
		//assert(node != 0 && "size of peers of null node");
		int cnt = 0;
		for(auto& peer:network_player.peers){
			if(peer.second.state == 2)cnt++;
		}
		return cnt;
	}
	int totalPeers(){
		return network_player.peers.size();
	}
	void checkConnected();
};
NetworkPlayer::NetworkPlayer(){
	my_commands.resize(0);
	peer_commands.resize(0);
	check.resize(0);
	//check = NULL;
	ingame = false;
	maxID = 1;
	isDC = false;
	checkcc = false;
	n_peers =0;
	isRC = false;
	retry = false;
	peer_to_try = 0;
	get_state = false;
}
bool NetworkPlayer::checkID(uint32_t id){
	for(auto& peer:network_player.peers){
		PeerData pdata = peer.second;
		if(pdata.id == id){
			return false;
		}
	}
	return true;
}
void NetworkPlayer::updateNewPeer(ENetPeer* peer,uint32_t id){
	AddressMessage AM;
	vector<pair<pair<uint32_t,uint16_t>,uint32_t> > peer_list;
	pair<uint32_t,uint16_t> temp;
	for(auto& peer1:network_player.peers){
		if(peer1.second.state == 0)continue;
		if(network_player.same_addr(peer1.first,peer->address)) continue;
		temp = make_pair(peer1.first.host,peer1.first.port);
		if(temp.first == 16777343)temp.first = inet_addr(ip.c_str());
		peer_list.push_back(make_pair(temp,peer1.second.id));
	}
	AM.init(peer_list,id);
	sendToPeer(&AM,peer);
}
void NetworkPlayer::pushCommand(Command cmd){
	my_commands.push_back(cmd);
	//cout<<my_commands.size()<<endl;
}
void NetworkPlayer::sendToPeer(Message *message,ENetPeer *peer){
	int idx=network_player.find(peer->address);
	assert(idx!=-1 && "peer not present");
	if(network_player.peers[idx].second.state == 0){cout<<"peer not active, not sending message"; return;}
	vector<char> buff;
	message->loadTo(buff);
	network_player.sendToPeer(buff,peer);
}
void NetworkPlayer::sendToAll(Message *message){
	vector<char> buff;
	message->loadTo(buff);
	network_player.sendToAll(buff);
}
bool NetworkPlayer::checkArray(){
	int flag=1;
	//Checks the messages of the active peer
	for(int i=0;i<network_player.peers.size();i++){
		if(network_player.peers[i].second.state==0)continue; //ignore dc'ed peers
		if(check.at(network_player.peers[i].second.id) == 0){
		//	cout<<"culprit"<<int(i)<<endl;
			flag=0;
			break;
		}
		//cout<<int(network_player.peers[i].second.id)<<endl;
	}
	return flag;
}
void NetworkPlayer::sync(){
	
	if(checkArray() || network_timer.isTimeout()){
		if(network_timer.isTimeout())cout<<"timeout sending my commmands"<<endl;
		sendCommandToAll();
		my_commands.clear(); 	
		//TODO - send the peer_commands to the game-engine
		//	cout<<"peer cmds"<<peer_commands.size()<<endl;
		//GameEngine::_()->applyCommand(peer_commands);
		for(auto& cmd:peer_commands){
			cout<<int(cmd.id)<<":"<<cmd.type<<endl;
			// if(cmd.id == 4)
				// cout<<"got it"<<endl;
		}
	//	cout<<"-------------------------------------------------------"<<endl;
		peer_commands.clear();
		fill(check.begin(),check.end(),0);
		fakka = false;
		network_timer.start(TN);
	}
}
void NetworkPlayer::sendCommandToAll(){
	//Loading the command to buffer
	//cout<<"s"<<endl;
	CmdMessage cm;
	cm.init(my_commands,my_data.id);
	//cout<<cm.commands.size()<<"aaaaaaa"<<my_commands.size()<<endl;
	sendToAll(&cm);
}

void NetworkPlayer::init(bool _isHost,string host_ip,uint16_t host_port){
	network_player.init();
	network_player.createNode();
	isHost = _isHost;
	my_data.state = 1;
	if(!isHost){
		network_player.connectToHost(host_ip,host_port);
	}
	else my_data.id = 1;
	//now you should start processing events

}
int NetworkPlayer::processEvent(){
	ENetEvent event;
	int rv = 0;
	if(enet_host_service(network_player.node->self, &event, 0)>0){
		//if(isDC) return 0;
		switch(event.type){
		case ENET_EVENT_TYPE_CONNECT:
			rv = eventConnect(event);
			break;
		case ENET_EVENT_TYPE_RECEIVE:
			rv = eventReceive(event);
			enet_packet_destroy(event.packet);
			break;
		case ENET_EVENT_TYPE_DISCONNECT:
			rv = eventDisconnected(event);
			break;
		default:cout<<"???"<<endl;
			break;
		}
		assert(rv>=0 && "Error in event handling!!");
	}
	return rv;
}
void NetworkPlayer::start(){
	//GameEngine::_()->init(connectedPeers()+1);
	
	//network_timer.start(tn);
	cout<<"started"<<endl;
	check.resize(totalPeers()+2);
	for(int i=0;i<totalPeers();i++){
		check.at(i) = 1;
	}

	// send a start game message to all peers
	if(isHost){
		Message m;
		m.head.id = my_data.id;
		m.head.r1 = 69;
		sendToAll(&m);
	}
	network_timer.start(TN);
}	

int NetworkPlayer::eventConnect(ENetEvent &event){
	cout<<"cn"<<endl;
	int idx = network_player.find(event.peer->address);
	if(idx != -1){
		//old peer
		//cout<<idx<<" "<<network_player.peers[idx].second.state<<endl;
		assert(network_player.peers[idx].second.state==0 && "Peer already connected,unexpected behaviour!");
	}
	if(event.data == 0){
		//echo
		assert(idx != -1 && "echo, should be already in the list");
		network_player.peers[idx].second.state = 2;
		cout<<"Connection to :"<<network_player.peers[idx].first.host<<" " <<event.peer->address.host<<" has completed"<<endl;
		if(checkcc){
			checkConnected();
		}
		if(isRC){
			isRC = false;
			isDC = false;
		}
	}
	else{
		//a connection request
		if(idx == -1){// new guy
			// if i am the host, i dont expect any id in the event data, rather i have to assign an id
			cout<<"New guy"<<endl;
			if(isHost){
				cout<<"giving him an id: "<<maxID+1<<endl;
				PeerData new_peer;
				new_peer.id = maxID+1;maxID++;
				new_peer.state = 1;
				network_player.peers.push_back(make_pair(event.peer->address,new_peer));
				//send message telling the new peer about his id
				updateNewPeer(event.peer,new_peer.id);

				cout<<"sent the list"<<endl;
			}
			else{
				assert(checkID(event.data) && event.data >0 && "invalid id");		//id should not have been assigned to a peer
				PeerData new_peer;
				new_peer.id = event.data;
				new_peer.state = 1;
				network_player.peers.push_back(make_pair(event.peer->address,new_peer));
				//cout<<connectedPeers()<<" "<<check.size()<<endl;
				cout<<"told me his id is :"<<event.data<<endl;
			}

		}	
		else{
			network_player.peers[idx].second.state = 1;
			cout<<network_player.peers[idx].second.id<<" asked to reconnect"<<endl;
			if(event.data == 100){
				
				//aww i am the first one he reconnected with
			
				//give him the list of all the peers to connect to
				updateNewPeer(event.peer,network_player.peers[idx].second.id);
				cout<<"sent the list"<<endl;
			}	
			cout<<"Connected to :"<<network_player.peers[idx].first.port<<"again :))"<<endl;
		}
		
	}
	enet_peer_timeout(event.peer,32,500,1000);
	enet_peer_ping_interval(event.peer,50);
	return 0;
	//cout<<"connected to "<<event.peer.address.port<<endl;
}
int NetworkPlayer::eventReceive(ENetEvent &event){
	//TODO
	//cout<<"rec"<<endl;
	vector<char> buff((char*)event.packet->data,(char*)event.packet->data+event.packet->dataLength);
	Message msg;
	msg.loadFrom(buff);

	switch(msg.head.type){
		case CMD:{
			//cout<<"command"<<endl;
			//cout<<"rec cmd"<<endl;
			//cout<<"r";
			CmdMessage cm;
			//cout<<"loading to buff"<<endl;
			cm.loadFrom(buff);
			for(auto& command:cm.commands){
				peer_commands.push_back(command);
			}
			//assert(m.head.id<=connectedPeers() && "Wrong id in the message");
			//cout<<int(cm.head.id)<<" "<<check.size()<<endl;
			check[cm.head.id]=1;

			break;
		}
		case ADDRESS:{
				cout<<"address"<<endl;
				AddressMessage m;
				m.loadFrom(buff);
				cout<<"received a list of peers to connect to"<<endl;
				cout<<"total "<<m.v.size()<<" peers to connect to"<<endl;
				n_peers = m.v.size() +1;
				checkcc = true;
				if(checkcc){
					checkConnected();
				}
				ENetAddress address;
				for(int i=0;i<m.v.size();i++){
					address.host = m.v[i].first.first;
					address.port = m.v[i].first.second;
					network_player.connectToPeer(address,m.head.id,m.v[i].second);
				}
				my_data.id =  m.head.id;
				break;
		}
		case SIMPLE:{
				
				if(msg.head.r1 ==69){
					cout<<"received start message"<<endl;
					start();
				}
				if(msg.head.r1 == 88){
					cout<<"someone completed their connection"<<endl;
					int idx = network_player.find(event.peer->address);
					assert(idx != -1 && "invalid complete connection msg");
					network_player.peers[idx].second.state = 2;

				}
				if(msg.head.r1 == 99){
					//send the game state
					//TODO replace this with an actual Game State message
					// DataMessage m;
					// GameEngine::_()->original.loadTo(m.v);
					// m.head.id = my_data.id;
					// m.head.r1 = 111;



					// Message m;
					// m.head.id = my_data.id;
					// m.head.r1 = 111;
					
					sendToPeer(&m,event.peer);
				}
				if(msg.head.r1 == 111){
					//now my game can be resumed again
					// TODO replace my game state with the one i received

					// DataMessage m;
					// m.loadFrom(buff);
					// GameEngine::_()->copy.loadFrom(m.v);


					cout<<"received game state... resuming my game"<<endl;
					//clear all the vectors
					peer_commands.clear();
					my_commands.clear();
					//isDC = false;
					ingame = true;
				}
				break;
		}
	}

	return 0;
}
int NetworkPlayer::eventDisconnected(ENetEvent &event){
	
	int idx = network_player.find(event.peer->address);
	assert(idx != -1 && "dc form a peer not in the list");
	network_player.peers[idx].second.state = 0;
	//enet_peer_reset(event.peer);
	cout<<"dced from"<<" "<<network_player.peers[idx].second.id<<endl;
	// if i am in reconnection mode, ignore any dc events
	if(isRC)return 0;
	if(connectedPeers() == 0){
		cout<<"disconnected from everyone... dcing myself"<<endl;
		isDC = true;
		ingame=false;
		isHost = false;

		peer_to_try = 0;
		get_state = true;
	}
	
	//exit(1);
}
void NetworkPlayer::reconnect(){
	if(peer_to_try >= totalPeers()){
		logInfo("out of peers to try to connect to");
		isRC = false;
		return;
	}

	cout<<"Now i will try to reconnect :)"<<endl;
	
	ENetAddress addr=network_player.peers[peer_to_try].first;
	cout<<"[Peer]Sending reconnection request to peer:";
	recon_peer = enet_host_connect(network_player.node->self,&addr,2,100);
	assert(recon_peer != NULL && "Could not connect to host!");
	cout<<recon_peer->address.port<<endl;

	enet_peer_timeout(recon_peer,32,300,500);
	enet_peer_ping_interval(recon_peer,50);


}
void NetworkPlayer::checkConnected(){

	if(n_peers == connectedPeers()){
		cout<<"completed"<<endl;
		Message m;
		m.head.id = my_data.id;
		m.head.r1 = 88;
		sendToAll(&m);
		checkcc = false;
		if(get_state){
			//ask any peer for the game state
			for(auto& peer:network_player.peers){
				if(peer.second.state == 2){
					Message m;
					m.head.id = my_data.id;
					m.head.r1 = 99;
					ENetAddress addr = peer.first;
					for(int i=0;i<network_player.node->self->connectedPeers;i++){
						ENetPeer *epeer = network_player.node->self->peers+i;
						if(network_player.same_addr(epeer->address,addr))
						sendToPeer(&m,epeer);
						break;
					}
					break;
				}
			}
		}
	}
}
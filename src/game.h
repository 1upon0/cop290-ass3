/**
 * Naming Conventions:
 *  - PascalCase for class names
 *  - camelCase for methods
 *  - snake_case,fokcase for variables
 * Passing buffers around:
 *  - use std::vector<char*> and not char*
 */
#pragma once
#include "command.h"
#include "physics.h"
#include "gameObject.h"
#include "timer.h"

/**
 * @brief Game state class
 * @details Stores the pointer to the game objects thereby storing the state of the game. This game state given to the player on reconnect.
 * 
 */
class GameState{
public:
	/**
	 * @brief Header
	 * @details Contains last synced time stamp which is later used in the virtual simuation
	 */
	struct Head{
		int64_t last_synced;
		uint32_t n;
	} head;
	vector<GameObj*> v;


	GameState():v(0){
		head.last_synced=Timer::now();
		head.n=0;
		v.clear();
	}

	/**
	 * @brief Copy constructor
	 * @details Called when object is passed as argument, returned as value and copy initialization
	 * 
	 * @param g [description]
	 */
	GameState(const GameState& g){
		int i=0;
		for(auto obj : g.v){
			v.push_back(obj->clone());
			//Assuming the P-H-P-H- pattern
			if(i%2){((Hook*)v[i])->pudge = (Pudge*)v[i-1];}
			i++;
		}
		i=0;
		for(auto obj : g.v){
			if(i%2)continue;
			//only pudges are considered
			if(v[i]->head.hook_id !=0){
				v[i]->head.hook = v.at(2*v[i]->head.hook_id - 2);
			}
			if(v[i]->head.hooker_id != 0){
				v[i]->head.hooker = v.at(2*v[i]->head.hooker_id - 2);
			}
			i++;
		}
		head = g.head;
	}

	/**
	 * @brief Assignment operator overloaded
	 * @details Called when already initialized object is copied. And not during the pass by value or return by value. This is also not called in case of copy initialization.
	 * 
	 * @param g [description]
	 */
	GameState& operator=(const GameState& g){
		assert((head.n == g.head.n) && "#elements should have been same");
		for(int i=0;i<head.n;i++){
			*v[i] = *(g.v[i]);
		}
		head = g.head;
	}

	/**
	 * @brief Virtual destructor
	 * @details Because the class contains virtual functions it is necessary to provide virtual destructor.
	 */
	~GameState(){
		v.clear();
		if(this)
			delete this;
	}

	/**
	 * @brief Loading from the vector<char>
	 * @details Loads the object from the buffer. This buffer is received over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	void loadFrom(vector<char>& buff){
		cout<<"I am in the load From"<<endl;
		assert(buff.size() >= sizeof(Head) && "GameObj");
		head = *(Head*)(buff.data());
		uint32_t offset=sizeof(Head);
		v.resize(head.n);
		for(int i=0;i<head.n/2;i++){
			Pudge* p =new Pudge;
			Hook* h = new Hook;
			assert(buff.size() >= offset + sizeof(Head)+sizeof(Head) && "GameObj");
			vector<char> tmp(buff.begin()+offset,buff.end());
			p->loadFrom(tmp);
			tmp = vector<char>(buff.begin()+offset+sizeof(p->head),buff.end());
			h->loadFrom(tmp);
			h->pudge = p;
			offset += sizeof(p->head)+sizeof(h->head);

			v[2*i] = p;
			v[2*i+1] = h;
		}
	}

	/**
	 * @brief Load to buffer
	 * @details Loads the object into the buffer. This buffer is then passed over the network
	 * 
	 * @param buff vector<char> buffer
	 */
	void loadTo(vector<char>& buff){
		//Assuming the pattern of P-H-P-H in the GameState
		//Has to be taken care at the creation time & sending time
		uint32_t offset=0;
		buff.resize(sizeof(Head));
		memcpy(buff.data(),(char*)&head,sizeof(Head));
		offset += sizeof(Head);
		vector<char> tmp;
		for(int i=0;i<head.n;i++){
			v[i]->loadTo(tmp);
			buff.insert(buff.end(),tmp.begin(),tmp.end());
			offset += tmp.size();
			tmp.clear();
		}
	}
};
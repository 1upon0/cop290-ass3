#pragma once
#include <cstdio>
#include <cmath>
// #include "log.h"
#define EPS 1e-6
/**
 * @brief Vector algebra helper class
 * @details Contains function to perform vector arithmetic operations like addition, subraction, dot product, cross product.
 * 
 */
class vector3{
public:
	union{
		struct{
			float x,y,z;};
		float v[3];
	};
	/**
	 * @brief Constructor of vector3 class
	 * @details Created null vector3  by default.
	 * 
	 * @param xx x co-ordinate
	 * @param yy y co-ordinate
	 * @param zz z co-ordinate
	 */
	vector3(float xx=0,float yy=0,float zz=0){ x=xx; y=yy;z = zz;}

	operator const float*() const{
		return v;
	}

	/**
	 * @brief Vector addition
	 * @return Vector with value equal given vector + input vector
	 */
	vector3 operator+(const vector3 &v)const{
		return vector3(x+v.x,y+v.y,z+v.z);
	}
	/**
	 * @brief Vector Subtraction
	 * @return Vector with value equal given vector - input vector
	 */
	vector3 operator-(const vector3 &v)const{
		return vector3(x-v.x,y-v.y,z-v.z);
	}
	/**
	 * @brief Dot product
	 */
	vector3 operator*(double k)const{
		return vector3(x*k,y*k,z*k);
	}
	/**
	 * @brief Scalar division
	 * @param k Non zero value of type double. If it is zero then does nothing
	 */
	vector3 operator/(double k)const{
		if(k==0)
			throw "divide vector3 by zero";
		return vector3(x/k,y/k,z/k);
	}
	/**
	 * @brief vector multiplication
	 */
	float operator*(const vector3 &v)const{
		return (x*v.x+y*v.y+z*v.z);
	}
	/**
	 * @brief Cross product
	 * @details Returs the cross product of the given vector with input vector
	 * 
	 * @param v Input vector
	 * @return Cross product of given vector and input vector
	 */
	vector3 operator^(const vector3 &v)const{
		vector3 res;
		res.x =  y*v.z - z*v.y;
		res.y =  z*v.x - x*v.z;
		res.z =  x*v.y - y*v.x;
		return res;
	}
	/**
	 * @brief Addition of a vector to given vector
	 * @param  Input vector
	 */
	vector3 operator+=(const vector3&v){
		x+=v.x;
		y+=v.y;
		z+=v.z;
		return *this;
	}
	/**
	 * @brief Subtract a vector from given vector
	 * @param v Input vector
	 */
	vector3 operator-=(const vector3 &v){
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return *this;
	}
	/**
	 * @brief Dot product with vector
	 * @param v Vector with which dot product need to be evaluated
	 */
	vector3 operator*=(const vector3  &v){
		x *= v.x;
		y *= v.y;
		z *= v.z;
		return *this;
	}
	/**
	 * @brief Scales up the vector
	 * @param k Magnitude by which the vector has to be scaled up.
	 */
	vector3 operator*=(double k){
		x *= k;
		y *= k;
		z *= k;
		return *this;
	}
	/**
	 * @brief Scales down the vector
	 * @param k Magnitude by which the vector has to be scaled down.
	 */
	vector3 operator/=(double k){
		if(k == 0)
			throw "Divide vector3 by zero";
		x /= k;
		y /= k;
		z /= k;
		return *this;
	}
	/**
	 * @brief Square of magnitude of vector
	 * @details Calculates the square of magnitude of vector. Basically it returns dot product with itself.
	 * @return Returns magnitude square
	 */
	float magsq(){
		return (x*x+y*y+z*z);
	}
	/**
	 * @brief magnitude of the vector
	 * @details Calculate the magnitude of the vector
	 * @return Returns the magnitude of the vector
	 */
	float mag(){
		return sqrt(magsq());
	}
	/**
	 * @brief Normalizes the vector
	 * @details Divides the given vector by its magnitude (Normalization). If Magnitude of the vector is zero then does nothing
	 */
	void normalize(){
		float magg = mag();
		if(magg == 0)return;
		x/=magg;
		y/=magg;
		z/=magg;
	}
	/**
	 * @brief Returns unit vector
	 * @details Returns the unit vector in the direction of the given vector
	 * @return unit vector in the direction of the given vector
	 */
	vector3 normalized(){
		vector3 v(x,y,z);
		v.normalize();
		return v;
	}
	/**
	 * @brief Prints vector
	 * @details Prints the co-ordinates of the vector
	 */
	void print(){
		printf("Vector is : <%f,%f,%.2f> \n",x,y,z);
	}
};
/**
 * @brief Scalar multiplication
 * @details Return the scalar times given vector. It does not modify the given vector
 */
inline vector3 operator*(float t,vector3 &v){
	return v*t;
}

#pragma once
#include "boilerplate.h"
#include <fstream>
#include <sstream>


class SkeletalObj{
  public:
  struct Vert{
    vec3 pos;
    vec2 tex;
    vec3 norm;
    GLuint mesh_id;
    GLuint bones[8];
    GLfloat weights[8];
    Vert(){
      mesh_id=0;
      memset(bones,0,8*sizeof(uint));
      memset(weights,0,8*sizeof(float));
    }
  };
  struct Face{
    GLuint indexes[3];
  };
  struct Bone{
    GLuint id,fakeid;
    string name;
    glm::mat4x4 base_mat,anim_mat,mat;
    vector<uint> children;
    int parent;
    Bone(){
      fakeid=0;
      parent=-2;
    }
  };

  vector<Vert> vert_buff;
  vector<Face> face_buff;
  vector<Bone> bones;
  vector<GLuint> textures;
  map<string,uint> bone_map;
  map<uint,mat4x4> mesh_transform;
   GLObject obj;
  Assimp::Importer aiImporter;

    vector<string> mesh_names;
  SkeletalObj(){
    hook_angle=0;
    force_hook=false;
  }
  void traverseNodes(aiNode *n,int depth=0,mat4x4 base=glm::mat4(1.f)){
    // if(n->mNumMeshes==0)
    //   logInfo("Found meshless node %s",n->mName.C_Str());
    // else
    //   logInfo("Found node %s with %d meshes",n->mName.C_Str(),n->mNumMeshes);
    mat4x4 new_base=base*convertToGLM(n->mTransformation);
    for(size_t i=0;i<n->mNumMeshes;i++){
      if(mesh_names.size()<=n->mMeshes[i])
        mesh_names.resize(n->mMeshes[i]+1);
      mesh_names[n->mMeshes[i]]=n->mName.C_Str();
      if(mesh_transform.find(n->mMeshes[i]) == mesh_transform.end())
        mesh_transform.insert(make_pair(n->mMeshes[i],new_base));
    }
    for(size_t i=0;i<n->mNumChildren;i++){
      traverseNodes(n->mChildren[i],depth+1,new_base);
    }
  }
  void load(const char* file)
  {
    auto scene = aiImporter.ReadFile(file,aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_FlipUVs  );
    assert(scene && "load mesh");
    traverseNodes(scene->mRootNode);
    vector<int> MeshTexSerials;
    for(size_t i=0;i<scene->mNumMeshes;i++){

      mat4x4 base=glm::mat4(1.f);
      auto id_transform=mesh_transform.find(i);
      if(id_transform!=mesh_transform.end()){
        base=id_transform->second;
      }
      auto &mesh=scene->mMeshes[i];
      assert(mesh);

      MeshTexSerials.push_back(i);

      GLuint starting_id=vert_buff.size();
      for(size_t j=0;j<mesh->mNumVertices;j++){
        Vert vert;
        vert.mesh_id=i;
        vert.pos=convertToGLM(mesh->mVertices[j]);
        vert.pos=glm::vec3(base*glm::vec4(vert.pos,1));
        // logInfo("%f %f %f\n",vert.pos.x,vert.pos.y,vert.pos.z);
        if(mesh->HasNormals())
          vert.norm=convertToGLM(mesh->mNormals[j]);
        if(mesh->mTextureCoords[0]){
          vert.tex=vec2(convertToGLM(mesh->mTextureCoords[0][j]));
          // vert.tex.x=vert.tex.x;
          // vert.tex.y=1-vert.tex.y;
        }
        vert_buff.push_back(vert);
      }
      for(size_t j=0;j<mesh->mNumFaces;j++){
        auto &face=mesh->mFaces[j];
        if(face.mNumIndices!=3)
          continue;
          // logError("All faces should be triangles!");
        face_buff.push_back({{face.mIndices[0]+starting_id,
          face.mIndices[1]+starting_id,
          face.mIndices[2]+starting_id}});
      }
      for(size_t j=0;j<mesh->mNumBones;j++){
        auto &ai_bone=mesh->mBones[j];
        auto it=bone_map.find(string(ai_bone->mName.C_Str()));
        Bone b;
        if(it==bone_map.end())
        {
          b.mat=b.base_mat=convertToGLM(ai_bone->mOffsetMatrix);
          b.name=ai_bone->mName.C_Str();
          b.id=bones.size();
          bones.push_back(b);
          bone_map.insert(make_pair(b.name,b.id));
        }else
          b=bones[it->second];
        for(size_t k=0;k<ai_bone->mNumWeights;k++){
          auto vid=ai_bone->mWeights[k].mVertexId+starting_id;
          bool found=false;
          for(int l=0;l<8;l++)
          {
            if(vert_buff[vid].weights[l]==0){
              vert_buff[vid].weights[l]=ai_bone->mWeights[k].mWeight;
              vert_buff[vid].bones[l]=b.id;
              found=true;
              break;
            }
          }
          if(!found)
            logError("More than 8 bones for a vertex!");
        }
      }
    }
    obj.loadShaders("shaders/skeletal-vert.glsl","shaders/skeletal-frag.glsl");
    obj.addAttrib("Position",offsetof(Vert,pos),3,GL_FLOAT);
    obj.addAttrib("TexCoord",offsetof(Vert,tex),2,GL_FLOAT);
    obj.addAttrib("Normal",offsetof(Vert,norm),3,GL_FLOAT);
    obj.addAttrib("MeshID",offsetof(Vert,mesh_id),1,GL_UNSIGNED_INT);
    obj.addAttrib("BoneIDs",offsetof(Vert,bones),4,GL_UNSIGNED_INT);
    obj.addAttrib("BoneIDs2",offsetof(Vert,bones)+4*sizeof(GLuint),4,GL_UNSIGNED_INT);
    obj.addAttrib("Weights",offsetof(Vert,weights),4,GL_FLOAT);
    obj.addAttrib("Weights2",offsetof(Vert,weights)+4*sizeof(GLfloat),4,GL_FLOAT);
    obj.bindBuffers(vert_buff.data(),sizeof(Vert),vert_buff.size(),face_buff.data(),sizeof(GLuint),face_buff.size()*3);




    assert(GL_MAX_TEXTURE_UNITS>=scene->mNumMeshes);

    glUseProgram(obj.gProgramID);
    glUniform1iv(glGetUniformLocation(obj.gProgramID, "gColorMaps"),scene->mNumMeshes,MeshTexSerials.data());

    for(size_t i=0;i<MeshTexSerials.size();i++){
      textures.push_back(GetRenderer()->loadTex(string("res/material/")+mesh_names[i]+".tga"));
    }
    glUseProgram(0);
    auto res=bone_map.find(string("root"));
    assert(res!=bone_map.end());
    root_bone=&bones[res->second];

    readAnim("res/pudge_meathook_start.smd");
    readAnim("res/pudge_meathook_end.smd");
    readAnim("res/pudge_walkN.smd");
    readAnim("res/pudge_idle.smd");
    readAnim("res/pudge_stun.smd");
  }
  void preRender(){
    obj.preRender();
    for(size_t i=0;i<textures.size();i++){
      glActiveTexture(GL_TEXTURE0 + i); // Texture unit 0
      glBindTexture(GL_TEXTURE_2D, textures[i]);
    }
  }
  void render(){
    obj.render();
    for(size_t i=0;i<textures.size();i++){
      glActiveTexture(GL_TEXTURE0 + i); // Texture unit 0
      glBindTexture(GL_TEXTURE_2D,0);
    }
  }

struct BoneAnim{
    GLuint id;//bone id as in bone_map
    bool ignore;
    vector<glm::vec3> translations;
    vector<glm::quat> rotations;
    BoneAnim(){ignore=false;}
  };
  struct Anim{
    string name;
    vector<BoneAnim> channels;
  };
  map<string,Anim> animations;
  Anim *curAnim;
  Bone *root_bone;
  bool force_hook;
  vec3 hook_pos;
  float hook_angle;
  void hook(bool force,vec3 pos,float angle){
    force_hook=force;
    hook_pos=pos;
    hook_angle=angle;
  }
  void calcBoneTransform(float t,Bone *n=NULL){
    uint hook=bone_map.find("weapon_rt_1")->second;
    if(n==NULL)
    {
      n=root_bone;
      float fps=30;
      int num_frames=curAnim->channels[0].translations.size();
      int low_frame=int(t*fps)%num_frames, high_frame=int(t*fps+1)%num_frames;
      float fraction=t*fps-int(t*fps);


      int cycle_frame=18;
      if(force_hook && t*fps>=cycle_frame && curAnim->name=="res/pudge_meathook_start.smd"){
        float tt=cycle_frame/fps+(t-cycle_frame/fps)/10;
        fraction=tt*fps-int(tt*fps);
        if(int(tt*fps-cycle_frame)%20<=9)
          low_frame=cycle_frame+int(tt*fps-cycle_frame)%10,high_frame=cycle_frame+1+int(tt*fps-cycle_frame)%10;
        else
          low_frame=cycle_frame+10-int(tt*fps-cycle_frame)%10,high_frame=cycle_frame+9-int(tt*fps-cycle_frame)%10;
      }
      for(auto &channel:curAnim->channels){
        if(channel.ignore)
          continue;
        auto q_final=glm::slerp(channel.rotations[low_frame],channel.rotations[high_frame],fraction);
        auto p_final=glm::mix(channel.translations[low_frame],channel.translations[high_frame],fraction);

        bones[channel.id].anim_mat=glm::translate(glm::mat4(1.f), p_final)*glm::mat4_cast(q_final);
        if(channel.id==hook && force_hook){
          if(low_frame>6 || curAnim->name!="res/pudge_meathook_start.smd"){
             bones[channel.id].anim_mat=glm::translate(glm::mat4(1.f), hook_pos)*glm::rotate(-hook_angle,vec3(0,0,1))*glm::rotate(3.1415f,vec3(1,0,0));
          }
        }
      }
    }
    if(n->parent>=0){
      if(n->id==hook && force_hook){
        // n->anim_mat=
      }else
      n->anim_mat=bones[n->parent].anim_mat*n->anim_mat;
    }
    n->mat=glm::inverse(root_bone->base_mat)*n->anim_mat*n->base_mat;
    for(auto &child:n->children)
      calcBoneTransform(t,&bones[child]);
  }
  void print(Bone *n=NULL,int depth=0){
    if(n==NULL)
    {
      auto res=bone_map.find(string("root"));
      assert(res!=bone_map.end());
      n=&bones[res->second];
    }
    for(int i=0;i<depth;i++)printf("  ");
    cout<<n->name<<" p:"<<n->parent<<" mesh:"<<n->id<<" smd:"<<n->fakeid<<endl;
    for(auto &child:n->children)
      print(&bones[child],depth+1);
  }
  vector<mat4x4> bone_buff;
  void fillBoneBuffer(){
    bone_buff.clear();
    for(auto &bone:bones)
      bone_buff.push_back(bone.mat);
    glUseProgram(obj.gProgramID);
    glUniformMatrix4fv(glGetUniformLocation(obj.gProgramID, "gBones"),bone_buff.size(),GL_FALSE,reinterpret_cast<GLfloat*>(bone_buff.data()));
  }
  void setCurAnim(const char *name){
    auto res=animations.find(string(name));
    if(res==animations.end()){
      logError("Couldn't activate animation %s. Is it loaded?",name);
      return;
    }
    curAnim=&(res->second);
  }
  void readAnim(const char *name){
    ifstream fs(name,ios::binary|ios::in);
    assert(fs.good());
    string tmp;
    istringstream line;

    getline(fs,tmp);line.str(tmp);
    line>>tmp;
    while(tmp=="//"||tmp=="version"){
      getline(fs,tmp);line.clear();line.str(tmp);
      line>>tmp;
    }
    auto &anim=animations.insert(make_pair(string(name),Anim())).first->second;
    anim.name=name;
    if(tmp == "nodes"){
      while(1)
      {
        getline(fs,tmp);line.clear();line.str(tmp);
        if(tmp[0]=='e')
          break;
        uint id;
        int parent;
        string sname;
        line>>id>>sname>>parent;
        sname=sname.substr(1,sname.length()-2);
        // logDebug("parent %d, %s, %d",id,sname.c_str(),parent);
        BoneAnim banim;
        auto res=bone_map.find(sname);
        if(res==bone_map.end())
        {
          banim.ignore=true;
          anim.channels.push_back(banim);
          logWarn("Invalid bone %s found in animation. Ignored",sname.c_str());
          continue;
        }
        banim.id=res->second;
        bones[banim.id].fakeid=id;
        if(bones[banim.id].parent==-2){
          if(parent>=0)
          {
            assert(parent<anim.channels.size());
            if(!anim.channels[parent].ignore)
            {
              parent=anim.channels[parent].id;
              bones[banim.id].parent=parent;
              bones[parent].children.push_back(banim.id);
            }else
              bones[banim.id].parent=-1;
          }else
            bones[banim.id].parent=-1;
        }
        anim.channels.push_back(banim);
      }
    }
    getline(fs,tmp);line.clear();line.str(tmp);
    line>>tmp;
    if(tmp=="skeleton"){
      bool done=false;
      while(!done)
      {
        done=readAnimFrame(anim,fs);
      }
    }
    curAnim=&anim;
  }
  bool readAnimFrame(Anim &anim,ifstream &fs){
    string tmp;
    getline(fs,tmp);
    istringstream line(tmp);
    size_t time=0;
    line>>tmp>>time;
    // logDebug("reading frame %s",tmp.c_str());

    if(tmp=="time"){
      while(1){
        auto prev_pos=fs.tellg();
        getline(fs,tmp);
        string tmp2;
        line.clear();
        line.str(tmp);
        line>>tmp2;

        if(tmp2=="time"||tmp2=="end")
        {
          fs.seekg(prev_pos);
          return false;
        }

        size_t bone;
        float x,y,z,rx,ry,rz;
        line.clear();
        line.str(tmp);
        line>>bone>>x>>y>>z>>rx>>ry>>rz;
        size_t num;
        while(anim.channels[bone].translations.size()<time){
          anim.channels[bone].translations.push_back(anim.channels[bone].translations.back());
          anim.channels[bone].rotations.push_back(anim.channels[bone].rotations.back());
        }
        anim.channels[bone].translations.push_back(glm::vec3(x,y,z));
        anim.channels[bone].rotations.push_back(glm::quat(glm::vec3(rx,ry,rz)));
      }
    }
    logDebug("Read %d frames for %s",anim.channels[0].translations.size(),anim.name.c_str());
    return true;
  }
};


#pragma once
#include <iostream>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <cstdint>
#include "vector3.h"
/**
 * @brief Circle class
 */
class Circle{
public:
	vector3 c;
	uint32_t r;
	vector3 v;
	Circle(){
		init(vector3(0,0,0),0);
	}
	void init(vector3 _c,uint32_t _r,vector3 _v = vector3(0,0,0)){
		c=_c;
		r=_r;
		v=_v;
	}
};
/**
 * @brief Line class
 */
class Line{
public:
	//ax + by = d
	vector3 n;		//n = normal (a,b,0)
	int32_t d;		//d = distance from origin
	Line(){
		init(vector3(0,0,0),0);
	}
	void init(vector3 _n,int32_t _d){
		n=_n;
		d=_d;
	}
};
/**
 * @brief Physics helper class
 * @details Solves collision
 */
class Physics{
public:
	/**
	 * @brief Solves the collision of two circles
	 */
	static bool checkCollision(Circle& c1,Circle& c2,bool solve_penetration=true){
		vector3 n = c2.c - c1.c;
		float dist = n.mag();
		if(dist){
			n = n/dist;
		}
		if(dist>=(c1.r+c2.r))
		return false;
		if(solve_penetration){
		//Solving the penetration
			float pen=c1.r+c2.r-dist;
			c1.c=c1.c - n*pen*0.5;
			c2.c=c2.c + n*pen*0.5;
		}

		return true;
	}
	/**
	 * @brief Solves collision of circle and a line
	 */
	static bool checkLineCollision(Circle& c,Line& l,bool solve_penetration=true){
		//If the distance is greater then false
		if(c.v*l.n >=0)return false;
		float  dist = l.d + (c.c*l.n) - c.r;
		if(dist>=0)return false;

		if(solve_penetration){
			c.c = c.c - l.n*dist;
		}
		return true;
	}
};

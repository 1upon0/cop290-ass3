#version 330

in vec3 Position;

uniform mat4 gMVP;

out vec3 frag_TexCoord;

void main()
{
    vec4 MVP_Pos = gMVP * vec4(Position, 1.0);
    gl_Position = MVP_Pos.xyww;
    frag_TexCoord = Position;
}
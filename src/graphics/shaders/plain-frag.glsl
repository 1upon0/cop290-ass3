#version 330

in vec2 frag_TexCoord;

uniform sampler2D gColorMap;

out vec4 FragColor;

void main()
{
    FragColor = texture(gColorMap, frag_TexCoord);
}

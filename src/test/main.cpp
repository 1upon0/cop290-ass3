#include <iostream>
#include <vector>
#include <GL/glut.h>
#include <GL/freeglut.h>
#include "command.h"
#include "gameEngine.h"
#include "network.h"
using namespace std;

void display1(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	UI::getInput();
	glutSwapBuffers();
}

void display2(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	AI::decideAction();
	glutSwapBuffers();
}

void mouse(int button,int state,int x,int y)
{
	switch (button)
	{
	  	case GLUT_LEFT_BUTTON:
    	  	if(state == GLUT_DOWN)
    	  	{
    	  		cout<<x<<" "<<y<<endl;
    	  	}	
	  		break;
	  	case GLUT_RIGHT_BUTTON:
	  		if(state == GLUT_DOWN)
    	  	{
    	  		cout<<x<<" "<<y<<endl;
    	  	}
    	  	break;
	    default:
	    	break;
	}
}
int main(int argc,char**argv){
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(400,400);
	glutInitWindowPosition(50,50);

	glutCreateWindow("Test");
	
	glClearColor(0.5, 0.5, 0.5, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	string inp;
	cin>>inp;
	if(inp == "human")
	{
		glutDisplayFunc(display1);
		glutIdleFunc(display1);
		glutMouseFunc(mouse);
	}
	else if(inp == "ai")
	{
		glutDisplayFunc(display2);
		glutIdleFunc(display2);
	}
	
	glutMainLoop();
	return 0;
}

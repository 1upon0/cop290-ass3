#pragma once
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <vector>
#include <enet/enet.h>
#include <algorithm>
#include "timer.h"
#include <cstring>
#include "message.h"
using namespace std;

struct Node{
	ENetHost  *self;
	vector<pair<ENetPeer*,uint16_t> > peers;
	size_t	npeers;

	string host_ip;
	uint16_t host_port;
	uint16_t my_port;
};
struct Data{
	uint16_t id;
};

class Network{
public:
	ENetPeer  *host;
	Node *node;
	int bandwidth;
	Network();
	
	void initENet();
	void init();
	void createNode(uint16_t my_port,int n_connections);
	void start(bool isHost,string host_ip="",uint16_t host_port=0);
	void connectToHost(ENetAddress*);
	void connectToPeer(ENetAddress*);
	int connectedPeers(){
		assert(node != 0 && "size of peers of null node");
		return node->peers.size();
	}
	int process();
	int same_addr(ENetAddress &a,ENetAddress &b);
	int find(ENetPeer* peer);
	int eventConnect(ENetEvent* event);
	int eventReceive(ENetEvent* event);
	int eventDisconnected(ENetEvent *event);
	void sendToAll(const vector<char>& buff);
	void sendToPeer(const vector<char>& buff,ENetPeer *peer);

};
Network::Network(){
	node=NULL;
	host=NULL;
}
void Network::init(){
	assert(enet_initialize()==0 && "Cannot initialize the ENet library");
    atexit(enet_deinitialize);
}
// void Network::initENet(){
	
// }
void Network::createNode(uint16_t my_port,int n_connections){
	node = new Node;


	//Initializing the node
	bandwidth = 2*n_connections;		//because each peer requires two connections
	node->self=NULL;
	
	node->my_port=my_port;


	//Initializing address
	ENetAddress address;
	address.host = ENET_HOST_ANY;
	address.port = node->my_port;

	node->self = enet_host_create(&address,bandwidth,2,0,0);
	assert(node->self!=NULL && "Could not create Node!");

}

//is isHost is false, connects to the specified host before starting the processing of events
void Network::start(bool isHost,string host_ip,uint16_t host_port){
	
	if(!isHost){
		assert(host_ip !="" && host_port >0 && "invalid ip or port for host");
		node->host_ip=host_ip;
		node->host_port=host_port;

		ENetAddress address;
		enet_address_set_host(&address,host_ip.c_str());
		address.port = host_port;
		connectToHost(&address);
	}	
	//cout<<"Starting to rocess"<<endl;
	// int res;
	// while(1){
	// 	res=process();
	// 	if(res < 0)break;
	// }
	//cout<<"Network::start() exited with code: "<<res<<endl;
}
// sends connection request to server in a non blocking manner
void Network::connectToHost(ENetAddress* addr){
	cout<<"[Peer]Sending connection request to host:";
	host = enet_host_connect(node->self, addr,2,1);
	assert(host!=NULL && "Could not connect to host!");
	cout<<host->address.port<<endl;

	int idx = find(host);
	if(idx==-1){
		node->peers.push_back(make_pair(host,0));
	}
	else{
		cout<<"Host is already present in the peer list"<<endl;
	}
}
void Network::connectToPeer(ENetAddress* addr){
	ENetPeer *peer;
	cout<<"[Peer]Sending connection request to peer:";
	peer = enet_host_connect(node->self, addr,2,1);
	assert(peer!=NULL && "Could not connect to peer!");
	cout<<peer->address.port<<endl;

	int idx = find(peer);
	if(idx==-1){
		node->peers.push_back(make_pair(peer,0));
	}
	else{
		cout<<"Peer is already present in the peer list"<<endl;
	}	
}
int Network::process(){
	ENetEvent event;
	int rv = 0;

	while (enet_host_service(node->self, &event, 100)>0){

		switch(event.type){
		case ENET_EVENT_TYPE_CONNECT:
			rv = eventConnect(&event);
			break;
		case ENET_EVENT_TYPE_RECEIVE:
			rv = eventReceive(&event);
			enet_packet_destroy(event.packet);
			break;
		case ENET_EVENT_TYPE_DISCONNECT:
			rv = eventDisconnected(&event);
			break;
		default:
			break;
		}

		if (rv<0){
			break;
		}
	}
	return rv;
}

 int Network::same_addr(ENetAddress &a, ENetAddress &b)
{
	int same_host = a.host == b.host;
	int same_port = a.port == b.port;
	return same_host && same_port;
}
int Network::find(ENetPeer* peer){
	//cout<<"find in "<<node->peers.size()<<endl;
	for(int i=0;i<node->peers.size();i++){
		//cout<<node->peers[i].first->address.port<<" "<<peer->address.port<<endl;
		if(same_addr(node->peers[i].first->address,peer->address)){
			return i;
		} 
	}
	return -1;
}
int Network::eventConnect(ENetEvent* event){
	//TODO - See the event structure
	cout<<"Connect event"<<endl;

	

	int idx = find(event->peer);
	if(event->data == 0){
		cout<<"Just echo of my request to peer: "<<event->peer->address.port<<endl;
		assert(idx != -1 && "echo from a peer not in list!!");	//since i am receiving the echo, i must have added the peer to list of peers
		node->peers[idx].second++;
		assert(node->peers[idx].second ==1 && "invalid peer request");
		if(event->peer==host){
			cout<<"[Peer]Host is connected to me"<<endl;
			cout<<"telling the host that i am the new guy"<<endl;
			Message m;
			m.head.id = 69;
			vector<char> buff;
			m.loadTo(buff);
			sendToPeer(buff,host);

		}
	}
	else {
		assert(event->data == 1 && "invalid connect event");
		cout<<"Connection request from peer: "<<event->peer->address.port<<endl;
		assert(idx == -1 && "recieved request from a peer already in my list"); //since we are following a single conection model,i should receive a request to connect only wheni havent already sent one myself
		node->peers.push_back(make_pair(event->peer,1));
		//Testing the broadcast to all
		if(host!=NULL){
			cout<<"Sending the check broadcast message"<<endl;
			CheckMessage m;
			string str="Testing broadcast";
			m.init("Testing broadcast",0);
			vector<char> buff;
			m.loadTo(buff);
			sendToAll(buff);
		}
		
	}
	cout<<"Connection complete with peer: "<<event->peer->address.port<<endl;
	return 0;

}
void Network::sendToAll(const vector<char>& buff){
	ENetPacket *packet;
	packet = enet_packet_create(buff.data(),buff.size(),0);
	enet_host_broadcast(node->self,1,packet);
}
void Network::sendToPeer(const vector<char>& buff,ENetPeer *peer){
	ENetPacket *packet;
	packet = enet_packet_create(buff.data(),buff.size(),0);
	enet_peer_send(peer,0,packet);
}

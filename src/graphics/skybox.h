#include "boilerplate.h"
#include "rigid.h"
class SkyBox{
public:
  GLuint tex;
  // GLObject cube;
  RigidObj cube;
  SkyBox(){
    tex=0;
  }
  void load(string base,string ext=".tga"){
    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_CUBE_MAP);
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_CUBE_MAP, tex);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    unsigned char *data;int h,w;
    data = stbi_load((base+"xpos"+ext).c_str(), &w, &h,0,4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, w, h, 0, GL_RGBA , GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
    data = stbi_load((base+"xneg"+ext).c_str(), &w, &h,0,4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
    data = stbi_load((base+"ypos"+ext).c_str(), &w, &h,0,4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
    data = stbi_load((base+"yneg"+ext).c_str(), &w, &h,0,4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
    data = stbi_load((base+"zpos"+ext).c_str(), &w, &h,0,4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
    data = stbi_load((base+"zneg"+ext).c_str(), &w, &h,0,4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    // vector<float> verts={-1.0,  1.0,  1.0,
    //     -1.0, -1.0,  1.0,
    //      1.0, -1.0,  1.0,
    //      1.0,  1.0,  1.0,
    //     -1.0,  1.0, -1.0,
    //     -1.0, -1.0, -1.0,
    //      1.0, -1.0, -1.0,
    //      1.0,  1.0, -1.0
    //    };
    // vector<uint> faces= {
    //   0, 1, 2, 3,0,2,
    //   3, 2, 6, 7,3,6,
    //   7, 6, 5, 4,7,5,
    //   4, 5, 1, 0,4,1,
    //   0, 3, 7, 4,0,7,
    //   1, 2, 6, 5,1,6
    //   };
    // cube.loadShaders("shaders/cube-vert.glsl","shaders/cube-frag.glsl");
    // cube.addAttrib("Position",0,3);
    // cube.bindBuffers(verts.data(),sizeof(float)*3,verts.size(),faces.data(),sizeof(uint),faces.size());

    cube.load("res/sphere.obj","shaders/cube-vert.glsl","shaders/cube-frag.glsl");
  }
  void render(const mat4x4 &mvp){
    cube.preRender();
    glUniformMatrix4fv(0, 1, GL_FALSE, &mvp[0][0]);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, tex);
    cube.render();
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
  }
  ~SkyBox(){
    if(tex)
      glDeleteTextures(1, &tex);
  }
};
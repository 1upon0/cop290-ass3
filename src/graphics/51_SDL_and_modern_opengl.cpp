/*This source code copyrighted by Lazy Foo' Productions (2004-2015)
and may not be redistributed without written permission.*/
#define DEBUG
//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <GL/glu.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;
#include <stdio.h>
#include <string>
#include <assimp/Importer.hpp>
#include <assimp/Exporter.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "boilerplate.h"

#include "../window.h"

#include "skeletal.h"

bool init();
bool initGL();
void update();
void render();
void close();

//The window we'll be rendering to
SDL_Window* gWindow = NULL;
//OpenGL context
SDL_GLContext gContext;

//Render flag
bool gRenderQuad = true;
Assimp::Importer aiImporter;
Assimp::Exporter aiExporter;
const struct aiScene* scene = NULL;

void recursive_render (const struct aiScene *sc, const struct aiNode* nd,int depth=0){
	for(int i=0;i<depth;i++)printf("  ");
	printf("%s; %d meshes\n",nd->mName.C_Str(),nd->mNumMeshes);
	for(size_t i=0;i<nd->mNumChildren;i++){
		recursive_render(sc,nd->mChildren[i],depth+1);
	}
}

bool init(int& argc,char** argv)
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else{
		//Use OpenGL 3.1 core
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 3 );
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

		//Create window
		gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
		if( gWindow == NULL ){
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else{
			//Create context
			gContext = SDL_GL_CreateContext( gWindow );
			if( gContext == NULL ){
				printf( "OpenGL context could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else{
				//Initialize GLEW
				glewExperimental = GL_TRUE; 
				GLenum glewError = glewInit();
				if( glewError != GLEW_OK ){
					printf( "Error initializing GLEW! %s\n", glewGetErrorString( glewError ) );
				}

				//Use Vsync
				if( SDL_GL_SetSwapInterval( 1 ) < 0 ){
					printf( "Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError() );
				}

				//Initialize OpenGL
				if(!initGL()){
					printf( "Unable to initialize OpenGL!\n" );
					success = false;
				}
			}
		}
	}

	//This is the Game initialization
	//This will initialize the window
	Window::_()->init(argc,argv);
	return success;
}

GLObject obj;
SkeletalObj puj;
bool initGL()
{
	//Initialize clear color
	glClearColor( 0.f,0.f, 0.2f, 1.f );

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	//Generate program

	puj.load("res/pudge_model.fbx");  
	puj.print();
	return true;
}

float theta=0;
void update(){
	//No per frame update needed
}

void render(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Window::_()->displayCallback();
}

void close(){
	glDeleteProgram(GetRenderer()->gProgramID);
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	SDL_Quit();
}

int main( int argc, char** argv )
{
	if(!init(argc,argv)){
		printf( "Failed to initialize!\n" );
	}
	else{
		bool quit = false;
		SDL_Event e;
		SDL_StartTextInput();

		while(!quit)
		{
			//Handle events on queue
			while( SDL_PollEvent( &e ) != 0 ){
				//User requests quit
				if( e.type == SDL_QUIT ){
					quit = true;
				}
				//Handle keypress with current mouse position
				else if( e.type == SDL_TEXTINPUT ){
					int x = 0, y = 0;
					SDL_GetMouseState( &x, &y );
					Window::_()->handleKeys( e.text.text[ 0 ], x, y );
				}
				else if(e.type == SDL_MOUSEBUTTONDOWN){
					Window::_()->mouseCallback(e.button.button,e.button.state,e.button.x,e.button.y);
				}
				else if(e.type == SDL_KEYDOWN){
					if(e.key.keysym.sym == SDLK_p){
						//TODO - pause
						
					}
				}
			}

			//Render quad
			render();
			//Update screen
			SDL_GL_SwapWindow( gWindow );
		}
		SDL_StopTextInput();
	}
	close();
	return 0;
}